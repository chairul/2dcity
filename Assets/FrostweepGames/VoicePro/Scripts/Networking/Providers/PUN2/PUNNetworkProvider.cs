﻿#if PUN2_NETWORK_PROVIDER
using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;
using System;
using UnityEngine;

namespace FrostweepGames.VoicePro.NetworkProviders.PUN
{
    /// <summary>
    /// Photon PUN 2 Network Provider for data transmition
    /// </summary>
    public class PUNNetworkProvider : INetworkProvider
    {
        /// <summary>
        /// Code of network event that uses for voice data transition
        /// </summary>
        private const byte VoiceEventCode = 199;

        public event Action<INetworkActor, byte[]> NetworkDataReceivedEvent;

        private INetworkActor _networkActor;

        private GameObject _eventsHandler;

        public bool ReadyToTransmit => PhotonNetwork.NetworkClientState == ClientState.Joined;

        public void Dispose()
        {
            PhotonNetwork.NetworkingClient.EventReceived -= NetworkEventReceivedHandler;

            _networkActor = null;
            NetworkDataReceivedEvent = null;

            if (_eventsHandler != null)
            {
                MonoBehaviour.Destroy(_eventsHandler);
                _eventsHandler = null;
            }
        }

        public void Init(INetworkActor networkActor)
        {
            _eventsHandler = MonoBehaviour.Instantiate(Resources.Load<GameObject>("PhotonNetworkInstance"));
            _networkActor = networkActor;

            PhotonNetwork.NetworkingClient.EventReceived += NetworkEventReceivedHandler;
        }

        public void SendNetworkData(NetworkRouter.NetworkParameters parameters, byte[] bytes)
        {
            // sending data of recorded samples by using raise event feature
            RaiseEventOptions raiseEventOptions = new RaiseEventOptions { Receivers = parameters.sendToAll ? ReceiverGroup.All : ReceiverGroup.Others };
            SendOptions sendOptions = new SendOptions { Reliability = parameters.reliable };
            PhotonNetwork.RaiseEvent(VoiceEventCode, new object[] { _networkActor.ToString(), bytes }, raiseEventOptions, sendOptions);
        }

        public string GetNetworkState()
        {
            return PhotonNetwork.NetworkClientState.ToString();
        }

        public string GetConnectionToServer()
        {
            return PhotonNetwork.Server.ToString();
        }

        public string GetCurrentRoomName()
        {
            return PhotonNetwork.CurrentRoom != null ? PhotonNetwork.CurrentRoom.Name : string.Empty;
        }

        /// <summary>
		/// PUN event handler of network events
		/// </summary>
		/// <param name="photonEvent"></param>
		private void NetworkEventReceivedHandler(EventData photonEvent)
        {
            if (photonEvent.Code == VoiceEventCode)
            {
                object[] data = (object[])photonEvent.CustomData;

                string sender = (string)data[0];
                byte[] bytes = (byte[])data[1];

                NetworkDataReceivedEvent?.Invoke(PUNNetworkActor.FromString(sender), bytes);
            }
        }

        public class PUNNetworkActor : INetworkActor
        {
            public int Id { get; private set; }

            public string Name { get; private set; }

            public PUNNetworkActor(int id, string name)
            {
                Id = id;
                Name = name;
            }

            public override string ToString()
            {
                return Id + "_" + Name;
            }

            public static PUNNetworkActor FromString(string data)
            {
                string[] split = data.Split('_');
                return new PUNNetworkActor(int.Parse(split[0]), split[1]);
            }
        }
    }
}
#endif