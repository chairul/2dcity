﻿namespace FrostweepGames.VoicePro
{
    public interface INetworkActor
    {
        int Id { get; }

        string Name { get; }
    }
}