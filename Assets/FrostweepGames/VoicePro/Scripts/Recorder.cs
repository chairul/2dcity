﻿using UnityEngine;
using FrostweepGames.Plugins.Native;
using System.Collections.Generic;
using System;
using FrostweepGames.Plugins;

namespace FrostweepGames.VoicePro
{
	/// <summary>
	/// Basic record system for voice chat
	/// </summary>
	public class Recorder : MonoBehaviour
	{
		/// <summary>
		/// Throws when record successfully started
		/// </summary>
		public event Action RecordStartedEvent;

		/// <summary>
		/// Throws when record successfully ended
		/// </summary>
		public event Action RecordEndedEvent;

		/// <summary>
		/// Throws when record starting failed
		/// </summary>
		public event Action<string> RecordFailedEvent;

		/// <summary>
		/// Last cached sample position
		/// </summary>
		private int _lastPosition = 0;

		/// <summary>
		/// Array of recoreded samples
		/// </summary>
		private List<float> _buffer;

		/// <summary>
		/// Microphone audio clip
		/// </summary>
		private AudioClip _workingClip;

		/// <summary>
		/// Current selected microphone device in usage
		/// </summary>
		[ReadOnly]
		[SerializeField]
		private string _microphoneDevice;

		/// <summary>
		/// Sets if transmission over network will be reliable or not
		/// </summary>
		public bool reliableTransmission = true;

		/// <summary>
		/// Sets network receivers in network, if enabled then sends also on this client, if not - only others
		/// </summary>
		public bool debugEcho = false;

		/// <summary>
		/// Says status of recording
		/// </summary>
		[ReadOnly]
		public bool recording = false;

		/// <summary>
		/// Sets detecting of voice (could decrease performance)
		/// </summary>
		public bool voiceDetectionEnabled = false;

		/// <summary>
		/// Sets voice volume threshold for detection of voice
		/// </summary>
		[Range(0f, 0.5f)]
		public float voiceDetectionThreshold = 0.02f;

		/// <summary>
		/// Average voice level during recording
		/// </summary>
		[ReadOnly]
		[SerializeField]
		private float _averageVoiceLevel = 0f;

		/// <summary>
		/// Saves last position of mic when it stops
		/// </summary>
		private int _stopRecordPosition = -1;

		/// <summary>
		/// Initializes buffer, refreshes microphones list and selects first microphone device if exists
		/// </summary>
		private void Start()
		{
			_buffer = new List<float>();

			RefreshMicrophones();

			if (CustomMicrophone.HasConnectedMicrophoneDevices())
			{
				_microphoneDevice = CustomMicrophone.devices[0];
			}
		}

		/// <summary>
		/// Handles processing of recording each frame
		/// </summary>
		private void Update()
		{
			if (!string.IsNullOrEmpty(_microphoneDevice))
			{
				ProcessRecording();
			}
		}

		/// <summary>
		/// Processes samples data from microphone recording and fills buffer of samples then sends it over network
		/// </summary>
		private void ProcessRecording()
		{
			int currentPosition = CustomMicrophone.GetPosition(_microphoneDevice);

			// fix for end record incorrect position
			if (_stopRecordPosition != -1)
				currentPosition = _stopRecordPosition;

			if (recording || currentPosition != _lastPosition)
			{
				float[] array = new float[Constants.RecordingTime * Constants.SampleRate];
				CustomMicrophone.GetRawData(ref array, _workingClip);

				if (_lastPosition != currentPosition && array.Length > 0)
				{
					// Detects does user says something based on volume level
					if (!voiceDetectionEnabled || VoiceDetector.IsVoiceDetected(AudioConverter.FloatToByte(array), ref _averageVoiceLevel, voiceDetectionThreshold))
					{
						if (_lastPosition > currentPosition)
						{
							_buffer.AddRange(GetChunk(array, _lastPosition, array.Length - _lastPosition));
							_buffer.AddRange(GetChunk(array, 0, currentPosition));
						}
						else
						{
							_buffer.AddRange(GetChunk(array, _lastPosition, currentPosition - _lastPosition));
						}
					}

					// sends data chunky
					if (_buffer.Count >= Constants.ChunkSize)
					{
						SendDataToNetwork(_buffer.GetRange(0, Constants.ChunkSize));
						_buffer.RemoveRange(0, Constants.ChunkSize);
					}
				}

				_lastPosition = currentPosition;
			}
			else
			{
				_lastPosition = currentPosition;

				if (_buffer.Count > 0)
				{
					// sends left data chunky
					if (_buffer.Count >= Constants.ChunkSize)
					{
						SendDataToNetwork(_buffer.GetRange(0, Constants.ChunkSize));
						_buffer.RemoveRange(0, Constants.ChunkSize);
					}
					// sends all left data
					else
					{
						SendDataToNetwork(_buffer);
						_buffer.Clear();
					}
				}
			}
		}

		/// <summary>
		/// Gets range from an array based on start index and length
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="data">input array</param>
		/// <param name="index">start offset</param>
		/// <param name="length">length of output array and how many items will be copied from initial array</param>
		/// <returns></returns>
		private T[] GetChunk<T> (T[] data, int index, int length)
		{
			if(data.Length < index + length)
				throw new Exception("Input array less than parameters income!");

			T[] result = new T[length];
			Array.Copy(data, index, result, 0, length);
			return result;
		}

		/// <summary>
		/// Sends data to other clients or if debug echo then sends to all including this client
		/// </summary>
		/// <param name="samples">list of sampels that will be sent over network</param>
		private void SendDataToNetwork(List<float> samples)
        {
            if (VoiceChatManager.sendVoiceViaManualRPC)
            {
                //PhotonManager.instance.Speak(SceneWorldManager.instance.Player.data.id, SceneWorldManager.instance.Player.data.name, Compressor.Compress(AudioConverter.FloatToByte(samples.ToArray())));
            }
            else
            {
                NetworkRouter.Instance.SendNetworkData(new NetworkRouter.NetworkParameters()
                {
                    reliable = reliableTransmission,
                    sendToAll = debugEcho
                }, Compressor.Compress(AudioConverter.FloatToByte(samples.ToArray())));
            }
        }

		/// <summary>
		/// Requests microphone perission and refreshes list of microphones if WebGL platform
		/// </summary>
		public void RefreshMicrophones()
		{
			CustomMicrophone.RequestMicrophonePermission();
		}

		/// <summary>
		/// Starts recording of microphone
		/// </summary>
		public bool StartRecord()
		{
			if (CustomMicrophone.IsRecording(_microphoneDevice) || !CustomMicrophone.HasConnectedMicrophoneDevices())
			{
				RecordFailedEvent?.Invoke("record already started or no microphone device connected");
				return false;
			}

			if (recording)
			{
				RecordFailedEvent?.Invoke("record already started");
				return false;
			}

			_stopRecordPosition = -1;

			recording = true;

			_buffer?.Clear();

			_workingClip = CustomMicrophone.Start(_microphoneDevice, true, Constants.RecordingTime, Constants.SampleRate);

			RecordStartedEvent?.Invoke();

			return true;
		}

		/// <summary>
		/// Stops recording of microphone
		/// </summary>
		public bool StopRecord()
		{
			if (!CustomMicrophone.IsRecording(_microphoneDevice))
				return false;

			if (!recording)
				return false;

			recording = false;

			if (CustomMicrophone.HasConnectedMicrophoneDevices())
			{
				_stopRecordPosition = CustomMicrophone.GetPosition(_microphoneDevice);

				CustomMicrophone.End(_microphoneDevice);
			}

			if (_workingClip != null)
			{
				Destroy(_workingClip);
			}

			RecordEndedEvent?.Invoke();

			return true;
		}

		/// <summary>
		/// Sets microphone device for usage
		/// </summary>
		/// <param name="microphone"></param>
		public void SetMicrophone(string microphone)
		{
			_microphoneDevice = microphone;
		}
	}
}