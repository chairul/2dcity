using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

public class GetURLParameter : MonoBehaviour
{
    [DllImport("__Internal")]
    public static extern string BrowserGetLinkHREF();

    // Start is called before the first frame update
    void Start()
    {
        //GetNEARId();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public string GetNEARId()
    {
#if UNITY_EDITOR
        Dictionary<string, string> prms = new Dictionary<string, string>();
        prms.Add("near", "rioarsa.testnet");
#else
        Dictionary<string, string> prms = GetBrowserParameters();
#endif
        if (prms.Count == 0)
            return "";

        //string output = "Listing Link Parameters:\n";
        //foreach (KeyValuePair<string, string> kvp in prms)
        //{
        //    if (string.IsNullOrEmpty(kvp.Value) == true)
        //        output += "\tKeyword " + kvp.Key + "\n";
        //    else
        //        output += "\tMapping " + kvp.Key + " with value " + kvp.Value + "\n";
        //}

        //Debug.Log(output);

        Debug.Log(prms["near"]);

        string id = prms["near"];

        return id;
    }

    public Vector3Int GetPositionFromParam()
    {
#if UNITY_EDITOR
        Dictionary<string, string> prms = new Dictionary<string, string>();
        prms.Add("pos", "x0y0");
#else
        Dictionary<string, string> prms = GetBrowserParameters();
#endif
        if (prms.Count == 0)
            return Vector3Int.zero;

        Debug.Log(prms["pos"]);

        Vector3Int pos = Vector3Int.zero;
        string posString = prms["pos"];

        string xString = posString.Split('y')[0];
        string yString = posString.Split('y')[1];

        xString = xString.Replace("x", string.Empty);
        xString = xString.Replace("y", string.Empty);
        yString = yString.Replace("x", string.Empty);
        yString = yString.Replace("y", string.Empty);

        int x = 0;
        int y = 0;
        bool successX = int.TryParse(xString, out x);
        bool successY = int.TryParse(yString, out y);

        if (successX && successX)
        {
            return new Vector3Int(x, y, 0);
        }

        return pos;
    }

    public static Dictionary<string, string> GetBrowserParameters()
    {

        Dictionary<string, string> ret =
            new Dictionary<string, string>();

        //#if UNITY_WEBGL && !UNITY_EDITOR

        // Use the .NET class Uri to parse the link and get the query portion for us
        System.Uri uri = new System.Uri(BrowserGetLinkHREF());
        string linkParams = uri.Query;

        if (linkParams.Length == 0)
            return ret;

        // If it contains the starting questionmark, strip it out
        if (linkParams[0] == '?')
            linkParams = linkParams.Substring(1);

        // Get the different parameters separated by '&''
        string[] sections = linkParams.Split(new char[] { '&' }, System.StringSplitOptions.RemoveEmptyEntries);
        foreach (string sec in sections)
        {
            // Check to see if it's a variable assignment
            string[] split = sec.Split(new char[] { '=' }, System.StringSplitOptions.RemoveEmptyEntries);
            if (split.Length == 0)
                continue;

            // If it contains an '=' sign, assign its key-value in the dictionary
            if (split.Length == 2)
            {
                if (ret.ContainsKey(split[0]) == true)
                    ret[split[0]] = split[1];
                else
                    ret.Add(split[0], split[1]);
            }
            else
            {
                // If it doesn't contain a '=', just log it as a key, and an empty value
                if (ret.ContainsKey(sec) == true)
                    ret[sec] = "";
                else
                    ret.Add(sec, "");
            }

        }
        //#endif
        return ret;
    }
}
