// Creating functions for the Unity
mergeInto(LibraryManager.library, {
	BrowserGetLinkHREF : function()
	{
		// https://css-tricks.com/snippets/javascript/get-url-and-url-parts-in-javascript/
		var search = window.location.href;
		var searchLen = lengthBytesUTF8(search) + 1;
		var buffer = _malloc(searchLen);
		stringToUTF8(search, buffer, searchLen);
		return  buffer;
	},

   // Function example
   CallFunction: function () {
      // Show a message as an alert
      window.alert("You called a function from this plugin!");
   },
   
   WalletCall: function () {
      // Define text value
      var textToPass = WalletConnect();

      // Create a buffer to convert text to bytes
      var bufferSize = lengthBytesUTF8(textToPass) + 1;
      var buffer = _malloc(bufferSize);

      // Convert text
      stringToUTF8(textToPass, buffer, bufferSize);

      // Return text value
      return buffer;
   }
});