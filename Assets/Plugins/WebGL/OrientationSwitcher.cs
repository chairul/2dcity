using System.Runtime.InteropServices;

public class OrientationSwitcher
{
    [DllImport("__Internal")]
    public static extern void BrowserToggleOrientation(string isPortrait);

    [DllImport("__Internal")]
    public static extern void ListenForBrowserResize(string gameobjectName, string callbackMethod);

    [DllImport("__Internal")]
    public static extern string GetBrowserOrientation();
}