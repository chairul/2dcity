mergeInto(LibraryManager.library, {
    ListenForBrowserResize: function(objectName, callback) {
        var parsedObjectName = Pointer_stringify(objectName);
        var parsedCallback = Pointer_stringify(callback);
		
		window.addEventListener('resize', function(){
			callUnityResize(parsedObjectName,parsedCallback);
		});
    },
	
	GetBrowserOrientation : function()
	{
		onResize();
		
		// Define text value
		var textToPass = container.getAttribute("isportrait").toLowerCase();

		// Create a buffer to convert text to bytes
		var bufferSize = lengthBytesUTF8(textToPass) + 1;
		var buffer = _malloc(bufferSize);

		// Convert text
		stringToUTF8(textToPass, buffer, bufferSize);

		// Return text value
		return buffer;
	},
	
	BrowserToggleOrientation: function(obj) {
		var isPortrait = Pointer_stringify(obj);
		toggleOrientation(isPortrait);
	}
});