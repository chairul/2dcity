using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;
using UnityEngine.Tilemaps;
using System.IO;

public class MapDataExtractor : MonoBehaviour
{
    public static readonly int chunkSize = 50;

#if UNITY_EDITOR
    [SerializeField] Tilemap map;
    [SerializeField] UnityEngine.UI.Text loadingText;

    IEnumerator ExtractData()
    {
        List<Vector3Int> startPost = new List<Vector3Int>();
        for (int x = 0; x < map.size.x; x += chunkSize)
        {
            for (int y = 0; y < map.size.y; y += chunkSize)
            {
                Vector3Int position = new Vector3Int(x + map.origin.x, y + map.origin.y, 0);

                if (map.GetTile(position) == null)
                {
                    continue;
                }

                if (!startPost.Contains(position))
                {
                    startPost.Add(position);
                }
            }

            loadingText.text = string.Format("Finding Chunk... ({0}/{1})", x, map.size.x);
            yield return null;
        }

        yield return null;

        Dictionary<Vector3Int, MapChunk> mapChunk = new Dictionary<Vector3Int, MapChunk>();
        for (int i = 0; i < startPost.Count; i++)
        {
            Vector3Int position = startPost[i];

            MapChunk data = new MapChunk();
            data.startPos = position;
            data.tiles = new Dictionary<Vector3Int, string>();

            for (int x = position.x; x < position.x + chunkSize; x++)
            {
                Vector3Int newPosition = new Vector3Int(x, position.y, 0);

                if (map.GetTile(newPosition) == null)
                {
                    break;
                }

                data.width++;
            }

            for (int y = position.y; y < position.y + chunkSize; y++)
            {
                Vector3Int newPosition = new Vector3Int(position.x, y, 0);

                if (map.GetTile(newPosition) == null)
                {
                    break;
                }

                data.height++;
            }

            for (int x = position.x; x < position.x + chunkSize; x++)
            {
                for (int y = position.y; y < position.y + chunkSize; y++)
                {
                    Vector3Int newPosition = new Vector3Int(x, y, 0);

                    if (map.GetTile(newPosition) == null)
                    {
                        continue;
                    }

                    if (!data.tiles.ContainsKey(newPosition))
                    {
                        string[] splittedId = map.GetTile(newPosition).name.Split('_');
                        string spriteId = splittedId[splittedId.Length - 1];
                        data.tiles.Add(newPosition, spriteId);
                        //tileCount++;
                    }
                }
            }
            mapChunk.Add(data.startPos, data);

            loadingText.text = string.Format("Loading Chunk... ({0}/{1})", i, map.size.x);
            yield return null;
        }

        yield return null;
        string basePath = Application.dataPath + "/Resources/Map/";

        int index = 0;
        foreach (MapChunk map in mapChunk.Values)
        {
            string path = basePath + map.startPos.ToString() + ".json";
            string data = map.GetJSON().ToString();

            // replace quote with whitespace
            data = data.Replace('\"', ' ');
            // remove whitespace
            data = System.Text.RegularExpressions.Regex.Replace(data, @"\s+", "");
            // add quote for key
            data = data.Replace("s", "\"s\"").Replace("x", "\"x\"").Replace("y", "\"y\"").Replace("w", "\"w\"").Replace("h", "\"h\"").Replace("t", "\"t\"");

            File.WriteAllText(path, data);

            index++;

            loadingText.text = string.Format("Writting Chunk... ({0}/{1})", index, mapChunk.Count);
            yield return null;
        }

        List<BuildingSpotGeneratedData> buildingSpotData = new List<BuildingSpotGeneratedData>();
        List<Vector3Int> blackListedPost = new List<Vector3Int>();
        index = 0;

        for (int i = 0; i < map.size.x; i++)
        {
            for (int j = 0; j < map.size.y; j++)
            {
                Vector3Int position = new Vector3Int(i + map.origin.x, j + map.origin.y, 0);

                if (map.GetTile(position) == null)
                {
                    continue;
                }

                string[] splittedId = map.GetTile(position).name.Split('_');
                string spriteId = splittedId[splittedId.Length - 1];

                if (DataManager.buildingSpotSpriteCode.Contains(spriteId) && !blackListedPost.Contains(position))
                {
                    BuildingSpotGeneratedData data = new BuildingSpotGeneratedData();

                    index++;

                    data.id = "b" + index.ToString();
                    data.startPosition = position;
                    blackListedPost.Add(position);

                    for (int x = position.x; x < map.size.x; x++)
                    {
                        Vector3Int newPosition = new Vector3Int(x, position.y, 0);

                        if (map.GetTile(newPosition) == null)
                        {
                            break;
                        }

                        splittedId = map.GetTile(newPosition).name.Split('_');
                        spriteId = splittedId[splittedId.Length - 1];
                        if (DataManager.buildingSpotSpriteCode.Contains(spriteId))
                        {
                            data.width++;
                            blackListedPost.Add(newPosition);
                        }
                        else
                        {
                            break;
                        }
                    }

                    for (int y = position.y; y < map.size.y; y++)
                    {
                        Vector3Int newPosition = new Vector3Int(position.x, y, 0);

                        if (map.GetTile(newPosition) == null)
                        {
                            break;
                        }

                        splittedId = map.GetTile(newPosition).name.Split('_');
                        spriteId = splittedId[splittedId.Length - 1];
                        if (DataManager.buildingSpotSpriteCode.Contains(spriteId))
                        {
                            data.height++;
                            blackListedPost.Add(newPosition);
                        }
                        else
                        {
                            break;
                        }
                    }

                    for (int x = position.x; x < position.x + data.width; x++)
                    {
                        for (int y = position.y; y < position.y + data.height; y++)
                        {
                            Vector3Int newPosition = new Vector3Int(x, y, 0);

                            if (map.GetTile(newPosition) == null)
                            {
                                continue;
                            }

                            splittedId = map.GetTile(newPosition).name.Split('_');
                            spriteId = splittedId[splittedId.Length - 1];
                            if (DataManager.buildingSpotSpriteCode.Contains(spriteId))
                            {
                                blackListedPost.Add(newPosition);
                            }
                        }
                    }
                    buildingSpotData.Add(data);
                }
            }
            loadingText.text = string.Format("Extracting Building... ({0}/{1})", i, map.size.x);
            yield return null;
        }

        JSONClass spotData = new JSONClass();
        for (int i = 0; i < buildingSpotData.Count; i++)
        {
            spotData["data"][i] = buildingSpotData[i].GetJSON();
        }

        File.WriteAllText(basePath + "building.json", spotData.ToString());

        JSONClass mapProps = new JSONClass();
        mapProps["origin"]["x"].AsInt = map.origin.x;
        mapProps["origin"]["y"].AsInt = map.origin.y;
        mapProps["size"]["x"].AsInt = map.size.x;
        mapProps["size"]["y"].AsInt = map.size.y;
        mapProps["start"]["x"].AsInt = startPost[0].x;
        mapProps["start"]["y"].AsInt = startPost[0].y;
        File.WriteAllText(basePath + "map.json", mapProps.ToString());

        loadingText.text = string.Format("Done");
        yield return null;
    }

    private void Start()
    {
        StartCoroutine(ExtractData());
    }
#endif
}

public struct MapChunk
{
    public Vector3Int startPos;
    public int width;
    public int height;
    public Dictionary<Vector3Int, string> tiles;

    public MapChunk(JSONNode data)
    {
        startPos = new Vector3Int(data["s"]["x"].AsInt, data["s"]["y"].AsInt, 0);
        width = data["w"].AsInt;
        height = data["h"].AsInt;
        //Debug.LogError($"startpos : {startPos.ToString()}");
        tiles = new Dictionary<Vector3Int, string>();
        for (int i = 0; i < data["t"].Count; i++)
        {
            int x = startPos.x + Mathf.FloorToInt(i / width);
            int y = startPos.y + (i % width);
            //Debug.Log($"{i}:({x},{y})");
            tiles.Add(new Vector3Int(x, y, 0), data["t"][i]);
        }
    }

    public JSONNode GetJSON()
    {
        JSONClass data = new JSONClass();

        data["s"]["x"].AsInt = startPos.x;
        data["s"]["y"].AsInt = startPos.y;
        data["w"].AsInt = width;
        data["h"].AsInt = height;

        JSONArray arr = new JSONArray();
        foreach (Vector3Int pos in tiles.Keys)
        {
            arr.Add(tiles[pos]);
        }
        data["t"] = arr;

        return data;
    }
}

[System.Serializable]
public struct BuildingSpotGeneratedData
{
    public string id;
    public string collectionUUID;
    public string tokenAddress;
    public Vector3Int startPosition;
    public int width;
    public int height;

    public BuildingSpotGeneratedData(JSONNode data)
    {
        id = data["id"];
        startPosition = new Vector3Int(data["start"]["x"].AsInt, data["start"]["y"].AsInt, 0);
        width = data["width"].AsInt;
        height = data["height"].AsInt;
        collectionUUID = !string.IsNullOrEmpty(data["coll_uuid"]) ? data["coll_uuid"].Value : "823752a5-0d54-4af5-a9c8-166666412501";
        tokenAddress = !string.IsNullOrEmpty(data["token_address"]) ? data["token_address"].Value : "0x88A9CCfbbAAF2f5a307f470A564103139f151d57";

        startPosition += new Vector3Int(2, 1, 0);
    }

    public JSONNode GetJSON()
    {
        JSONClass data = new JSONClass();

        data["id"] = id;
        data["start"]["x"].AsInt = startPosition.x;
        data["start"]["y"].AsInt = startPosition.y;
        data["width"].AsInt = width;
        data["height"].AsInt = height;
        data["coll_uuid"] = collectionUUID;
        data["token_address"] = tokenAddress;

        return data;
    }

    public List<Vector3Int> GetCornerPosition()
    {
        return new List<Vector3Int>()
        {
            startPosition,
            new Vector3Int(startPosition.x + width, startPosition.y, 0),
            new Vector3Int(startPosition.x, startPosition.y + height, 0),
            new Vector3Int(startPosition.x + width, startPosition.y + height, 0)    
        };
    }

    public Vector3Int GetCenterPosition()
    {
        int x = (startPosition.x + GetCornerPosition()[GetCornerPosition().Count - 1].x) / 2;
        int y = (startPosition.y + GetCornerPosition()[GetCornerPosition().Count - 1].y) / 2;

        return new Vector3Int(x, y, 0);
    }
}

public struct MapProperties
{
    public Vector3Int origin;
    public Vector3Int size;
    public Vector3Int start;

    public MapProperties(JSONNode data)
    {
        origin = new Vector3Int(data["origin"]["x"].AsInt, data["origin"]["y"].AsInt, 0);
        size = new Vector3Int(data["size"]["x"].AsInt, data["size"]["y"].AsInt, 0);
        start = new Vector3Int(data["start"]["x"].AsInt, data["start"]["y"].AsInt, 0);
    }
}
