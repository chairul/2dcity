using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using SimpleJSON;

public class MapDataLoader : MonoBehaviour
{
    public static MapDataLoader instance;
    public System.Action OnExtractFinished;

    public Dictionary<Vector3Int, WorldObjectData> worldObjectData;
    public List<BuildingSpotGeneratedData> buildingSpotData;
    public List<Dictionary<string, TileBase>> mapSkinsData;
    public MapProperties mapProperties;
    public Dictionary<GameObject, BuildingTileType> objectPool;
    public List<BuildingFloor> floorPool;
    public List<BuildingWall> wallPool;
    public int loadDistance = 25;

    [SerializeField] List<Tilemap> mapSkins;
    [SerializeField] GameObject loadingPanel;
    [SerializeField] UnityEngine.UI.Text loadingText;
    [SerializeField] GameObject floorSample;
    [SerializeField] GameObject wallSample;

    IEnumerator LoadData()
    {
        loadingPanel.SetActive(true);
        loadingText.text = string.Format("Preparing World ... 0%");

        worldObjectData = new Dictionary<Vector3Int, WorldObjectData>();
        JSONNode worldObjectDataRaw = JSON.Parse(Resources.Load<TextAsset>("map/world_objects").text);

        buildingSpotData = new List<BuildingSpotGeneratedData>();
        JSONNode buildingData = JSON.Parse(Resources.Load<TextAsset>("map/building").text);

        int currentProccess = 0;
        int worldObjectCount = worldObjectDataRaw.Count;
        int buildingCount = buildingData["data"].Count;
        int skinCount = mapSkins.Count;
        int objectPoolCount = Mathf.CeilToInt(Mathf.Pow((loadDistance * 2), 2) * 1.5f);
        int targetProccess = worldObjectCount + buildingCount + skinCount + objectPoolCount;

        for (int i = 0; i < worldObjectCount; i++)
        {
            WorldObjectData worldObject = new WorldObjectData(worldObjectDataRaw[i], true);
            worldObject.tilePosition += new Vector3Int(1, 0, 0);
            worldObjectData.Add(worldObject.tilePosition, worldObject);

            currentProccess++;
            loadingText.text = string.Format("Preparing World ... {0}%", Mathf.FloorToInt(((float)currentProccess / (float)targetProccess) * 100));

            if (i % 10 == 0)
            {
                yield return null;
            }
        }

        for (int i = 0; i < buildingCount; i++)
        {
            buildingSpotData.Add(new BuildingSpotGeneratedData(buildingData["data"][i]));

            currentProccess++;
            loadingText.text = string.Format("Preparing World ... {0}%", Mathf.FloorToInt(((float)currentProccess / (float)targetProccess) * 100));

            if (i % 10 == 0)
            {
                yield return null;
            }
        }

        mapProperties = new MapProperties(JSON.Parse(Resources.Load<TextAsset>("map/map").text));

        mapSkinsData = new List<Dictionary<string, TileBase>>();
        for (int i = 0; i < skinCount; i++)
        {
            Tilemap currentSkin = mapSkins[i];
            Dictionary<string, TileBase> skinData = new Dictionary<string, TileBase>();

            for (int j = 0; j < currentSkin.size.x; j++)
            {
                for (int k = 0; k < currentSkin.size.y; k++)
                {
                    Vector3Int position = new Vector3Int(j + currentSkin.origin.x, k + currentSkin.origin.y, 0);

                    if (currentSkin.GetTile(position) == null)
                    {
                        continue;
                    }

                    string[] splitted = currentSkin.GetTile(position).name.Split('_');
                    string spriteId = splitted[splitted.Length - 1];

                    if (!skinData.ContainsKey(spriteId))
                    {
                        skinData.Add(spriteId, currentSkin.GetTile(position));
                    }
                }
            }

            mapSkinsData.Add(skinData);

            currentProccess++;
            loadingText.text = string.Format("Preparing World ... {0}%", Mathf.FloorToInt(((float)currentProccess / (float)targetProccess) * 100));
            yield return null;
        }

        objectPool = new Dictionary<GameObject, BuildingTileType>();
        floorPool = new List<BuildingFloor>();
        wallPool = new List<BuildingWall>();
        for (int i = 0; i < objectPoolCount; i++)
        {
            BuildingFloor floor = Instantiate(floorSample, transform).GetComponent<BuildingFloor>();
            floor.gameObject.SetActive(false);
            floor.gameObject.name = "floor";
            floorPool.Add(floor);
            objectPool.Add(floor.gameObject, BuildingTileType.Floor);

            BuildingWall wall = Instantiate(wallSample, transform).GetComponent<BuildingWall>();
            wall.gameObject.SetActive(false);
            wall.gameObject.name = "wall";
            wallPool.Add(wall);
            objectPool.Add(wall.gameObject, BuildingTileType.Wall);

            currentProccess++;
            loadingText.text = string.Format("Preparing World ... {0}%", Mathf.FloorToInt(((float)currentProccess / (float)targetProccess) * 100));

            if (i % 100 == 0)
            {
                yield return null;
            }
        }

        loadingPanel.SetActive(false);

        OnExtractFinished?.Invoke();
    }

    public BuildingFloor GetFreeFloorFromPool()
    {
        BuildingFloor floor = null;
        if (floorPool.Count > 0)
        {
            floor = floorPool[0];
            floorPool.RemoveAt(0);
        }

        return floor;
    }

    public void ReturnFloorToPool(BuildingFloor floor)
    {
        floorPool.Add(floor);

        floor.ClearDecorations();
        floor.transform.SetParent(transform);
        floor.gameObject.name = "floor";
        floor.gameObject.SetActive(false);
    }

    public BuildingWall GetFreeWallFromPool()
    {
        BuildingWall wall = null;
        if (wallPool.Count > 0)
        {
            wall = wallPool[0];
            wallPool.RemoveAt(0);
        }

        return wall;
    }

    public void ReturnWallToPool(BuildingWall wall)
    {
        wallPool.Add(wall);

        wall.ClearDecorations();
        wall.transform.SetParent(transform);
        wall.gameObject.name = "wall";
        wall.gameObject.SetActive(false);
    }

    public void ReturnAllObejctPool()
    {
        foreach(GameObject obj in objectPool.Keys)
        {
            if (obj.transform.parent == transform)
            {
                continue;
            }

            if (objectPool[obj] == BuildingTileType.Floor)
            {
                ReturnFloorToPool(obj.GetComponent<BuildingFloor>());
            }
            else if (objectPool[obj] == BuildingTileType.Wall)
            {
                ReturnWallToPool(obj.GetComponent<BuildingWall>());
            }
        }
    }

    void Awake()
    {
        instance = this;
        DontDestroyOnLoad(gameObject);
    }

    public void Load(UnityEngine.UI.Text statusText)
    {
        if (statusText != null)
        {
            loadingText.gameObject.SetActive(false);
            loadingText = statusText;
        }

        StartCoroutine(LoadData());
    }

    private void Update()
    {
        //Debug.LogError(floorPool.Count + " - " + wallPool.Count);
    }
}
