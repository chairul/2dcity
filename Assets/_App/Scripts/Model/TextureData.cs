using SimpleJSON;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TextureType { Wall, Floor, Furniture, Painting, AvatarHead, AvatarHair, AvatarFace, AvatarTop, AvatarArm, AvatarHip, AvatarThigh, AvatarLeg, AvatarShoe }
public enum TextureSourceType { URL, Index }

public class TextureData
{
    public TextureType type;
    public TextureSourceType textureSourceType;
    public string textureURL = "";
    public string sourceURL = "";
    public int textureIndex;
    public string textureId = "";
    public bool isAnimated;

    public TextureData() { }

    public TextureData(JSONNode data, TextureType type)
    {
        this.type = type;
        textureSourceType = (TextureSourceType)data["st"].AsInt;
        textureURL = data["url"];
        sourceURL = data["sc"];
        textureIndex = data["i"].AsInt;
        textureId = data["id"];
        isAnimated = data["gif"].AsBool;
    }

    public TextureData(string sourceURL, string textureURL, TextureType type, bool isAnimated)
    {
        this.type = type;
        this.textureSourceType = TextureSourceType.URL;
        this.textureURL = textureURL;
        this.sourceURL = sourceURL;
        this.isAnimated = isAnimated;
    }

    public TextureData(int textureIndex, string textureId, TextureType type)
    {
        this.type = type;
        this.textureSourceType = TextureSourceType.Index;
        this.textureIndex = textureIndex;
        this.textureId = textureId;
        this.isAnimated = false;
    }

    public TextureData(TextureData copy)
    {
        this.type = copy.type;
        this.textureSourceType = copy.textureSourceType;
        this.sourceURL = copy.sourceURL;
        this.textureURL = copy.textureURL;
        this.textureIndex = copy.textureIndex;
        this.textureId = copy.textureId;
        this.isAnimated = copy.isAnimated;
    }

    public JSONClass ToJSON()
    {
        JSONClass json = new JSONClass();

        json["st"].AsInt = (int)textureSourceType;
        if (!string.IsNullOrEmpty(textureURL))
        {
            json["url"] = textureURL;
        }
        if (!string.IsNullOrEmpty(sourceURL))
        {
            json["sc"] = sourceURL;
        }
        json["i"].AsInt = textureIndex;
        if (!string.IsNullOrEmpty(textureId))
        {
            json["id"] = textureId;
        }
        json["gif"].AsBool = isAnimated;

        return json;
    }

    public static bool operator !=(TextureData texture, TextureData newtexture)
    {
        return !(texture == newtexture);
    }

    public static bool operator ==(TextureData texture, TextureData newtexture)
    {
        try
        {
            if (texture.type != newtexture.type)
            {
                return false;
            }
            if (texture.textureSourceType != newtexture.textureSourceType)
            {
                return false;
            }
            if (texture.textureURL != newtexture.textureURL)
            {
                return false;
            }
            if (texture.sourceURL != newtexture.sourceURL)
            {
                return false;
            }
            if (texture.textureIndex != newtexture.textureIndex)
            {
                return false;
            }
            if (texture.textureId != newtexture.textureId)
            {
                return false;
            }
            if (texture.isAnimated != newtexture.isAnimated)
            {
                return false;
            }
            return true;
        }
        catch (System.Exception)
        {
            return false;
        }
    }

    public static TextureType ToTextureType(BuildingTileType type)
    {
        if (type == BuildingTileType.Floor)
        {
            return TextureType.Floor;
        }
        else if (type == BuildingTileType.Wall)
        {
            return TextureType.Wall;
        }
        else if (type == BuildingTileType.Furniture)
        {
            return TextureType.Furniture;
        }
        else if (type == BuildingTileType.Painting)
        {
            return TextureType.Painting;
        }

        return TextureType.Floor;
    }

    public static TextureType ToTextureType(string id, AvatarItemType type)
    {
        if (type == AvatarItemType.Head)
        {
            if (id.Contains("a"))
            {
                return TextureType.AvatarHead;
            }
            else if (id.Contains("h"))
            {
                return TextureType.AvatarHair;
            }
        }
        else if (type == AvatarItemType.Face)
        {
            return TextureType.AvatarFace;
        }
        else if (type == AvatarItemType.Top)
        {
            if (id.Contains("r"))
            {
                return TextureType.AvatarArm;
            }
            else
            {
                return TextureType.AvatarTop;
            }
        }
        else if (type == AvatarItemType.Bottom)
        {
            if (id.Contains("p"))
            {
                return TextureType.AvatarHip;
            }
            else if (id.Contains("t"))
            {
                return TextureType.AvatarThigh;
            }
            else if (id.Contains("l"))
            {
                return TextureType.AvatarLeg;
            }
            else if(id.Contains("z"))
            {
                return TextureType.AvatarShoe;
            }
        }

        return TextureType.AvatarHead;
    }
}

