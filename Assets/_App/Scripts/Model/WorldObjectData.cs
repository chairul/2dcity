using UnityEngine;
using SimpleJSON;

[System.Serializable]
public class WorldObjectData
{
    public string id;
    public string name;
    public WorldObjectInteractionType interactionType;
    public float playerPosX;
    public float playerPosY;
    public Vector3Int tilePosition;

    public void CopyNewData(WorldObjectData data, Vector3Int tilePosition)
    {
        this.id = data.id;
        this.name = data.name;
        this.interactionType = data.interactionType;
        this.playerPosX = data.playerPosX;
        this.playerPosY = data.playerPosY;
        this.tilePosition = new Vector3Int(tilePosition.x, tilePosition.y, 0);
    }

    public WorldObjectData()
    {

    }

    public WorldObjectData(JSONNode data, bool loadFromMapEditor = false)
    {
        if (loadFromMapEditor)
        {
            this.id = data["i"];
            this.tilePosition = new Vector3Int(data["s"]["x"].AsInt, data["s"]["y"].AsInt, 0);
            CopyNewData(WorldObjectManager.worldObjects[id], tilePosition);
            return;
        }

        this.id = data["id"];
        this.name = data["name"];
        this.interactionType = (WorldObjectInteractionType)data["type"].AsInt;
        this.playerPosX = data["x"].AsFloat;
        this.playerPosY = data["y"].AsFloat;
        this.tilePosition = new Vector3Int(data["s"]["x"].AsInt, data["s"]["y"].AsInt, 0);
    }

    public JSONClass ToJSON()
    {
        JSONClass data = new JSONClass();
        data["i"] = id;
        data["s"]["x"].AsInt = tilePosition.x;
        data["s"]["y"].AsInt = tilePosition.y;
        return data;
    }
}
