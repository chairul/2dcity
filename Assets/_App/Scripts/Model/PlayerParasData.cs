using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerParasData : PlayerData
{
    public PlayerParasData(string username, string fullname)
    {
        this.username = username;
        this.fullName = fullname;
        this.nftList = new List<PlayerNFTData>();
    }
}
