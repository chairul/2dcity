using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.Tilemaps;

public class BuildingSpot : MonoBehaviour
{
    [Header("Data")]
    public BuildingSpotGeneratedData spotData;
    Grid grid;
    bool isLoadingBuilding;

    [Header("Component")]
    [SerializeField] Building buildingSample;
    [SerializeField] Transform buildingParent;
    [SerializeField] GameObject minimapLegendEmpty;
    [SerializeField] GameObject minimapLegendMe;
    [SerializeField] GameObject minimapLegendOther;
    [SerializeField] List<GameObject> cornerPoints;
    [SerializeField] GameObject centerPoint;

    public string OwnerName { get; private set; }

    public BuildingData GetBuildingData()
    {
        return buildingSample.Data;
    }

    public string GetBuildungOwner()
    {
        return GetBuildingData() == null ? "" : GetBuildingData().owner;
    }

    public bool IsOwned()
    {
        return !string.IsNullOrEmpty(GetBuildungOwner());
    }

    public bool IsMine()
    {
        return GetBuildungOwner() == DataManager.PlayerUsername;
    }

    public bool IsReadyToEdit()
    {
        return !isLoadingBuilding;
    }

    public bool IsNearby()
    {
        for (int i = 0; i < spotData.GetCornerPosition().Count; i++)
        {
            if (Vector2.Distance(SceneWorldManager.instance.Player.transform.position, grid.CellToWorld(spotData.GetCornerPosition()[i])) <= DataManager.minimumBuildingDistance)
            {
                return true;
            }
        }

        if (Vector2.Distance(SceneWorldManager.instance.Player.transform.position, GetCenterPosition()) <= DataManager.minimumBuildingDistance)
        {
            return true;
        }

        return false;
    }

    public bool IsLoadingBuilding()
    {
        return GetBuildingData() == null ? true : isLoadingBuilding;
    }

    public Vector3 GetCenterPosition()
    {
        return minimapLegendMe.transform.position;
    }

    public void Init(BuildingSpotGeneratedData spotData)
    {
        this.spotData = spotData;

        StartCoroutine(CreateCorners());
    }

    IEnumerator CreateCorners()
    {
        centerPoint.transform.SetParent(null);
        centerPoint.transform.position = grid.CellToWorld(spotData.GetCenterPosition());
        for (int i = 0; i < spotData.GetCornerPosition().Count; i++)
        {
            cornerPoints[i].transform.SetParent(null);
            cornerPoints[i].transform.position = grid.CellToWorld(spotData.GetCornerPosition()[i]);
        }

        yield return null;
        centerPoint.transform.SetParent(transform);
        minimapLegendEmpty.transform.position = minimapLegendMe.transform.position = minimapLegendOther.transform.position = centerPoint.transform.position;
        Destroy(centerPoint);

        PolygonCollider2D polygonCollider2D = GetComponent<PolygonCollider2D>();
        polygonCollider2D.points = new Vector2[spotData.GetCornerPosition().Count];

        Vector2[] points = new Vector2[spotData.GetCornerPosition().Count];

        for (int i = 0; i < spotData.GetCornerPosition().Count; i++)
        {
            cornerPoints[i].transform.SetParent(transform);
            yield return null;
            points[i] = cornerPoints[i].transform.localPosition;

            Destroy(cornerPoints[i]);
        }

        Vector2 lastPoint = points[points.Length - 1];
        points[points.Length - 1] = points[points.Length - 2];
        points[points.Length - 2] = lastPoint;
        polygonCollider2D.SetPath(0, points);
    }

    void SetMinimapLegendStatus()
    {
        minimapLegendEmpty.SetActive(!IsOwned() && !IsMine());
        minimapLegendMe.SetActive(IsOwned() && IsMine());
        minimapLegendOther.SetActive(IsOwned() && !IsMine());
    }

    public void BuyBuilding(string ownerId, int actorNumber)
    {
        BuildingData data = buildingSample.Data;
        data.owner = ownerId;

        SetMinimapLegendStatus();

        isLoadingBuilding = true;

        FirebaseManager.Instance.BuyBuilding(data, (bool isSuccess) =>
        {
            isLoadingBuilding = false;

            if (isSuccess)
            {

            }
            else
            {

            }
        });
    }

    public void LoadBuilding()
    {
        StartCoroutine(LoadingBuilding());
    }

    IEnumerator LoadingBuilding()
    {
        isLoadingBuilding = true;

        bool isWaiting = true;
        BuildingData buildingData = new BuildingData(spotData.id, spotData.width, spotData.height);

        PlayerData player = null;

        FirebaseManager.Instance.GetBuilding(spotData.id, (string data) =>
        {
            if (!string.IsNullOrEmpty(data))
            {
                SimpleJSON.JSONNode parsedData = SimpleJSON.JSON.Parse(data);
                buildingData = new BuildingData(spotData.id, parsedData["owner"], parsedData["data"].AsObject);

                if (buildingData.owner == DataManager.PlayerUsername)
                {
                    isWaiting = false;
                    OwnerName = DataManager.PlayerName;
                    NFTItemManager.Instance.ValidateBuilding(buildingData);
                }
                else
                {
                    NFTItemManager.Instance.GetPlayer(buildingData.owner, (PlayerData result) =>
                    {
                        player = result;
                        isWaiting = false;

                        OwnerName = player == null ? "Unknown" : (string.IsNullOrEmpty(player.FullName) ? buildingData.owner : player.FullName);
                    });
                }
            }
            else
            {
                isWaiting = false;
            }
        },
        () =>
        {
            SceneWorldManager.instance.ShowPopupInfo("Connection timed out. Please try again.");
        });

        yield return new WaitUntil(() => !isWaiting);

        FirebaseManager.Instance.newBuildings.Remove(spotData.id);

        isLoadingBuilding = false;

        BuildingData validData = null;
        if (player != null)
        {
            validData = new BuildingData(buildingData.ValidateData(player));
        }
        else
        {
            validData = new BuildingData(buildingData.ValidateData());
        }

        if (buildingSample == null)
        {
            yield break;
        }

        buildingSample.Load(buildingData, grid, grid.WorldToCell(transform.position));
        SetMinimapLegendStatus();
    }

    private void Awake()
    {
        SetMinimapLegendStatus();
        grid = transform.parent.GetComponent<Grid>();
    }

    void CheckLoadData()
    {
        if (isLoadingBuilding)
        {
            return;
        }

        if (FirebaseManager.Instance.newBuildings.Contains(spotData.id))
        {
            LoadBuilding();
        }
    }

    public void RefreshBuilding()
    {
        buildingSample.DrawBuilding();
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        SetMinimapLegendStatus();
        if (IsNearby())
        {
            CheckLoadData();
        } 
    }
}
