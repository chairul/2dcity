﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;
using System.Linq;

public class PlayerDataModel
{
    public Dictionary<string, JSONNode> ownedBuildingData { get; private set; }
    public string[] ownedBuildingIdList { get { return ownedBuildingData.Keys.ToArray(); } }

    public PlayerDataModel()
    {
        ownedBuildingData = new Dictionary<string, JSONNode>();
    }

    public PlayerDataModel(JSONNode json)
    {
        ownedBuildingData = new Dictionary<string, JSONNode>();

        for (int i = 0; i < json["building"].Count; i++)
        {
            AddBuilding(json["building"][i].AsObject);
        }
    }

    public void AddBuilding(string id, JSONNode data)
    {
        ownedBuildingData.Add(id, data);
    }

    public void AddBuilding(string rawData)
    {
        JSONNode json = JSON.Parse(rawData);
        ownedBuildingData.Add(json["id"].Value, json["data"].AsObject);
    }

    public void AddBuilding(JSONClass data)
    {
        ownedBuildingData.Add(data["id"].Value, data["data"]);
    }

    public JSONClass ToJSON()
    {
        JSONClass json = new JSONClass();

        JSONArray arrBuilding = new JSONArray();
        foreach (var item in ownedBuildingData)
        {
            JSONClass building = new JSONClass();
            building["id"] = item.Key;
            building["data"] = item.Value;

            arrBuilding.Add(building);
        }
        json["building"] = arrBuilding;
        
        return json;
    }
}