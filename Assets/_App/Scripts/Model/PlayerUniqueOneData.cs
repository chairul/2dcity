using System.Collections.Generic;
using SimpleJSON;
using UnityEngine;

public class PlayerUniqueOneData : PlayerData
{
    public PlayerUniqueOneData(JSONNode json)
    {
        username = json["username"];
        fullName = json["fullName"];

        nftList = new List<PlayerNFTData>();
        for (int i = 0; i < json["user_collectible_balance_info"].Count; i++)
        {
            nftList.Add(new PlayerNFTData(json["user_collectible_balance_info"][i]));
        }
    }

    public JSONClass ToJSON()
    {
        JSONClass result = new JSONClass();

        return result;
    }
}

public class PlayerNFTData
{
    public string collectibleUUID { get; }
    public string tokenAddress { get; }
    public string name { get; }
    public string category { get; }
    public string description { get; }
    public string owner { get; }
    public string creator { get; }
    public int quantity { get; }
    public bool isCurrentOwner { get; }

    public NFTImageType imageType { get; }
    public string imageMimeType { get; }
    public string imageURL { get; }
    public string altImageURL { get; }

    public NFTMediaType mediaType { get; }
    public AudioType audioType { get; }
    public string mediaMimeType { get; }
    public string mediaURL { get; }
    public string altMediaURL { get; }

    public string SourceURL
    {
        get
        {
#if METAMASK
            return $"https://appdevv2.unique.one/token/{collectibleUUID}/{tokenAddress}";
#else
            // TEMP
            return $"https://appdevv2.unique.one/token/{collectibleUUID}/{tokenAddress}";
#endif
        }
    }

    public int quantityLeft
    {
        get
        {
            if (NFTItemManager.Instance.NFTUsedData.ContainsKey(SourceURL))
            {
                return quantity - NFTItemManager.Instance.NFTUsedData[SourceURL];
            }
            else
            {
                return quantity;
            }
        }
    }

    public TextureType TextureType
    {
        get
        {
            if (name.Contains("wall"))
            {
                return TextureType.Wall;
            }
            else if (name.Contains("floor"))
            {
                return TextureType.Floor;
            }
            else if (name.Contains("furn"))
            {
                return TextureType.Furniture;
            }
            else if (name.Contains("head"))
            {
                return TextureType.AvatarHead;
            }
            else if (name.Contains("hair"))
            {
                return TextureType.AvatarHair;
            }
            else if (name.Contains("face"))
            {
                return TextureType.AvatarFace;
            }
            else if (name.Contains("torso"))
            {
                return TextureType.AvatarTop;
            }
            else if (name.Contains("arm"))
            {
                return TextureType.AvatarArm;
            }
            else if (name.Contains("hip"))
            {
                return TextureType.AvatarHip;
            }
            else if (name.Contains("thigh"))
            {
                return TextureType.AvatarThigh;
            }
            else if (name.Contains("leg"))
            {
                return TextureType.AvatarLeg;
            }
            else if (name.Contains("shoe"))
            {
                return TextureType.AvatarShoe;
            }

            return TextureType.Painting;
        }
    }

    public PlayerNFTData(JSONNode json)
    {
        tokenAddress = json["tokenAddress"];

        JSONNode jsonItem = json["collectible_balance_of_collectible"];

        collectibleUUID = jsonItem["collectible_uuid"];
        name = jsonItem["collectible_name"];
        category = jsonItem["collectible_category"];
        description = "???";
        owner = jsonItem["user_public_address"];
        creator = "???";
        quantity = jsonItem["noOfCopies"].AsInt;
        isCurrentOwner = jsonItem["collectible_saleinfo"]["is_current_owner"].AsBool;

        imageMimeType = !string.IsNullOrEmpty(jsonItem["collectible_image_mime_type"]) ? jsonItem["collectible_image_mime_type"].Value : "";
        imageType = NFTImageType.Static;
        if (!string.IsNullOrEmpty(imageMimeType))
        {
            try
            {
                string type = imageMimeType.Split('/')[1];

                if (type == "gif")
                {
                    imageType = NFTImageType.Animated;
                }
            }
            catch (System.Exception)
            {
                Debug.Log($"Error parsing image mime type : {imageMimeType}. Fallback to {imageType}");
            }
            imageURL = jsonItem["ipfs_media_path"];
            altImageURL = jsonItem["alternative_media_path"];
        }

        mediaMimeType = !string.IsNullOrEmpty(jsonItem["animation_media_mime_type"]) ? jsonItem["animation_media_mime_type"].Value : "";
        mediaType = NFTMediaType.none;
        if (!string.IsNullOrEmpty(mediaMimeType))
        {
            try
            {
                string mime = mediaMimeType.Split('/')[0];
                mediaType = (NFTMediaType)System.Enum.Parse(typeof(NFTMediaType), mime);
            }
            catch (System.Exception)
            {
                Debug.Log($"Error parsing media mime : {mediaMimeType}. Fallback to {mediaType}");
            }
            mediaURL = jsonItem["ipfs_animation_media_path"];
            altMediaURL = jsonItem["alternative_animation_media_path"];
        }

        audioType = AudioType.UNKNOWN;
        if(mediaType == NFTMediaType.audio)
        {
            try
            {
                string type = mediaMimeType.Split('/')[1];
                audioType = (AudioType)System.Enum.Parse(typeof(AudioType), type.ToUpper());
            }
            catch (System.Exception)
            {
                Debug.Log($"Error parsing audio type : {mediaMimeType}. Fallback to {audioType}");
            }
        }
    }

    public JSONClass ToJSON()
    {
        JSONClass result = new JSONClass();

        return result;
    }
}

public enum NFTImageType { Static, Animated }
public enum NFTMediaType { none, audio, video }