using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;
using System.Linq;

public class BuildingData
{
    public string id { get; set; }
    public string owner { get; set; }
    public Vector2Int size { get; set; }
    public Dictionary<Vector3Int, BuildingTileData> tiles { get; set; }

    public BuildingData()
    {

    }
    public BuildingData(BuildingData data)
    {
        id = data.id;
        owner = data.owner;
        size = data.size;
        tiles = new Dictionary<Vector3Int, BuildingTileData>(data.tiles);
    }

    public BuildingData(string id, int width, int height)
    {
        this.id = id;
        owner = string.Empty;
        size = new Vector2Int(width, height);

        tiles = new Dictionary<Vector3Int, BuildingTileData>();
        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                Vector3Int position = new Vector3Int(x, y, 0);
                BuildingTileData data = new BuildingTileData();
                data.position = position;
                data.type = BuildingTileType.Floor;
                data.textureData = new TextureData(0, "", TextureType.Floor);

                tiles.Add(position, data);
            }
        }
    }

    public BuildingData(string id, string ownerId, JSONNode json)
    {
        this.id = id;
        owner = ownerId;
        size = new Vector2Int(json["size"]["x"].AsInt, json["size"]["y"].AsInt);

        tiles = new Dictionary<Vector3Int, BuildingTileData>();
        for (int i = 0; i < size.x * size.y; i++)
        {
            Vector3Int position = new Vector3Int(json["tiles"][i]["s"]["x"].AsInt, json["tiles"][i]["s"]["y"].AsInt, 0);
            BuildingTileType type = (BuildingTileType)json["tiles"][i]["t"].AsInt;

            if (type == BuildingTileType.Floor)
            {
                tiles.Add(position, new BuildingTileFloorData(json["tiles"][i]));
            }
            else if (type == BuildingTileType.Wall)
            {
                tiles.Add(position, new BuildingTileWallData(json["tiles"][i]));
            }
            else if (type == BuildingTileType.Furniture)
            {
                tiles.Add(position, new BuildingTileFurnitureData(json["tiles"][i]));
            }
            else if(type == BuildingTileType.Painting)
            {
                tiles.Add(position, new BuildingTilePaintingData(json["tiles"][i]));
            }
        }
    }

    public JSONClass ToJSON()
    {
        JSONClass json = new JSONClass();

        json["size"]["x"].AsInt = size.x;
        json["size"]["y"].AsInt = size.y;

        JSONArray array = new JSONArray();
        foreach (var tile in tiles)
        {
            array.Add(tile.Value.ToJSON());
        }
        json["tiles"] = array;

        return json;
    }

    public JSONClass ToJSONAll()
    {
        JSONClass json = new JSONClass();

        json["owner"] = owner;
        json["data"] = ToJSON();
        
        return json;
    }

    public List<BuildingTileData> GetTilesByType(BuildingTileType type)
    {
        return tiles.Values.Where(x => x.type == type).ToList();
    }

    public BuildingData ValidateData()
    {
        BuildingData validData = new BuildingData(this);
        validData.tiles = new Dictionary<Vector3Int, BuildingTileData>();

        foreach (BuildingTileData tileData in tiles.Values)
        {
            if (tileData.type == BuildingTileType.Floor)
            {
                BuildingTileData buildingTileData = ChackValidation(tileData, BuildingTileType.Floor);
                validData.tiles.Add(buildingTileData.position, buildingTileData);
            }
            else if (tileData.type == BuildingTileType.Wall)
            {
                BuildingTileData buildingTileData = ChackValidation(tileData, BuildingTileType.Wall);
                validData.tiles.Add(buildingTileData.position, buildingTileData);
            }
            else if (tileData.type == BuildingTileType.Furniture)
            {
                BuildingTileFurnitureData buildingTileData = ChackValidationFurniture((BuildingTileFurnitureData)tileData);
                validData.tiles.Add(buildingTileData.position, buildingTileData);
            }
            else if (tileData.type == BuildingTileType.Painting)
            {
                BuildingTilePaintingData buildingTileData = ChackValidationPainting((BuildingTilePaintingData)tileData);
                validData.tiles.Add(buildingTileData.position, buildingTileData);
            }
        }

        return validData;
    }

    BuildingTileData ChackValidation(BuildingTileData data, BuildingTileType type)
    {
        if (type == BuildingTileType.Floor)
        {
            BuildingTileFloorData floorData = new BuildingTileFloorData(data.ToJSON());
            floorData.textureData = GetValidatedData(floorData.textureData, TextureType.Floor);
            return floorData;
        }
        else if (type == BuildingTileType.Wall)
        {
            BuildingTileWallData wallData = new BuildingTileWallData(data.ToJSON());
            wallData.textureData = GetValidatedData(wallData.textureData, TextureType.Wall);
            return wallData;
        }

        return data;
    }

    BuildingTileFurnitureData ChackValidationFurniture(BuildingTileFurnitureData data)
    {
        BuildingTileFurnitureData furnData = new BuildingTileFurnitureData(data.ToJSON());
        furnData.textureData = GetValidatedData(furnData.textureData, TextureType.Floor);
        furnData.objTextureData = GetValidatedData(furnData.objTextureData, TextureType.Furniture);
        return furnData;
    }

    BuildingTilePaintingData ChackValidationPainting(BuildingTilePaintingData data)
    {
        BuildingTilePaintingData paintingData = new BuildingTilePaintingData(data.ToJSON());
        paintingData.textureData = GetValidatedData(paintingData.textureData, TextureType.Wall);
        paintingData.objTextureData = GetValidatedData(paintingData.objTextureData, TextureType.Painting);
        return paintingData;
    }

    public TextureData GetValidatedData(TextureData data, TextureType textureType)
    {
        if (data.textureSourceType == TextureSourceType.URL)
        {
            if (!NFTItemManager.Instance.IsValidated(data, textureType))
            {
                return GetDefault(textureType);
            }
        }

        return data;
    }

    TextureData GetDefault(TextureType type)
    {
        return new TextureData(0, "", type);
    }

    public BuildingData ValidateData(PlayerData player)
    {
        List<string> nfts = new List<string>();
        foreach (var item in player.NFTList)
        {
            nfts.Add(item.SourceURL);
        }

        BuildingData validData = new BuildingData(this);
        validData.tiles = new Dictionary<Vector3Int, BuildingTileData>();

        foreach (BuildingTileData tileData in tiles.Values)
        {
            if (tileData.type == BuildingTileType.Floor)
            {
                if (tileData.textureData.textureSourceType == TextureSourceType.URL)
                {
                    if (!nfts.Contains(tileData.textureData.sourceURL))
                    {
                        BuildingTileFloorData floorData = new BuildingTileFloorData(tileData.ToJSON());
                        floorData.textureData = GetDefault(TextureType.Floor);
                        validData.tiles.Add(floorData.position, floorData);
                        continue;
                    }
                }
            }
            else if (tileData.type == BuildingTileType.Wall)
            {
                if (tileData.textureData.textureSourceType == TextureSourceType.URL)
                {
                    if (!nfts.Contains(tileData.textureData.sourceURL))
                    {
                        BuildingTileWallData wallData = new BuildingTileWallData(tileData.ToJSON());
                        wallData.textureData = GetDefault(TextureType.Wall);
                        validData.tiles.Add(wallData.position, wallData);
                        continue;
                    }
                }
            }
            else if (tileData.type == BuildingTileType.Furniture)
            {
                BuildingTileFurnitureData furnData = new BuildingTileFurnitureData(tileData.ToJSON());

                if (furnData.textureData.textureSourceType == TextureSourceType.URL)
                {
                    if (!nfts.Contains(furnData.textureData.sourceURL))
                    {
                        furnData.textureData = GetDefault(TextureType.Floor);
                    }
                }

                if (furnData.objTextureData.textureSourceType == TextureSourceType.URL)
                {
                    if (!nfts.Contains(furnData.objTextureData.sourceURL))
                    {
                        furnData.objTextureData = GetDefault(TextureType.Furniture);
                    }
                }

                validData.tiles.Add(furnData.position, furnData);
                continue;
            }
            else if (tileData.type == BuildingTileType.Painting)
            {
                BuildingTilePaintingData paintingData = new BuildingTilePaintingData(tileData.ToJSON());

                if (paintingData.textureData.textureSourceType == TextureSourceType.URL)
                {
                    if (!nfts.Contains(paintingData.textureData.sourceURL))
                    {
                        paintingData.textureData = GetDefault(TextureType.Wall);
                    }
                }

                if (paintingData.objTextureData.textureSourceType == TextureSourceType.URL)
                {
                    if (!nfts.Contains(paintingData.objTextureData.sourceURL))
                    {
                        paintingData.objTextureData = GetDefault(TextureType.Painting);
                    }
                }

                validData.tiles.Add(paintingData.position, paintingData);
                continue;
            }

            validData.tiles.Add(tileData.position, tileData);
        }

        return validData;
    }

    public bool IsSameData(BuildingData other)
    {
        if (id != other.id)
        {
            return false;
        }
        else if (owner != other.owner)
        {
            return false;
        }
        else if (size != other.size)
        {
            return false;
        }
        else
        {
            foreach(Vector3Int position in tiles.Keys)
            {
                if (!other.tiles.ContainsKey(position))
                {
                    return false;
                }

                if (tiles[position] != other.tiles[position])
                {
                    return false;
                }
            }
        }

        return true;
    }
}