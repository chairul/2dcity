﻿using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;

public class DeviceDataModel
{
    public bool IsMuted;

    public DeviceDataModel()
    {
        IsMuted = false;
    }

    public DeviceDataModel(JSONNode json)
    {
        IsMuted = json["is_muted"].AsBool;
    }

    public JSONClass ToJSON()
    {
        JSONClass json = new JSONClass();

        json["is_muted"].AsBool = IsMuted;

        return json;
    }
}