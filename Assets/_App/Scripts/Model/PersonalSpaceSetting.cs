using UnityEngine;

[CreateAssetMenu(fileName = "PersonalSpace Setting", menuName = "2D City/PersonalSpace Setting")]
public class PersonalSpaceSetting : ScriptableObject
{
    [SerializeField] float cameraMoveSpeed;
    public float CameraMoveSpeed { get { return cameraMoveSpeed; } }

    [SerializeField] float cameraZoomStep;
    public float CameraZoomStep { get { return cameraZoomStep; } }

    [SerializeField] float cameraMinZoom;
    public float CameraMinZoom { get { return cameraMinZoom; } }

    [SerializeField] float cameraMaxZoom;
    public float CameraMaxZoom { get { return cameraMaxZoom; } }
}
