using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;

public enum BuildingTileType { Floor, Wall, Furniture, Painting }
public enum BuildingObjectFace { Left, Right, Both, None, }
public class BuildingTileData
{
    public Vector3Int position;
    public BuildingTileType type;
    public TextureData textureData;

    public BuildingTileData() { }

    public BuildingTileData(Vector3Int position, BuildingTileType type)
    {
        this.position = position;
        this.type = type;
        this.textureData = new TextureData();
    }

    public virtual JSONClass ToJSON()
    {
        JSONClass json = new JSONClass();

        json["s"]["x"].AsInt = position.x;
        json["s"]["y"].AsInt = position.y;
        json["t"].AsInt = (int)type;
        json["tx"] = textureData.ToJSON();

        return json;
    }
    public static bool operator != (BuildingTileData tile, BuildingTileData newTile)
    {
        return !(tile == newTile);
    }

    public static bool operator ==(BuildingTileData tile, BuildingTileData newTile)
    {
        try
        {
            if (tile.position != newTile.position)
            {
                return false;
            }
            if (tile.type != newTile.type)
            {
                return false;
            }
            if (tile.textureData != newTile.textureData)
            {
                return false;
            }

            if (tile.type == BuildingTileType.Furniture)
            {
                if (((BuildingTileFurnitureData)tile).objTextureData != ((BuildingTileFurnitureData)newTile).objTextureData)
                {
                    return false;
                }
                if (((BuildingTileFurnitureData)tile).face != ((BuildingTileFurnitureData)newTile).face)
                {
                    return false;
                }
            }

            if (tile.type == BuildingTileType.Painting)
            {
                if (((BuildingTilePaintingData)tile).objTextureData != ((BuildingTilePaintingData)newTile).objTextureData)
                {
                    return false;
                }
                if (((BuildingTilePaintingData)tile).face != ((BuildingTilePaintingData)newTile).face)
                {
                    return false;
                }
            }

            return true;
        }
        catch (System.Exception)
        {
            return false;
        }
    }
}

public class BuildingTileFloorData : BuildingTileData
{
    public BuildingTileFloorData() { }

    public BuildingTileFloorData(JSONNode data)
    {
        position = new Vector3Int(data["s"]["x"].AsInt, data["s"]["y"].AsInt, 0);
        type = BuildingTileType.Floor;
        textureData = new TextureData(data["tx"], TextureType.Floor);
    }
}

public class BuildingTileWallData : BuildingTileData
{
    public BuildingTileWallData() { }

    public BuildingTileWallData(JSONNode data)
    {
        position = new Vector3Int(data["s"]["x"].AsInt, data["s"]["y"].AsInt, 0);
        type = BuildingTileType.Wall;
        textureData = new TextureData(data["tx"], TextureType.Wall);
    }
}

public class BuildingTileFurnitureData : BuildingTileData
{
    public TextureData objTextureData;
    public BuildingObjectFace face;

    public BuildingTileFurnitureData() { }

    public BuildingTileFurnitureData(JSONNode data)
    {
        position = new Vector3Int(data["s"]["x"].AsInt, data["s"]["y"].AsInt, 0);
        type = BuildingTileType.Furniture;
        textureData = new TextureData(data["tx"], TextureType.Floor);
        objTextureData = new TextureData(data["ot"], TextureType.Furniture);
        face = (BuildingObjectFace)data["of"].AsInt;
    }

    public override JSONClass ToJSON()
    {
        JSONClass json = base.ToJSON();

        json["ot"] = objTextureData.ToJSON();
        json["of"].AsInt = (int)face;

        return json;
    }
}

public class BuildingTilePaintingData : BuildingTileData
{
    public TextureData objTextureData;
    public BuildingObjectFace face;

    public BuildingTilePaintingData() { }

    public BuildingTilePaintingData(JSONNode data)
    {
        position = new Vector3Int(data["s"]["x"].AsInt, data["s"]["y"].AsInt, 0);
        type = BuildingTileType.Painting;
        textureData = new TextureData(data["tx"], TextureType.Wall);
        objTextureData = new TextureData(data["ot"], TextureType.Painting);
        face = (BuildingObjectFace)data["of"].AsInt;
    }

    public override JSONClass ToJSON()
    {
        JSONClass json = base.ToJSON();

        json["ot"] = objTextureData.ToJSON();
        json["of"].AsInt = (int)face;

        return json;
    }
}
