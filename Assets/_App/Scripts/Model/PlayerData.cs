using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerData
{
    protected string username;
    protected string fullName;
    public string FullName
    {
        get
        {
            return fullName;
        }
    }

    protected List<PlayerNFTData> nftList;

    public List<PlayerNFTData> NFTList {
        get
        {
            return nftList;
        }
    }
}
