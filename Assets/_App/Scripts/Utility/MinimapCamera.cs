﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinimapCamera : MonoBehaviour
{
    [SerializeField] Player player;
    [SerializeField] float areaX;
    [SerializeField] float areaY;

    private void LateUpdate()
    {
        if (player == null)
            return;

        transform.position = new Vector3(
            Mathf.Clamp(player.transform.position.x, -areaX, areaX),
            Mathf.Clamp(player.transform.position.y, -areaY, areaY),
            transform.position.z);
    }

    public void InitPlayer(Player player)
    {
        this.player = player;
    }
}
