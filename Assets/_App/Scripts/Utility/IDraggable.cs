interface IDraggable
{
    void OnBeginDrag();
    void OnDrag();
    void OnEndDrag();
}