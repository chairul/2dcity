using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIToWorldObjectFollower : MonoBehaviour
{
    GameObject objectToFollow;

    public void SetObjectToFollow(GameObject objectToFollow)
    {
        this.objectToFollow = objectToFollow;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (isActiveAndEnabled)
        {
            if (objectToFollow != null)
            {
                transform.position = Camera.main.WorldToScreenPoint(objectToFollow.transform.position);
            }
        }
    }
}
