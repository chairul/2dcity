﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ExtensionMethod
{
    public static Vector2 GetCustomPivotPoint(this Sprite sprite)
    {
        float pivotX = -sprite.bounds.center.x / sprite.bounds.extents.x / 2 + 0.5f;
        float pivotY = -sprite.bounds.center.y / sprite.bounds.extents.y / 2 + 0.5f;
        Vector2 pivot = new Vector2(pivotX, pivotY);
        return pivot;
    }

    public static Color SetAlpha(this Color color, float opacity)
    {
        return new Color(color.r, color.g, color.b, opacity / 255f);
    }

    public static void RemoveAllChildren(this Transform transform, Action<GameObject> onDestroyed = null, params GameObject[] exception)
    {
        if (transform.childCount == 0)
            return;

        for (int i = transform.childCount - 1; i >= 0; i--)
        {
            if (exception.Contains(transform.GetChild(i).gameObject))
                continue;

            onDestroyed?.Invoke(transform.GetChild(i).gameObject);
            GameObject.Destroy(transform.GetChild(i).gameObject);
        }
    }

    public static Vector2 SizeToParent(this UnityEngine.UI.RawImage image, float padding = 0)
    {
        float w = 0, h = 0;
        var parent = image.GetComponentInParent<RectTransform>();
        var imageTransform = image.GetComponent<RectTransform>();

        // check if there is something to do
        if (image.texture != null)
        {
            if (parent == null) { return imageTransform.sizeDelta; } //if we don't have a parent, just return our current width;
            padding = 1 - padding;
            float ratio = image.texture.width / (float)image.texture.height;
            var bounds = new Rect(0, 0, parent.rect.width, parent.rect.height);
            if (Mathf.RoundToInt(imageTransform.eulerAngles.z) % 180 == 90)
            {
                //Invert the bounds if the image is rotated
                bounds.size = new Vector2(bounds.height, bounds.width);
            }
            //Size by height first
            h = bounds.height * padding;
            w = h * ratio;
            if (w > bounds.width * padding)
            { //If it doesn't fit, fallback to width;
                w = bounds.width * padding;
                h = w / ratio;
            }
        }
        imageTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, w);
        imageTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, h);
        return imageTransform.sizeDelta;
    }

    public static void DisableOverflowContent(this Transform transform, int count)
    {
        if (transform.childCount > count)
        {
            for (int i = count; i < transform.childCount; i++)
            {
                transform.GetChild(i).gameObject.SetActive(false);
            }
        }
    }

    public static void FillContentEach(this Transform transform, int count, Action<Transform> onCreated)
    {
        for (int i = 0; i < count; i++)
        {
            Transform t = null;

            if (i < transform.childCount)
            {
                t = transform.GetChild(i);
            }
            else
            {
                t = transform.GetChild(0).CloneTo(transform);
            }

            onCreated?.Invoke(t);
        }
    }

    public static Transform CloneTo(this Transform transform, Transform parent = null)
    {
        return GameObject.Instantiate(transform, parent);
    }

    public static void SetLayerRecursively(this GameObject obj, int layer)
    {
        obj.layer = layer;

        foreach (Transform child in obj.transform)
        {
            child.gameObject.SetLayerRecursively(layer);
        }
    }

    public static IEnumerable<T> GetEnumValues<T>()
    {
        return Enum.GetValues(typeof(T)).Cast<T>();
    }

    public static string ToDurationStringHMS(this TimeSpan span)
    {
        return span.ToString(@"hh\:mm\:ss");
    }

    public static string ToDurationStringMS(this TimeSpan span)
    {
        return span.ToString(@"mm\:ss");
    }

    public static void Shuffle<T>(this IList<T> list)
    {
        System.Random rng = new System.Random();
        int n = list.Count;
        while (n > 1)
        {
            n--;
            int k = rng.Next(n + 1);
            T value = list[k];
            list[k] = list[n];
            list[n] = value;
        }
    }
}