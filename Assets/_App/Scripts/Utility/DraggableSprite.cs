using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DraggableSprite : MonoBehaviour, IDraggable
{
    [Header("Draggable")]
    public bool draggable = true;
    public bool useOffset = true;

    protected bool isDragging;
    protected Vector3 offset;

    private void OnMouseDown()
    {
        if (draggable)
        {
            isDragging = true;
            offset = transform.position - Camera.main.ScreenToWorldPoint(Input.mousePosition);

            OnBeginDrag();
        }
    }

    private void OnMouseUp()
    {
        if (draggable)
        {
            isDragging = false;
            OnEndDrag();
        }
    }

    private void Update()
    {
        if (isDragging)
        {
            OnDrag();
        }
    }

    public virtual void OnBeginDrag()
    {

    }

    public virtual void OnDrag()
    {
        if (isDragging)
        {
            Vector3 position = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            transform.position = new Vector3(position.x + (useOffset ? offset.x : 0), 
                                            position.y + (useOffset ? offset.y : 0), 
                                            0);
        }
    }

    public virtual void OnEndDrag()
    {

    }
}
