using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using SimpleJSON;
using System.Linq;

public class MapEditorManager : MonoBehaviour
{
    [SerializeField] GameObject panelLoading;
    [SerializeField] Text textLoadingProgress;
    [SerializeField] Camera camera;
    [SerializeField] Joystick joystick;
    [SerializeField] float moveSpeed;
    [SerializeField] Slider zoomSlider;
    [SerializeField] float maxZoom;
    [SerializeField] float minZoom;
    [SerializeField] float defaultZoom;

    private void CheckMovement()
    {
        Vector2 movement = joystick.Direction != Vector2.zero ?
               joystick.Direction :
               new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));

        if (movement != Vector2.zero)
        {
            Vector3 pos = new Vector3(
                camera.transform.position.x + (movement.x * moveSpeed * Time.deltaTime),
                camera.transform.position.y + (movement.y * moveSpeed * Time.deltaTime),
                camera.transform.position.z);

            camera.transform.position = pos;
        }
    }

    public void OnZoomSliderValueChange()
    {
        camera.GetComponent<Camera>().orthographicSize = zoomSlider.value;
    }

    // Start is called before the first frame update
    void Start()
    {
        WorldObjectManager.LoadData();

        zoomSlider.minValue = minZoom;
        zoomSlider.maxValue = maxZoom;
        zoomSlider.value = defaultZoom;
        OnZoomSliderValueChange();

        ShowHidePanel(false);

        Load();
    }

    // Update is called once per frame
    void Update()
    {
        CheckMovement();
    }

    public enum EditorMode { None, Add, Remove }

    [SerializeField] GameObject editorPanel;
    [SerializeField] GameObject modePanel;
    [SerializeField] GameObject addModePanel;
    [SerializeField] GameObject removeModePanel;
    [SerializeField] Transform contentParent;
    [SerializeField] GameObject contentSample;
    [SerializeField] Grid grid;
    [SerializeField] Transform worldObjectParent;
    [SerializeField] GameObject worldObjectSample;

    bool isInEditMode;
    EditorMode editMode;
    string selectedItem;
    Dictionary<Vector3Int, WorldObject> placedObject;

    public void ShowHidePanel(bool isShowing)
    {
        isInEditMode = isShowing;
        editorPanel.SetActive(isShowing);
        SelectEditorMode(0);
    }

    public void SelectEditorMode(int mode)
    {
        editMode = (EditorMode)mode;

        modePanel.SetActive(false);
        addModePanel.SetActive(false);
        removeModePanel.SetActive(false);

        selectedItem = "";
        for (int i = 0; i < worldObjectParent.childCount; i++)
        {
            worldObjectParent.GetChild(i).GetComponent<WorldObject>().SetHighlight(false);
        }

        if (editMode == EditorMode.None)
        {
            modePanel.SetActive(true);
        }
        else if (editMode == EditorMode.Add)
        {
            addModePanel.SetActive(true);

            contentParent.RemoveAllChildren();
            contentSample.SetActive(false);

            foreach(string id in WorldObjectManager.worldObjects.Keys)
            {
                if (CollectionManager.GetAllWorldObject().ToList().Exists((x)=>x.name == id))
                {
                    MapEditorObjectItem item = Instantiate(contentSample, contentParent).GetComponent<MapEditorObjectItem>();
                    item.Init(id, SelectItem);

                    item.gameObject.SetActive(true);
                }
            }

        }
        else if (editMode == EditorMode.Remove)
        {
            removeModePanel.SetActive(true);

            for (int i = 0; i < worldObjectParent.childCount; i++)
            {
                worldObjectParent.GetChild(i).GetComponent<WorldObject>().SetHighlight(true);
            }
        }
    }

    void SelectItem(string id)
    {
        selectedItem = id;

        for (int i = 0; i < contentParent.childCount; i++)
        {
            contentParent.GetChild(i).GetComponent<MapEditorObjectItem>().Deselect();
        }
    }

    public void PutObjectInWorld()
    {
        if (editMode == EditorMode.Add && !string.IsNullOrEmpty(selectedItem))
        {
            Vector3Int position = new Vector3Int(
                grid.WorldToCell(Camera.main.ScreenToWorldPoint(Input.mousePosition)).x, 
                grid.WorldToCell(Camera.main.ScreenToWorldPoint(Input.mousePosition)).y, 0);

            WorldObjectData data = new WorldObjectData();
            data.CopyNewData(WorldObjectManager.worldObjects[selectedItem], position);
            CreateWorldObject(data);
        }
        else if (editMode == EditorMode.Remove)
        {
            Vector3Int position = new Vector3Int(
                grid.WorldToCell(Camera.main.ScreenToWorldPoint(Input.mousePosition)).x,
                grid.WorldToCell(Camera.main.ScreenToWorldPoint(Input.mousePosition)).y, 0);

            if (!placedObject.ContainsKey(position))
            {
                return;
            }

            placedObject[position].Delete();
        }
    }

    void OnWorldObjectDeleted(Vector3Int position)
    {
        placedObject.Remove(position);
    }

    public void Save()
    {
        StartCoroutine(Save_());
    }

    IEnumerator Save_()
    {
        panelLoading.SetActive(true);
        textLoadingProgress.text = "Saving World...";

        string path = Application.dataPath + "/world_objects.json";
        JSONClass data = new JSONClass();
        int index = 0;
        foreach (WorldObject worldObject in placedObject.Values)
        {
            data["data"][index] = worldObject.data.ToJSON();
            index++;

            if (index % 1 == 0)
            {
                textLoadingProgress.text = string.Format("Saving World...{0}", Mathf.FloorToInt(((float)index / (float)placedObject.Count) * 100));
                yield return null;
            }
        }
        File.WriteAllText(path, data["data"].ToString());

        yield return null;
        panelLoading.SetActive(false);
    }

    public void Load()
    {
        StartCoroutine(Load_());
    }

    IEnumerator Load_()
    {
        panelLoading.SetActive(true);
        textLoadingProgress.text = "Loading World...";

        string path = Application.dataPath + "/world_objects.json";

        placedObject = new Dictionary<Vector3Int, WorldObject>();

        if (File.Exists(path))
        {
            StreamReader reader = new StreamReader(path);
            string rawdata = reader.ReadToEnd();
            reader.Close();

            if (!string.IsNullOrEmpty(rawdata))
            {
                JSONNode data = JSON.Parse(rawdata);
                for (int i = 0; i < data.Count; i++)
                {
                    WorldObjectData worldObjectData = new WorldObjectData(data[i], true);
                    CreateWorldObject(worldObjectData);

                    if (i % 1 == 0)
                    {
                        textLoadingProgress.text = string.Format("Loading World...{0}", Mathf.FloorToInt(((float)i / (float)data.Count) * 100));
                        yield return null;
                    }
                }
            }
        }

        yield return null;
        panelLoading.SetActive(false);
    }

    void CreateWorldObject(WorldObjectData data)
    {
        if (placedObject.ContainsKey(data.tilePosition))
        {
            return;
        }

        WorldObject worldObject = Instantiate(worldObjectSample, worldObjectParent).GetComponent<WorldObject>();
        worldObject.Init(grid, data, OnWorldObjectDeleted);
        worldObject.gameObject.SetActive(true);

        placedObject.Add(data.tilePosition, worldObject);
    }
}
