using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MapEditorObjectItem : MonoBehaviour
{
    [SerializeField] Image frameImage;
    [SerializeField] Image iconImage;

    string id;
    System.Action<string> OnSelected;

    public void Init(string id, System.Action<string> OnSelected)
    {
        this.id = id;
        this.OnSelected = OnSelected;

        iconImage.sprite = CollectionManager.GetWorldObject(id);
        Deselect();
    }

    public void Select()
    {
        OnSelected?.Invoke(id);
        frameImage.color = Color.green;
    }

    public void Deselect()
    {
        frameImage.color = Color.white;
    }
}
