using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FullMapTest : MonoBehaviour
{
    [SerializeField] Vector3Int playerTilePos;
    [SerializeField] Transform fullMapPin;
    [SerializeField] Transform fullMapUpLeft;
    [SerializeField] Transform fullMapUpRight;
    [SerializeField] Transform fullMapDownLeft;

    // Start is called before the first frame update
    void Start()
    {
        Vector3 playerProjection = new Vector3((float)(Mathf.Abs(playerTilePos.x) + -23) / 1536f, (Mathf.Abs(playerTilePos.y) + -35) / 1024f, 0);
        Debug.LogError(playerTilePos);
        Debug.LogError(playerProjection);

        Vector3 mapWidthVector = fullMapUpRight.transform.position - fullMapUpLeft.transform.position;
        Vector3 mapHeightVector = fullMapUpLeft.transform.position - fullMapDownLeft.transform.position;
        Debug.LogError((mapHeightVector * playerProjection.y));
        Debug.LogError((mapWidthVector * playerProjection.x));

        //Vector3 playerPositionVector = fullMapUpLeft.transform.position + (mapHeightVector * playerProjection.y) + (mapWidthVector * playerProjection.x);

        Vector3 playerPositionVector = fullMapDownLeft.transform.position + (mapHeightVector * (1 - playerProjection.y)) + (mapWidthVector * (playerProjection.x));
        fullMapPin.transform.position = playerPositionVector;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
