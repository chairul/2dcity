using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using System.Linq;
using UnityEngine.UI;

public class MapDataRenderer : MonoBehaviour
{
    public static MapDataRenderer instance;
    public static bool isMapReady;
    public static System.Action OnRefresh;

    [Header("Map")]
    [SerializeField] Grid grid;
    [SerializeField] Tilemap dynamicMap;
    [SerializeField] Transform player;
    [SerializeField] GameObject tileColliderSample;
    [SerializeField] Transform worldObjectParent;
    [SerializeField] GameObject worldObjectSample;
    [SerializeField] Text gridPositionText;

    [Header("Building")]
    [SerializeField] GameObject buildingSample;
    [SerializeField] Transform buildingParent;

    public Grid Grid { get { return grid; } }
    public Vector3Int CurrentPlayerTile { get { return currentPlayerPos; } }

    bool isExtracted = false;
    Vector3Int prevPlayerPos;
    Vector3Int currentPlayerPos;
    int currentSkin;
    List<BuildingSpot> buildingSpots;
    Dictionary<Vector3Int, string> mapSpriteData;
    Dictionary<Vector3Int, GameObject> activeTileCollider;
    Dictionary<Vector3Int, WorldObject> worldObjects;

    List<Vector3Int> neighborOffset = new List<Vector3Int>() {
        Vector3Int.zero,
        new Vector3Int(0, 1, 0),
        new Vector3Int(0, -1, 0),

        new Vector3Int(-1, 0, 0),
        new Vector3Int(-1, 1, 0),
        new Vector3Int(-1, -1, 0),

        new Vector3Int(1, 0, 0),
        new Vector3Int(1, 1, 0),
        new Vector3Int(1, -1, 0),

        //new Vector3Int(-2, 0, 0),
        //new Vector3Int(-2, 1, 0),
        //new Vector3Int(-2, 2, 0),
        //new Vector3Int(-2, -1, 0),
        //new Vector3Int(-2, -2, 0),

        //new Vector3Int(2, 0, 0),
        //new Vector3Int(2, 1, 0),
        //new Vector3Int(2, 2, 0),
        //new Vector3Int(2, -1, 0),
        //new Vector3Int(2, -2, 0),

        //new Vector3Int(0, -2, 0),
        //new Vector3Int(1, -2, 0),
        //new Vector3Int(2, -2, 0),
        //new Vector3Int(-1, -2, 0),
        //new Vector3Int(-2, -2, 0),

        //new Vector3Int(0, 2, 0),
        //new Vector3Int(1, 2, 0),
        //new Vector3Int(2, 2, 0),
        //new Vector3Int(-1, 2, 0),
        //new Vector3Int(-2, 2, 0)
    };

    public void Init(Transform player)
    {
        this.player = player;
        StartCoroutine(Load());
    }

    IEnumerator Load()
    {
        isMapReady = false;
        yield return null;

        mapSpriteData = new Dictionary<Vector3Int, string>();
        activeTileCollider = new Dictionary<Vector3Int, GameObject>();
        worldObjects = new Dictionary<Vector3Int, WorldObject>();

        prevPlayerPos = Vector3Int.zero;
        currentSkin = -1;

        ChangeTileSeason();

        yield return null;

        isExtracted = true;

        foreach(WorldObjectData worldObjectData in MapDataLoader.instance.worldObjectData.Values)
        {
            WorldObject worldObject = Instantiate(worldObjectSample, worldObjectParent).GetComponent<WorldObject>();
            worldObject.Init(grid, worldObjectData, null);

            //fix offset manual pivoting
            Vector3 originPosition = grid.CellToWorld(worldObjectData.tilePosition);
            float xPosition = originPosition.x;
            float yPosition = originPosition.y + (worldObject.Sprite.size.y);
            worldObject.transform.position = new Vector3(xPosition, yPosition, 0);

            worldObjects.Add(worldObjectData.tilePosition, worldObject);
            yield return null;
        }

        int buildingCount = MapDataLoader.instance.buildingSpotData.Count;

        buildingSpots = new List<BuildingSpot>();
        for (int i = 0; i < buildingCount; i++)
        {
            BuildingSpotGeneratedData data = MapDataLoader.instance.buildingSpotData[i];

            BuildingSpot spot = Instantiate(buildingSample, buildingParent).GetComponent<BuildingSpot>();
            spot.Init(data);
            spot.transform.position = grid.CellToWorld(data.startPosition);

            buildingSpots.Add(spot);
            yield return null;
        }

        FirebaseManager.Instance.InitBuildingListener(buildingSpots);

        isMapReady = true;
    }

    void CreateTileOnPosition(Vector3Int position)
    {
        if (mapSpriteData.ContainsKey(position))
        {
            string spriteId = mapSpriteData[position];
            if (MapDataLoader.instance.mapSkinsData[currentSkin].ContainsKey(spriteId))
            {
                dynamicMap.SetTile(position, MapDataLoader.instance.mapSkinsData[currentSkin][spriteId]);
            }
            else
            {
                dynamicMap.SetTile(position, MapDataLoader.instance.mapSkinsData[currentSkin].First().Value);
            }
            dynamicMap.RefreshTile(position);

            if (DataManager.waterSpotSpriteCode.Contains(spriteId))
            {
                //dynamicCollideredMap.SetTile(position, MapDataLoader.instance.mapSkinsData[currentSkin][spriteId]);
                //dynamicCollideredMap.RefreshTile(position);
                AddTileCollider(position);
            }
        }
    }

    void CreateNewMap(bool isForce = false)
    {
        currentPlayerPos = grid.WorldToCell(player.position);

        int minDistance = MapDataLoader.instance.loadDistance / 3;

        if (isForce || Vector3Int.Distance(prevPlayerPos, currentPlayerPos) > minDistance)
        {
            int prevMinX = prevPlayerPos.x - MapDataLoader.instance.loadDistance;
            int prevMinY = prevPlayerPos.y - MapDataLoader.instance.loadDistance;
            int prevMaxX = prevPlayerPos.x + MapDataLoader.instance.loadDistance;
            int prevMaxY = prevPlayerPos.y + MapDataLoader.instance.loadDistance;
            int minX = currentPlayerPos.x - MapDataLoader.instance.loadDistance;
            int minY = currentPlayerPos.y - MapDataLoader.instance.loadDistance;
            int maxX = currentPlayerPos.x + MapDataLoader.instance.loadDistance;
            int maxY = currentPlayerPos.y + MapDataLoader.instance.loadDistance;

            //System.DateTime start = System.DateTime.Now;

            GetChunk(currentPlayerPos);

            for (int x = prevMinX; x < prevMaxX; x++)
            {
                for (int y = prevMinY; y < prevMaxY; y++)
                {
                    Vector3Int position = new Vector3Int(x, y, 0);

                    if (position.x <= minX || position.x >= maxX || position.y <= minY || position.y >= maxY)
                    {
                        ////Debug.LogError("Delete");
                        dynamicMap.SetTile(position, null);
                        //dynamicCollideredMap.SetTile(position, null);

                        RemoveTileCollider(position);
                    }
                }
            }

            for (int x = minX; x < maxX; x++)
            {
                for (int y = minY; y < maxY; y++)
                {
                    Vector3Int position = new Vector3Int(x, y, 0);

                    if (position.x <= prevMinX || position.x >= prevMaxX || position.y <= prevMinY || position.y >= prevMaxY)
                    {
                        //Debug.LogError("Create);
                        CreateTileOnPosition(position);
                    }
                }
            }

            //Debug.LogError((System.DateTime.Now - start).TotalMilliseconds);

            CheckWorldObjectActivation();

            prevPlayerPos = currentPlayerPos;

            OnRefresh?.Invoke();
        }
    }

    void GetChunk(Vector3Int position)
    {
        Vector3Int offset = MapDataLoader.instance.mapProperties.start;
        Vector3Int mapSector = currentPlayerPos / MapDataExtractor.chunkSize;

        for (int i = 0; i < neighborOffset.Count; i++)
        {
            Vector3Int targetChunk = new Vector3Int((mapSector.x + neighborOffset[i].x) * MapDataExtractor.chunkSize + offset.x, (mapSector.y + neighborOffset[i].y) * MapDataExtractor.chunkSize + offset.y, 0);
            if (mapSpriteData.ContainsKey(targetChunk))
            {
                continue;
            }

            TextAsset textData = Resources.Load<TextAsset>(string.Format("Map/{0}", targetChunk));
            if (textData == null)
            {
                //Debug.LogError(targetChunk);
                continue;
            }

            MapChunk chunk = new MapChunk(SimpleJSON.JSON.Parse(textData.text));
            foreach (Vector3Int pos in chunk.tiles.Keys)
            {
                mapSpriteData.Add(pos, chunk.tiles[pos]);
            }
        }
    }
    
    void AddTileCollider(Vector3Int position)
    {
        if (activeTileCollider.ContainsKey(position))
        {
            return;
        }

        bool isNeedCollider = false;
        for (int i = 0; i < neighborOffset.Count; i++)
        {
            if (!mapSpriteData.ContainsKey(position + neighborOffset[i]))
            {
                continue;
            }

            string spriteId = mapSpriteData[position + neighborOffset[i]];
            if (!DataManager.waterSpotSpriteCode.Contains(spriteId))
            {
                isNeedCollider = true;
                break;
            }
        }

        if (isNeedCollider)
        {
            Transform collider = Instantiate(tileColliderSample, dynamicMap.transform).transform;
            collider.transform.position = grid.CellToWorld(position);
            activeTileCollider.Add(position, collider.gameObject);
        }
    }

    void RemoveTileCollider(Vector3Int position)
    {
        if (!activeTileCollider.ContainsKey(position))
        {
            return;
        }

        Destroy(activeTileCollider[position]);
        activeTileCollider.Remove(position);
    }

    void ReloadMap()
    {
        currentPlayerPos = grid.WorldToCell(player.position);
        int minX = currentPlayerPos.x - MapDataLoader.instance.loadDistance;
        int minY = currentPlayerPos.y - MapDataLoader.instance.loadDistance;
        int maxX = currentPlayerPos.x + MapDataLoader.instance.loadDistance;
        int maxY = currentPlayerPos.y + MapDataLoader.instance.loadDistance;

        dynamicMap.ClearAllTiles();

        for (int x = minX; x < maxX; x++)
        {
            for (int y = minY; y < maxY; y++)
            {
                Vector3Int position = new Vector3Int(x, y, 0);
                CreateTileOnPosition(position);
            }
        }

    }

    public void ChangeTileSeason()
    {
        currentSkin = (currentSkin + 1) % MapDataLoader.instance.mapSkinsData.Count;
        ReloadMap();
        CreateNewMap(true);
    }

    public BuildingSpot GetBuildingSpot(string buildingId)
    {
        for (int i = 0; i < buildingSpots.Count; i++)
        {
            if (buildingSpots[i].spotData.id == buildingId)
            {
                return buildingSpots[i];
            }
        }

        return null;
    }

    public bool IsTileRendered(Vector3 position)
    {
        return dynamicMap.GetTile(grid.WorldToCell(position)) != null;
    }

    public bool IsTileRendered(Vector3Int position)
    {
        return dynamicMap.GetTile(position) != null;
    }

    void CheckWorldObjectActivation()
    {
        for (int i = 0; i < worldObjectParent.childCount; i++)
        {
            WorldObject worldObject = worldObjectParent.GetChild(i).GetComponent<WorldObject>();
            worldObject.gameObject.SetActive(IsTileRendered(worldObject.data.tilePosition));
        }
    }

    public Vector3Int GetTilePosition(Vector3 position)
    {
        return grid.WorldToCell(position);
    }

    public WorldObject GetWorldObject(Vector3Int position)
    {
        if (worldObjects.ContainsKey(position))
        {
            return worldObjects[position];
        }

        return null;
    }

    public void AddToWorldObject(WorldObject newObject)
    {
        if (!worldObjects.ContainsKey(newObject.data.tilePosition))
        {
            worldObjects.Add(newObject.data.tilePosition, newObject);
        }
    }

    public void RemoveFromWorldObject(Vector3Int position)
    {
        if (worldObjects.ContainsKey(position))
        {
            worldObjects.Remove(position);
        }
    }

    public void TeleportPlayerToPosition(Vector3Int gridPosition)
    {
        player.transform.position = grid.CellToWorld(gridPosition);
    }

    private void Awake()
    {
        instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        //Init(player);
    }

    // Update is called once per frame
    void Update()
    {
        if (isExtracted && player != null)
        {
            CreateNewMap();
            gridPositionText.text = string.Format("{0}, {1}", grid.WorldToCell(player.position).x, grid.WorldToCell(player.position).y);
        }

        //Debug.LogError(floorPool.Count);
    }
}
