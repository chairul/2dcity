﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ArsaUtil
{
    public static class ArsaLogger
    {
        public static void Log(object message)
        {
            #if DEBUG_MODE || UNITY_EDITOR
            Debug.Log(message);
            #endif
        }

        public static void Log(object message, Object context)
        {
            #if DEBUG_MODE || UNITY_EDITOR
            Debug.Log(message, context);
            #endif
        }

        public static void LogFormat(string format, params object[] args)
        {
            #if DEBUG_MODE || UNITY_EDITOR
            Debug.LogFormat(format, args);
            #endif
        }

        public static void LogWarning(object message)
        {
            #if DEBUG_MODE || UNITY_EDITOR
            Debug.LogWarning(message);
            #endif
        }

        public static void LogWarningFormat(string format, params object[] args)
        {
            #if DEBUG_MODE || UNITY_EDITOR
            Debug.LogWarningFormat(format, args);
            #endif
        }

        public static void LogError(object message)
        {
            #if DEBUG_MODE || UNITY_EDITOR
            Debug.LogError(message);
            #endif
        }

        public static void LogErrorFormat(string format, params object[] args)
        {
            #if DEBUG_MODE || UNITY_EDITOR
            Debug.LogErrorFormat(format, args);
            #endif
        }
    }
}
