using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using SimpleJSON;
using ArsaUtil;

public class BackendConnection : MonoBehaviour
{
    private System.Action<bool, string> OnRequestCompleted;
    private System.Action<bool, Texture2D, string> OnTextureRequestCompleted;
    private System.Action<bool, byte[], string> OnBytesRequestCompleted;
    private System.Action<bool, AudioClip, string> OnAudioClipRequestCompleted;

    private UnityWebRequest www;
    private string url;
    private int timeout;
    private DownloadMode downloadMode = DownloadMode.Text;
    private bool showLog = false;

    public string RequestURL => url;

    public BackendConnection ShowLog(bool showLog)
    {
        this.showLog = showLog;
        return this;
    }

    public void Abort()
    {
        if(www.result == UnityWebRequest.Result.InProgress)
        {
            www.Abort();
            StopAllCoroutines();
            Destroy(gameObject, 0.1f);
        }
    }

    private void post(
        string url,
        System.Action<bool, string> OnRequestCompleted,
        int timeout = 60,
        Dictionary<string, string> header = null,
        Dictionary<string, string> body = null)
    {
        this.url = url;
        this.timeout = timeout;
        this.OnRequestCompleted = OnRequestCompleted;

        JSONClass json = new JSONClass();

        if (body != null)
        {
            foreach (string key in body.Keys)
            {
                json.Add(key, body[key]);
            }
        }

        www = new UnityWebRequest(url, "POST");

        byte[] bodyRaw = System.Text.Encoding.UTF8.GetBytes(json.ToString());
        www.uploadHandler = (UploadHandler)new UploadHandlerRaw(bodyRaw);

        if (header != null)
        {
            foreach (string key in header.Keys)
            {
                www.SetRequestHeader(key, header[key]);
            }
        }

        if (showLog)
        {
            ArsaLogger.LogFormat("method : {0}\n" +
                "url : {1}\n" +
                "header : [Accept:{2},Content-Type:{3}]\n" +
                "body : {4}\n",
                www.method, www.url, www.GetRequestHeader("Accept"), www.GetRequestHeader("Content-Type"), json.ToString());
        }

        StartCoroutine(CallAPI());
        DontDestroyOnLoad(gameObject);
    }

    private void get(
        string url,
        System.Action<bool, string> OnRequestCompleted,
        int timeout = 60,
        Dictionary<string, string> header = null)
    {
        this.url = url;
        this.timeout = timeout;
        this.OnRequestCompleted = OnRequestCompleted;

        www = UnityWebRequest.Get(url);

        if (header != null)
        {
            foreach (string key in header.Keys)
            {
                www.SetRequestHeader(key, header[key]);
            }
        }

        if (showLog)
        {
            ArsaLogger.LogFormat("method : {0}\n" +
                "url : {1}\n" +
                "header : [Accept:{2},Content-Type:{3}]\n",
                www.method, www.url, www.GetRequestHeader("Accept"), www.GetRequestHeader("Content-Type"));
        }

        StartCoroutine(CallAPI());
        DontDestroyOnLoad(gameObject);
    }

    private void put(
        string url,
        System.Action<bool, string> OnRequestCompleted,
        int timeout = 60,
        Dictionary<string, string> header = null,
        Dictionary<string, string> body = null)
    {
        this.url = url;
        this.timeout = timeout;
        this.OnRequestCompleted = OnRequestCompleted;

        JSONClass json = new JSONClass();

        if (body != null)
        {
            foreach (string key in body.Keys)
            {
                json.Add(key, body[key]);
            }
        }

        byte[] bodyRaw = System.Text.Encoding.UTF8.GetBytes(json.ToString());
        www = UnityWebRequest.Put(url, bodyRaw);

        if (header != null)
        {
            foreach (string key in header.Keys)
            {
                www.SetRequestHeader(key, header[key]);
            }
        }

        if (showLog)
        {
            ArsaLogger.LogFormat("method : {0}\n" +
                "url : {1}\n" +
                "header : [Accept:{2},Content-Type:{3}]\n" +
                "body : {4}\n",
                www.method, www.url, www.GetRequestHeader("Accept"), www.GetRequestHeader("Content-Type"), json.ToString());
        }

        StartCoroutine(CallAPI());
        DontDestroyOnLoad(gameObject);
    }

    private void getTexture(
        string url,
        System.Action<bool, Texture2D, string> OnTextureRequestCompleted,
        int timeout = 60)
    {
        this.url = url;
        this.timeout = timeout;
        this.OnTextureRequestCompleted = OnTextureRequestCompleted;

        www = UnityWebRequestTexture.GetTexture(url);
        downloadMode = DownloadMode.Texture;

        StartCoroutine(CallAPI());
        DontDestroyOnLoad(gameObject);
    }

    private void getBytes(
        string url,
        System.Action<bool, byte[], string> OnBytesRequestCompleted,
        int timeout = 60)
    {
        this.url = url;
        this.timeout = timeout;
        this.OnBytesRequestCompleted = OnBytesRequestCompleted;

        www = UnityWebRequest.Get(url);
        downloadMode = DownloadMode.Bytes;

        StartCoroutine(CallAPI());
        DontDestroyOnLoad(gameObject);
    }

    private void getAudio(
        string url,
        AudioType audioType,
        System.Action<bool, AudioClip, string> OnAudioClipRequestCompleted,
        int timeout = 60)
    {
        this.url = url;
        this.timeout = timeout;
        this.OnAudioClipRequestCompleted = OnAudioClipRequestCompleted;

        www = UnityWebRequestMultimedia.GetAudioClip(url, audioType);
        downloadMode = DownloadMode.Audio;

        StartCoroutine(CallAPI());
        DontDestroyOnLoad(gameObject);
    }

    private IEnumerator CallAPI()
    {
        www.timeout = timeout;

        switch (downloadMode)
        {
            case DownloadMode.Text:
                www.downloadHandler = new DownloadHandlerBuffer();
                break;
            case DownloadMode.Texture:
                www.downloadHandler = new DownloadHandlerTexture();
                break;
            case DownloadMode.Bytes:
                break;
            case DownloadMode.Audio:
                break;
        }

        // Make request
        yield return www.SendWebRequest();

        switch (downloadMode)
        {
            case DownloadMode.Text:
                ParseData();
                break;
            case DownloadMode.Texture:
                ParseTexture();
                break;
            case DownloadMode.Bytes:
                ParseBytes();
                break;
            case DownloadMode.Audio:
                ParseAudio();
                break;
        }
    }

    private void ParseData()
    {
        bool isSuccess = false;
        string data = "";

        if (www.result == UnityWebRequest.Result.Success)
        {
            if (www.responseCode == 200)
            {
                isSuccess = true;
            }

            data = www.downloadHandler.text;
        }
        else
        {
            data = $"({www.responseCode}) {www.error}";
        }

        OnRequestCompleted?.Invoke(isSuccess, data);

        // Default debug
        if (showLog)
        {
            if (isSuccess)
            {
                ArsaLogger.LogFormat("({2}) Result from : {0}\n{1}", www.url, data, www.responseCode);
            }
            else
            {
                ArsaLogger.LogErrorFormat("({2}) Result from : {0}\n{1}", www.url, data, www.responseCode);
            }
        }

        Destroy(gameObject, 1f);
    }

    private void ParseTexture()
    {
        bool isSuccess = false;
        string message = string.Empty;
        Texture2D texture = null;

        if (www.result == UnityWebRequest.Result.Success)
        {
            isSuccess = true;
            texture = DownloadHandlerTexture.GetContent(www);
        }
        else
        {
            message = $"({www.responseCode}) {www.error}";
        }

        OnTextureRequestCompleted?.Invoke(isSuccess, texture, message);

        // Default debug
        if (showLog)
        {
            if (isSuccess)
            {
                ArsaLogger.Log("Texture downloaded. Source : " + www.url);
            }
            else
            {
                ArsaLogger.LogError("(" + www.responseCode + ") Error downloading texture. " + www.error);
            }
        }

        Destroy(gameObject, 1f);
    }

    private void ParseBytes()
    {
        bool isSuccess = false;
        string message = string.Empty;
        byte[] bytes = null;

        if (www.result == UnityWebRequest.Result.Success)
        {
            isSuccess = true;
            bytes = www.downloadHandler.data;
        }
        else
        {
            message = $"({www.responseCode}) {www.error}";
        }

        OnBytesRequestCompleted?.Invoke(isSuccess, bytes, message);

        // Default debug
        if (showLog)
        {
            if (isSuccess)
            {
                ArsaLogger.Log("Texture downloaded. Source : " + www.url);
            }
            else
            {
                ArsaLogger.LogError("(" + www.responseCode + ") Error downloading texture. " + www.error);
            }
        }

        Destroy(gameObject, 1f);
    }

    private void ParseAudio()
    {
        bool isSuccess = false;
        string message = string.Empty;
        AudioClip clip = null;

        if (www.result == UnityWebRequest.Result.Success)
        {
            isSuccess = true;
            clip = DownloadHandlerAudioClip.GetContent(www);
        }
        else
        {
            message = $"({www.responseCode}) {www.error}";
        }

        OnAudioClipRequestCompleted?.Invoke(isSuccess, clip, message);

        // Default debug
        if (showLog)
        {
            if (isSuccess)
            {
                ArsaLogger.Log("Audio downloaded. Source : " + www.url);
            }
            else
            {
                ArsaLogger.LogError("(" + www.responseCode + ") Error downloading audio. " + www.error);
            }
        }

        Destroy(gameObject, 1f);
    }

    public enum DownloadMode
    {
        Text, Texture, Audio, Bytes
    }

    #region Static

    public static BackendConnection POST(
        string url,
        System.Action<bool, string> OnRequestCompleted,
        int timeout = 60,
        Dictionary<string, string> header = null,
        Dictionary<string, string> body = null)
    {
        BackendConnection bc = new GameObject(typeof(BackendConnection).Name).AddComponent<BackendConnection>();
        bc.post(url, OnRequestCompleted, timeout, header, body);

        return bc;
    }

    public static BackendConnection GET(
        string url,
        System.Action<bool, string> OnRequestCompleted,
        int timeout = 60,
        Dictionary<string, string> header = null)
    {
        BackendConnection bc = new GameObject(typeof(BackendConnection).Name).AddComponent<BackendConnection>();
        bc.get(url, OnRequestCompleted, timeout, header);

        return bc;
    }

    public static BackendConnection PUT(
        string url,
        System.Action<bool, string> OnRequestCompleted,
        int timeout = 60,
        Dictionary<string, string> header = null,
        Dictionary<string, string> body = null)
    {
        BackendConnection bc = new GameObject(typeof(BackendConnection).Name).AddComponent<BackendConnection>();
        bc.put(url, OnRequestCompleted, timeout, header, body);

        return bc;
    }

    public static BackendConnection GetTexture(
        string url,
        System.Action<bool, Texture2D, string> OnTextureRequestCompleted,
        int timeout = 60)
    {
        BackendConnection bc = new GameObject(typeof(BackendConnection).Name).AddComponent<BackendConnection>();
        bc.getTexture(url, OnTextureRequestCompleted, timeout);

        return bc;
    }

    public static BackendConnection GetBytes(
        string url,
        System.Action<bool, byte[], string> OnBytesRequestCompleted,
        int timeout = 60)
    {
        BackendConnection bc = new GameObject(typeof(BackendConnection).Name).AddComponent<BackendConnection>();
        bc.getBytes(url, OnBytesRequestCompleted, timeout);

        return bc;
    }

    public static BackendConnection GetAudio(
        string url,
        AudioType audioType,
        System.Action<bool, AudioClip, string> OnAudioClipRequestCompleted,
        int timeout = 60)
    {
        BackendConnection bc = new GameObject(typeof(BackendConnection).Name).AddComponent<BackendConnection>();
        bc.getAudio(url, audioType, OnAudioClipRequestCompleted, timeout);

        return bc;
    }

    #endregion
}