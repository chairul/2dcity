using SimpleJSON;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TestStoreConnection : MonoBehaviour
{

    [SerializeField] InputField inputURL;
    [SerializeField] RawImage imageNFT;
    [SerializeField] Text textDescription;
    [SerializeField] Text textOwner;
    [SerializeField] Text textCreator;
    string viewUrl;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ImportNFTInfo()
    {
        if (!string.IsNullOrEmpty(inputURL.text))
        {
            string[] splitToken = inputURL.text.Split('/');
            string url = string.Format("https://api-v2-mainnet.paras.id/{0}/{1}/", splitToken[3], splitToken[4]);

            viewUrl = inputURL.text;
            //viewUrl = string.Format("https://paras.id/{0}/{1}/", splitToken[3], splitToken[4]);

            Debug.Log(url);

            BackendConnection.GET(url, HandleOnRequestCompleted);
        }
    }

    private void HandleOnRequestCompleted(bool isSuccess, string data)
    {
        //Debug.Log(isSuccess);
        //Debug.Log(data);

        JSONNode parsedData = JSON.Parse(data);

        textCreator.text = parsedData["creator_id"];
        textOwner.text = parsedData["owner_id"];

        textDescription.text = parsedData["metadata"]["description"];

        BackendConnection.GetTexture(parsedData["metadata"]["media"], HandleOnImageRetrieved);
    }

    private void HandleOnImageRetrieved(bool isSuccess, Texture data, string message)
    {
        imageNFT.texture = data;
    }

    public void ViewOnWeb()
    {
        Application.OpenURL(viewUrl);
    }
}
