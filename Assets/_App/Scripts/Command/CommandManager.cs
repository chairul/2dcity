using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CommandManager : MonoBehaviour
{
    private Stack<ICommand> undoCommands;
    private Stack<ICommand> redoCommands;

    public System.Action<ICommand> OnCommandExecuted;
    public System.Action OnUndo;
    public System.Action OnRedo;

    public bool UndoAvailable { get { return undoCommands.Count > 0; } }
    public bool RedoAvailable { get { return redoCommands.Count > 0; } }

    private void Awake()
    {
        undoCommands = new Stack<ICommand>();
        redoCommands = new Stack<ICommand>();
    }

    public void Execute(ICommand command)
    {
        command.Execute();
        undoCommands.Push(command);
        redoCommands.Clear();

        OnCommandExecuted?.Invoke(command);
    }

    public void Undo()
    {
        if (undoCommands.Count > 0)
        {
            redoCommands.Push(undoCommands.Peek());

            undoCommands.Peek().UnExecute();
            undoCommands.Pop();

            OnUndo?.Invoke();
        }
    }

    public void Redo()
    {
        if (redoCommands.Count > 0)
        {
            undoCommands.Push(redoCommands.Peek());

            redoCommands.Peek().Execute();
            redoCommands.Pop();

            OnRedo?.Invoke();
        }
    }
}
