using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EraseWallCommand : ICommand
{
    private Building building;
    private List<Vector3Int> tilePos;

    private List<BuildingWall> prevWall;
    private List<BuildingFloor> prevFloor;

    public EraseWallCommand(Building building, List<Vector3Int> tilePos)
    {
        this.building = building;
        this.tilePos = new List<Vector3Int>(tilePos);

        prevWall = new List<BuildingWall>();
        prevFloor = new List<BuildingFloor>();
        for (int i = 0; i < tilePos.Count; i++)
        {
            prevWall.Add(building.GetWallByPosition(tilePos[i]));
            prevFloor.Add(building.GetFloorByPosition(tilePos[i]));
        }
    }

    #region ICommand Members

    public void Execute()
    {
        for (int i = 0; i < tilePos.Count; i++)
        {
            building.DestroyWall(tilePos[i]);
            building.SpawnFloor(tilePos[i], prevFloor[i]);
        }
    }

    public void UnExecute()
    {
        for (int i = 0; i < tilePos.Count; i++)
        {
            building.DestroyFloor(tilePos[i]);
            building.SpawnWall(tilePos[i], prevWall[i], prevWall[i].TextureData);
        }
    }

    #endregion
}
