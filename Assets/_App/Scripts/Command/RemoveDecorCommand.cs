using UnityEngine;
using UnityEngine.UI;

public class RemoveDecorCommand : ICommand
{
    private Building building;
    private BuildingWall place;
    private Transform parentPainting;
    private System.Action<UIDecorItem, BuildingObject> OnFirstExecute;

    private BuildingObject obj;
    private UIDecorItem ui;

    public RemoveDecorCommand(Building building, BuildingWall place, Transform parentPainting, System.Action<UIDecorItem, BuildingObject> OnFirstExecute)
    {
        this.building = building;
        this.place = place;
        this.parentPainting = parentPainting;
        this.OnFirstExecute = OnFirstExecute;

        obj = building.GetDecorOnWall(place);

        if (obj.TextureData.textureSourceType == TextureSourceType.URL)
        {
            for (int i = 0; i < parentPainting.childCount; i++)
            {
                UIDecorItem ui = parentPainting.GetChild(i).GetComponent<UIDecorItem>();

                if(ui.Data.sourceURL == obj.TextureData.sourceURL)
                {
                    this.ui = ui;
                    break;
                }
            }
        }

        // replace with parent wall
        this.place = (BuildingWall)obj.place;
    }

    #region ICommand Members

    public void Execute()
    {
        foreach (var item in obj.paintingObject)
        {
            item.gameObject.SetActive(false);
        }

        building.RemoveDecorFromList(obj);

        if (ui == null)
        {
            ui = Object.Instantiate(parentPainting.GetChild(parentPainting.childCount - 1), parentPainting).GetComponent<UIDecorItem>();
            OnFirstExecute?.Invoke(ui, obj);
        }

        ui.ChangeQuantity(1);
    }

    public void UnExecute()
    {
        foreach (var item in obj.paintingObject)
        {
            item.gameObject.SetActive(true);
        }

        building.AddDecorToList(obj);
        ui.ChangeQuantity(-1);
    }

    #endregion
}
