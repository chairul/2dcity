using UnityEngine;
using UnityEngine.UI;

public class PlaceFurnCommand : ICommand
{
    private Building building;
    private BuildingFloor place;
    private UIDecorItem ui;

    private BuildingObject obj;
    public BuildingObject Representation { get { return obj; } }

    public PlaceFurnCommand(Building building, BuildingFloor place, UIDecorItem ui)
    {
        this.building = building;
        this.ui = ui;
        this.place = place;
    }

    #region ICommand Members

    public void Execute()
    {
        if (obj == null)
        {
            obj = building.AddDecor(place.transform, DecorPlacement.Floor);

            BuildingTileFurnitureData data = new BuildingTileFurnitureData();
            data.position = place.tilePosition;
            data.type = BuildingTileType.Furniture;
            data.textureData = place.TextureData;
            data.objTextureData = new TextureData(ui.Data);

            obj.Init(data, place, DataManager.PlayerUsername);
        }

        obj.gameObject.SetActive(true);

        building.AddDecorToList(obj);
        ui.GetComponent<Toggle>().SetIsOnWithoutNotify(false);
        ui.ChangeQuantity(-1);
    }

    public void UnExecute()
    {
        obj.gameObject.SetActive(false);

        building.RemoveDecorFromList(obj);
        ui.ChangeQuantity(1);
    }

    #endregion
}
