using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveFurnCommand : ICommand
{
    private Building building;
    private BuildingObject obj;
    private BuildingFloor place;
    private BuildingFloor prevPlace;

    public MoveFurnCommand(Building building, BuildingObject obj, BuildingFloor place, BuildingFloor prevPlace)
    {
        this.building = building;
        this.obj = obj;
        this.place = place;
        this.prevPlace = prevPlace;
    }

    #region ICommand Members

    public void Execute()
    {
        //place = building.FindNearestPossibleWall(place);
        obj.MoveToPlace(place);
        building.MoveDecorInList(obj, prevPlace.tilePosition);
    }

    public void UnExecute()
    {
        obj.MoveToPlace(prevPlace);
        building.MoveDecorInList(obj, place.tilePosition);
    }

    #endregion
}
