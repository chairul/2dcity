using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeFloorCommand : ICommand
{
    private Building building;
    private List<Vector3Int> tilePos;
    private UIDecorItem ui;

    private List<TextureData> prevTextureData = new List<TextureData>();

    public ChangeFloorCommand(Building building, List<Vector3Int> tilePos, UIDecorItem ui)
    {
        this.building = building;
        this.tilePos = new List<Vector3Int>(tilePos);
        this.ui = ui;

        for (int i = 0; i < tilePos.Count; i++)
        {
            TextureData textureData =
                building.GetFloorByPosition(tilePos[i]) != null ?
                new TextureData(building.GetFloorByPosition(tilePos[i]).TextureData) :
                new TextureData(0, "", TextureType.Floor);

            prevTextureData.Add(textureData);
        }
    }

    public void Execute()
    {
        for (int i = 0; i < tilePos.Count; i++)
        {
            building.ChangeFloor(tilePos[i], ui.Data);
        }
    }

    public void UnExecute()
    {
        for (int i = 0; i < tilePos.Count; i++)
        {
            building.ChangeFloor(tilePos[i], prevTextureData[i]);
        }
    }
}
