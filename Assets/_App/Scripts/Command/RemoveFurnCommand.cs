using UnityEngine;
using UnityEngine.UI;

public class RemoveFurnCommand : ICommand
{
    private Building building;
    private BuildingFloor place;
    private Transform parentFurniture;
    private System.Action<UIDecorItem, BuildingObject> OnFirstExecute;

    private BuildingObject obj;
    private UIDecorItem ui;

    public RemoveFurnCommand(Building building, BuildingFloor place, Transform parentFurniture, System.Action<UIDecorItem, BuildingObject> OnFirstExecute)
    {
        this.building = building;
        this.place = place;
        this.parentFurniture = parentFurniture;
        this.OnFirstExecute = OnFirstExecute;

        obj = building.GetDecorOnFloor(place);
    }

    #region ICommand Members

    public void Execute()
    {
        obj.gameObject.SetActive(false);
        building.RemoveDecorFromList(obj);

        if (ui == null)
        {
            ui = Object.Instantiate(parentFurniture.GetChild(parentFurniture.childCount - 1), parentFurniture).GetComponent<UIDecorItem>();
            OnFirstExecute?.Invoke(ui, obj);
        }

        ui.ChangeQuantity(1);
    }

    public void UnExecute()
    {
        obj.gameObject.SetActive(true);
        building.AddDecorToList(obj);
        ui.ChangeQuantity(-1);
    }

    #endregion
}
