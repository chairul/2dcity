using UnityEngine;
using UnityEngine.UI;

public class PlacePaintingCommand : ICommand
{
    private Building building;
    private BuildingWall place;
    private UIDecorItem ui;

    private BuildingObject obj;
    public BuildingObject Representation { get { return obj; } }

    public PlacePaintingCommand(Building building, BuildingWall place, UIDecorItem ui)
    {
        this.building = building;
        this.ui = ui;
        this.place = place;
    }

    #region ICommand Members

    public void Execute()
    {
        if (obj == null)
        {
            obj = building.AddDecor(place.transform, DecorPlacement.Wall);

            BuildingTilePaintingData data = new BuildingTilePaintingData();
            data.position = place.tilePosition;
            data.type = BuildingTileType.Painting;
            data.textureData = place.TextureData;
            data.objTextureData = new TextureData(ui.Data);
            data.face = place.Face;

            obj.Init(data, place, DataManager.PlayerUsername);
        }

        foreach (var item in obj.paintingObject)
        {
            item.gameObject.SetActive(true);
        }

        building.AddDecorToList(obj);
        ui.GetComponent<Toggle>().SetIsOnWithoutNotify(false);
        ui.ChangeQuantity(-1);
    }

    public void UnExecute()
    {
        foreach (var item in obj.paintingObject)
        {
            item.gameObject.SetActive(false);
        }

        building.RemoveDecorFromList(obj);
        ui.ChangeQuantity(1);
    }

    #endregion
}
