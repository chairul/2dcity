using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveDecorCommand : ICommand
{
    private Building building;
    private BuildingObject obj;
    private BuildingWall place;
    private BuildingWall prevPlace;

    public MoveDecorCommand(Building building, BuildingObject obj, BuildingWall place, BuildingWall prevPlace)
    {
        this.building = building;
        this.obj = obj;
        this.place = place;
        this.prevPlace = prevPlace;

        building.RemoveDecorFromList(obj);
    }

    #region ICommand Members

    public void Execute()
    {
        //place = building.FindNearestPossibleWall(place);
        obj.MoveToWall(place);
        building.MoveDecorInList(obj, prevPlace.tilePosition);
    }

    public void UnExecute()
    {
        obj.MoveToWall(prevPlace);
        building.MoveDecorInList(obj, place.tilePosition);
    }

    #endregion
}
