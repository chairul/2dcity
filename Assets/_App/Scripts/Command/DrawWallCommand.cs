using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawWallCommand : ICommand
{
    private Building building;
    private List<Vector3Int> tilePos;
    private UIDecorItem uiWall;

    private List<BuildingWall> prevWall;
    private List<BuildingFloor> prevFloor;

    public DrawWallCommand(Building building, List<Vector3Int> tilePos, UIDecorItem uiWall)
    {
        this.building = building;
        this.tilePos = new List<Vector3Int>(tilePos);
        this.uiWall = uiWall;

        prevFloor = new List<BuildingFloor>();
        for (int i = 0; i < tilePos.Count; i++)
        {
            prevFloor.Add(building.GetFloorByPosition(tilePos[i]));
        }
    }

    #region ICommand Members

    public void Execute()
    {
        for (int i = 0; i < tilePos.Count; i++)
        {
            building.DestroyFloor(tilePos[i]);
        }

        if (prevWall == null)
        {
            prevWall = new List<BuildingWall>();
            for (int i = 0; i < tilePos.Count; i++)
            {
                prevWall.Add(building.SpawnWall(tilePos[i], null, uiWall.Data));
            }
        }
        else
        {
            for (int i = 0; i < tilePos.Count; i++)
            {
                building.SpawnWall(tilePos[i], prevWall[i]);
            }
        }
    }

    public void UnExecute()
    {
        for (int i = 0; i < tilePos.Count; i++)
        {
            building.DestroyWall(tilePos[i]);
            building.SpawnFloor(tilePos[i], prevFloor[i]);
        }
    }

    #endregion
}