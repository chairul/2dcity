using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System;

public class ScenePersonalSpaceManager : MonoBehaviour
{
    public static BuildingData currentBuildingData = null;

    #region Fields & Properties

    [Header("Component - UI")]
    [SerializeField] PersonalSpaceUI ui;

    [Header("Component - Building")]
    [SerializeField] PersonalSpaceComp comp;

    [Header("Setting")]
    [SerializeField] PersonalSpaceSetting setting;

    [Header("Dummy")]
    [SerializeField] GameObject system;
    [SerializeField] TextAsset testData;
    [SerializeField] string metamaskUsername;

    private PSMode activeMode;
    private PSBuildMode activeBuildMode;

    private CommandManager commandManager;
    private UIDecorItem selectedWallUI;
    private UIDecorItem selectedDecorUI;
    private UIDecorItem selectedFloorUI;
    private UIDecorItem selectedFurnUI;
    private BuildingObject selectedDecor;
    private BuildingObject selectedFurn;

    private bool isDrawing;
    private List<Vector3Int> currentPosQueue = new List<Vector3Int>();
    private List<GameObject> tempObjs = new List<GameObject>();
    private bool isRemoveADecor;
    private List<TextureData> importedParasTextureData;

    #endregion

    #region Unity Callback

    private void Awake()
    {
        commandManager = GetComponent<CommandManager>();

        DataManager.Instance.LoadData();
        //if (!DataManager.Instance.playerData.ownedBuildingData.ContainsKey(currentBuildingId))
        //{
        //    DataManager.Instance.playerData.AddBuilding(testData.text);
        //}

        SetActiveMode(PSMode.None);
    }

    private void Start()
    {
        ShowLoading("Loading data...");

        if (MirrorManager.instance != null)
        {
            StartCoroutine(LoadBulding());
        }
        else
        {
            system.SetActive(true);
            currentBuildingData = new BuildingData("building_test", metamaskUsername, SimpleJSON.JSON.Parse(testData.text)["data"]);

            DataManager.playerUsername = metamaskUsername;
            ImportAll();

#if !UNITY_WEBGL
        FirebaseManager.Instance.Init();
#endif
        }
    }

    private void Update()
    {
        CheckMovement();
        CheckDrawing();
    }

    private void OnEnable()
    {
        commandManager.OnCommandExecuted += HandleOnCommandExecuted;
        commandManager.OnUndo += HandleOnUndoCommand;
        commandManager.OnRedo += HandleOnRedoCommand;

        ui.toggleBuild.onValueChanged.AddListener(ToggleModeBuild);
        ui.toggleDelete.onValueChanged.AddListener(ToggleModeDelete);

        for (int i = 0; i < ui.toggleBuildAll.Length; i++)
        {
            PSBuildMode mode = (PSBuildMode)i;
            ui.toggleBuildAll[i].onValueChanged.AddListener(delegate { SetActiveBuildMode(mode); });
        }
    }

    private void OnDisable()
    {
        commandManager.OnCommandExecuted -= HandleOnCommandExecuted;
        commandManager.OnUndo -= HandleOnUndoCommand;
        commandManager.OnRedo -= HandleOnRedoCommand;

        ui.toggleBuild.onValueChanged.RemoveAllListeners();
        ui.toggleDelete.onValueChanged.RemoveAllListeners();

        for (int i = 0; i < ui.toggleBuildAll.Length; i++)
        {
            ui.toggleBuildAll[i].onValueChanged.RemoveAllListeners();
        }
    }

#endregion

    #region Gameplay

    IEnumerator LoadBulding()
    {
        bool isWaiting = true;

        BuildingData data = null;
        FirebaseManager.Instance.GetBuilding(currentBuildingData.id,
            (string result) =>
            {
                isWaiting = false;

                SimpleJSON.JSONNode parsedData = SimpleJSON.JSON.Parse(result).AsObject;
                BuildingData buildingData = new BuildingData(currentBuildingData.id, parsedData["owner"], parsedData["data"]);
                data = buildingData;
            },
            () =>
            {
                SceneWorldManager.instance.ShowPopupInfo("Connection timed out. Please try again.");
            });

        yield return new WaitUntil(() => !isWaiting);

        if (data == null)
        {
            HideLoading();
            yield break;
        }
        else
        {
            currentBuildingData = data;
            LoadData();
        }
    }

    private void LoadData()
    {
        comp.building.SetCallback(HandleOnWallClicked, HandleOnFloorClicked);
        comp.building.LoadPS(currentBuildingData, comp.grid, Vector3Int.zero,
            () =>
            {
                SetupWall();
                SetupFloor();
                SetupPainting();
                SetupFurniture();
                HideLoading();
            });

        Camera.main.transform.position = new Vector3(0, currentBuildingData.size.y / 8, Camera.main.transform.position.z);

        ui.buttonUndo.interactable = false;
        ui.buttonRedo.interactable = false;
        ui.buttonSave.interactable = false;
        ui.buttonDismiss.interactable = false;
    }

    private void SetupWall()
    {
        comp.parentWallList.RemoveAllChildren();
        comp.sampleWallList.SetActive(true);

        Sprite[] availableWall = CollectionManager.GetAllWall();
        for (int i = 0; i < availableWall.Length; i++)
        {
            UIDecorItem uIDecorItem = Instantiate(comp.sampleWallList, comp.parentWallList).GetComponent<UIDecorItem>();
            uIDecorItem.Init(i, BuildingTileType.Wall, HandleOnWallSelected);
        }

        if (NFTItemManager.PlayerData != null)
        {
            foreach(TextureData data in NFTItemManager.Instance.NFTWalls.Values)
            {
                UIDecorItem uIDecorItem = Instantiate(comp.sampleWallList, comp.parentWallList.transform).GetComponent<UIDecorItem>();
                uIDecorItem.Init(data, BuildingTileType.Wall, HandleOnWallSelected);
            }
        }

        comp.sampleWallList.SetActive(false);
    }

    private void SetupFloor()
    {
        comp.parentFloorList.RemoveAllChildren();
        comp.sampleFloorList.SetActive(true);

        Sprite[] availableFloor = CollectionManager.GetAllFloor();
        for (int i = 0; i < availableFloor.Length; i++)
        {
            UIDecorItem uIDecorItem = Instantiate(comp.sampleFloorList, comp.parentFloorList).GetComponent<UIDecorItem>();
            uIDecorItem.Init(i, BuildingTileType.Floor, HandleOnFloorSelected);
        }

        if (NFTItemManager.PlayerData != null)
        {
            foreach (TextureData data in NFTItemManager.Instance.NFTFloors.Values)
            {
                UIDecorItem uIDecorItem = Instantiate(comp.sampleFloorList, comp.parentFloorList.transform).GetComponent<UIDecorItem>();
                uIDecorItem.Init(data, BuildingTileType.Floor, HandleOnFloorSelected);
            }
        }

        comp.sampleFloorList.SetActive(false);
    }

    private void SetupPainting()
    {
        comp.parentPaintingList.RemoveAllChildren();
        comp.samplePaintingList.SetActive(true);

        var allPaintings = new List<TextureData>();
        var spawnedPaintings = currentBuildingData.GetTilesByType(BuildingTileType.Painting);

        // add default painting
        for (int i = 0; i < CollectionManager.GetAllPainting().Length; i++)
        {
            // when already spawned
            if (spawnedPaintings.Exists(x => ((BuildingTilePaintingData)x).objTextureData.textureSourceType == TextureSourceType.Index && ((BuildingTilePaintingData)x).objTextureData.textureIndex == i))
            {
                continue;
            }

            allPaintings.Add(new TextureData(i, "", TextureType.Painting));
        }

        // add spawned painting
        //if (spawnedPaintings != null)
        //{
        //    foreach (var item in spawnedPaintings)
        //    {
        //        allPaintings.Add(((BuildingTilePaintingData)item).objTextureData);
        //    }
        //}

        for (int i = 0; i < allPaintings.Count; i++)
        {
            UIDecorItem uIDecorItem = Instantiate(comp.samplePaintingList, comp.parentPaintingList).GetComponent<UIDecorItem>();
            uIDecorItem.Init(i, BuildingTileType.Painting, HandleOnPaintingSelected);
        }

        if (NFTItemManager.PlayerData != null)
        {
            foreach (var data in NFTItemManager.PlayerData.NFTList)
            {
                if (data.TextureType == TextureType.Painting)
                {
                    if (NFTItemManager.Instance.NFTUsedData.ContainsKey(data.SourceURL))
                    {
                        if (data.quantityLeft <= 0)
                        {
                            continue;
                        }
                    }

                    UIDecorItem uIDecorItem = Instantiate(comp.samplePaintingList, comp.parentPaintingList.transform).GetComponent<UIDecorItem>();
                    uIDecorItem.Init(data, BuildingTileType.Painting, HandleOnPaintingSelected);
                }
            }
        }

#if !METAMASK
        if (importedParasTextureData != null)
        {
            for (int i = 0; i < importedParasTextureData.Count; i++)
            {
                UIDecorItem uIDecorItem = Instantiate(comp.samplePaintingList, comp.parentPaintingList.transform).GetComponent<UIDecorItem>();
                uIDecorItem.Init(importedParasTextureData[i], BuildingTileType.Painting, HandleOnPaintingSelected);
            }
        }
#endif

        comp.samplePaintingList.SetActive(false);
    }

    private void SetupFurniture()
    {
        comp.parentFurnitureList.RemoveAllChildren();
        comp.sampleFurnitureList.SetActive(true);

        var allFurnitures = new List<TextureData>();
        //var spawnedFurnitures = currentBuildingData.GetTilesByType(BuildingTileType.Furniture);

        // add default painting
        for (int i = 0; i < CollectionManager.GetAllFurniture().Length; i++)
        {
            // when already spawned
            //if (spawnedFurnitures.Exists(x => ((BuildingTileFurnitureData)x).objTextureData.textureSourceType == TextureSourceType.Index && ((BuildingTileFurnitureData)x).objTextureData.textureIndex == i))
            //{
            //    continue;
            //}

            allFurnitures.Add(new TextureData(i, "", TextureType.Furniture));
        }

        // add spawned furniture
        //if (spawnedPaintings != null)
        //{
        //    foreach (var item in spawnedPaintings)
        //    {
        //        allPaintings.Add(((BuildingTilePaintingData)item).objTextureData);
        //    }
        //}

        for (int i = 0; i < allFurnitures.Count; i++)
        {
            UIDecorItem uIDecorItem = Instantiate(comp.sampleFurnitureList, comp.parentFurnitureList).GetComponent<UIDecorItem>();
            uIDecorItem.Init(i, BuildingTileType.Furniture, HandleOnFurnitureSelected);
        }

        if (NFTItemManager.PlayerData != null)
        {
            foreach (var data in NFTItemManager.PlayerData.NFTList)
            {
                if (data.TextureType == TextureType.Furniture)
                {
                    if (NFTItemManager.Instance.NFTUsedData.ContainsKey(data.SourceURL))
                    {
                        if (data.quantityLeft <= 0)
                        {
                            continue;
                        }
                    }

                    UIDecorItem uIDecorItem = Instantiate(comp.sampleFurnitureList, comp.parentFurnitureList.transform).GetComponent<UIDecorItem>();
                    uIDecorItem.Init(data, BuildingTileType.Furniture, HandleOnFurnitureSelected);
                }
            }
        }

        comp.sampleFurnitureList.SetActive(false);
    }

    private void CheckMovement()
    {
        Vector2 movement = ui.joystick.Direction != Vector2.zero ?
               ui.joystick.Direction :
               new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));

        if (movement != Vector2.zero)
        {
            Vector3 pos = new Vector3(
                ui.mainCamera.transform.position.x + (movement.x * setting.CameraMoveSpeed * Time.deltaTime),
                ui.mainCamera.transform.position.y + (movement.y * setting.CameraMoveSpeed * Time.deltaTime),
                ui.mainCamera.transform.position.z);

            ui.mainCamera.transform.position = pos;
        }
    }

    private void CheckDrawing()
    {
        if (isDrawing)
        {
            if (Input.GetMouseButton(0))
            {
                DrawSomething();
            }

            if (Input.GetMouseButtonUp(0))
            {
                if (activeMode == PSMode.Build)
                {
                    switch (activeBuildMode)
                    {
                        case PSBuildMode.Wall:
                            CommitDrawWall();
                            break;
                        case PSBuildMode.Floor:
                            CommitChangeFloor();
                            break;
                    }
                }
                else if(activeMode == PSMode.Delete)
                {
                    switch (activeBuildMode)
                    {
                        case PSBuildMode.Wall:
                            CommitDeleteWall();
                            break;
                        case PSBuildMode.Floor:
                            CommitChangeFloor();
                            break;
                    }
                }

                isDrawing = false;
                isRemoveADecor = false;
            }
        }
        else
        {
            if (Input.GetMouseButtonDown(0))
            {
                if (UnityEngine.EventSystems.EventSystem.current.currentSelectedGameObject != null)
                    return;

                if (DrawSomething())
                {
                    isDrawing = true;
                }
            }
        }
    }

    private bool DrawSomething()
    {
        Vector3 position = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        var raycastHit = Physics2D.Raycast(position, Vector2.zero, 0f);

        if (raycastHit.collider != null && raycastHit.transform.root.GetComponent<Building>() != null)
        {
            if (raycastHit.transform.GetComponentInParent<BuildingWall>() != null)
            {
                if (activeMode == PSMode.Delete)
                {
                    if (!isDrawing)
                    {
                        if (comp.building.IsDecorExistOnWall(raycastHit.transform.GetComponentInParent<BuildingWall>()))
                        {
                            RemovePainting(raycastHit.transform.GetComponentInParent<BuildingWall>());
                            isRemoveADecor = true;
                            return true;
                        }
                        else
                        {
                            DeleteWall(raycastHit.transform.GetComponentInParent<BuildingWall>().tilePosition);
                            comp.building.SetHighlighWallByPaintingSize(0);
                            activeBuildMode = PSBuildMode.Wall;
                        }
                    }
                    else
                    {
                        if (isRemoveADecor)
                        {
                            return false;
                        }
                    }
                }

                HandleOnWallClicked(raycastHit.transform.GetComponentInParent<BuildingWall>());

                return true;
            }
            else if (raycastHit.transform.GetComponentInParent<BuildingFloor>() != null)
            {
                if (activeMode == PSMode.Delete)
                {
                    if (!isDrawing)
                    {
                        if (comp.building.IsDecorExistOnFloor(raycastHit.transform.GetComponentInParent<BuildingFloor>()))
                        {
                            RemoveFurniture(raycastHit.transform.GetComponentInParent<BuildingFloor>());
                            isRemoveADecor = true;
                            return true;
                        }
                        else
                        {
                            ChangeFloor(raycastHit.transform.GetComponentInParent<BuildingFloor>().tilePosition);
                            comp.building.SetHighlightFloor(true);
                            activeBuildMode = PSBuildMode.Floor;
                        }
                    }
                    else
                    {
                        if (isRemoveADecor)
                        {
                            return false;
                        }
                    }
                }

                HandleOnFloorClicked(raycastHit.transform.GetComponentInParent<BuildingFloor>());

                return true;
            }
        }

        return false;
    }

    public void Save()
    {
        ui.buttonSave.interactable = false;

        currentBuildingData = comp.building.GetCurrentData();

        if (FirebaseManager.Instance != null)
        {
            ShowLoading("Saving data...");
            ui.buttonDismiss.interactable = false;

            FirebaseManager.Instance.SetBuilding(currentBuildingData, (bool isSuccess) =>
            {
                if (isSuccess)
                {
                    
                }
                else
                {
                    ui.buttonDismiss.interactable = true;
                }

                HideLoading();
            });
        }
        else
        {

        }
    }

    public void Dismiss()
    {
        while (commandManager.UndoAvailable)
        {
            commandManager.Undo();
        }

        ui.buttonSave.interactable = false;
        ui.buttonDismiss.interactable = false;
    }

    public void GoToWorld()
    {
        if (MirrorManager.instance.isHostMode)
        {
            MirrorManager.instance.StartAsHost(null, null);
        }
        else
        {
            MirrorManager.instance.StartAsClient(DataManager.PlayerUsername, null, null);
        }
    }

    private void ToggleModeBuild(bool isOn)
    {
        if (isOn)
        {
            SetActiveMode(PSMode.Build);
            SetActiveBuildMode(activeBuildMode);
        }
        else
        {
            if (!ui.toggleGroupMode.AnyTogglesOn())
            {
                SetActiveMode(PSMode.None);
            }
        }
    }

    private void ToggleModeDelete(bool isOn)
    {
        if (isOn)
        {
            SetActiveMode(PSMode.Delete);
        }
        else
        {
            if (!ui.toggleGroupMode.AnyTogglesOn())
            {
                SetActiveMode(PSMode.None);
            }
        }
    }

    private void SetActiveMode(PSMode mode)
    {
        activeMode = mode;
        ui.panelBuild.SetActive(activeMode == PSMode.Build);

        if (comp.building.Data != null)
        {
            comp.building.SetHighlightWall(activeMode != PSMode.None, activeMode != PSMode.None);
            comp.building.SetHighlightFloor(activeMode != PSMode.None, activeMode != PSMode.None);
            comp.building.SetWallHeightMode(activeMode != PSMode.Build);
        }
    }

    public void ZoomIn()
    {
        ui.mainCamera.orthographicSize -= setting.CameraZoomStep;
        ui.buttonZoomIn.interactable = ui.mainCamera.orthographicSize > setting.CameraMinZoom;
        ui.buttonZoomOut.interactable = ui.mainCamera.orthographicSize < setting.CameraMaxZoom;
    }

    public void ZoomOut()
    {
        ui.mainCamera.orthographicSize += setting.CameraZoomStep;
        ui.buttonZoomIn.interactable = ui.mainCamera.orthographicSize > setting.CameraMinZoom;
        ui.buttonZoomOut.interactable = ui.mainCamera.orthographicSize < setting.CameraMaxZoom;
    }

#endregion

#region PSMode - Build

    private void SetActiveBuildMode(PSBuildMode mode)
    {
        activeBuildMode = mode;

        comp.building.SetHighlightWall(activeBuildMode == PSBuildMode.WallItem, activeMode != PSMode.Build);
        comp.building.SetHighlightFloor(activeBuildMode != PSBuildMode.WallItem, activeMode != PSMode.Build);
        comp.building.SetWallHeightMode(activeBuildMode != PSBuildMode.WallItem, activeBuildMode != PSBuildMode.WallItem);

        for (int i = 0; i < ui.panelBuildAll.Length; i++)
        {
            ui.panelBuildAll[i].SetActive(i == (int)activeBuildMode);
        }

        selectedDecor = null;
        selectedFurn = null;
    }

    private void DrawWall(Vector3Int tilePosition)
    {
        if (comp.building.IsInvalidTile(tilePosition) || currentPosQueue.Contains(tilePosition))
            return;

        currentPosQueue.Add(tilePosition);

        // preview
        BuildingWall wall = comp.building.SpawnWall(tilePosition, null, selectedWallUI.Data);
        wall.SetAlpha(100);
        tempObjs.Add(wall.gameObject);
    }

    private void CommitDrawWall()
    {
        if (currentPosQueue.Count == 0)
            return;

        for (int i = 0; i < tempObjs.Count; i++)
        {
            Destroy(tempObjs[i]);
        }
        tempObjs.Clear();

        DrawWallCommand command = new DrawWallCommand(
            comp.building,
            currentPosQueue,
            selectedWallUI);
        commandManager.Execute(command);
    }

    private void DeleteWall(Vector3Int tilePosition)
    {
        if (comp.building.IsInvalidTile(tilePosition) || 
            !comp.building.IsWallExist(tilePosition) || 
            (activeMode == PSMode.Delete && activeBuildMode != PSBuildMode.Wall) || 
            currentPosQueue.Contains(tilePosition))
            return;

        currentPosQueue.Add(tilePosition);

        // preview
        BuildingWall wall = comp.building.GetWallByPosition(tilePosition);
        wall.SetAlpha(100);
        tempObjs.Add(wall.gameObject);
    }

    private void CommitDeleteWall()
    {
        if (currentPosQueue.Count == 0)
            return;

        for (int i = 0; i < tempObjs.Count; i++)
        {
            tempObjs[i].GetComponent<BuildingWall>().SetAlpha(255);
        }
        tempObjs.Clear();

        EraseWallCommand command = new EraseWallCommand(
            comp.building,
            currentPosQueue);
        commandManager.Execute(command);
    }

    private void ChangeFloor(Vector3Int tilePosition)
    {
        BuildingFloor floor = comp.building.GetFloorByPosition(tilePosition);

        if (comp.building.IsInvalidTile(tilePosition) ||
            currentPosQueue.Contains(tilePosition) ||
            selectedFloorUI == null ||
            selectedFloorUI.Data == floor.TextureData ||
            (activeMode == PSMode.Delete && activeBuildMode != PSBuildMode.Floor))
            return;

        currentPosQueue.Add(tilePosition);

        // preview
        floor.SetHighlight(false);
        tempObjs.Add(floor.gameObject);
    }

    private void CommitChangeFloor()
    {
        if (currentPosQueue.Count == 0)
            return;

        for (int i = 0; i < tempObjs.Count; i++)
        {
            tempObjs[i].GetComponent<BuildingFloor>().SetHighlight(true);
        }
        tempObjs.Clear();

        ChangeFloorCommand command = new ChangeFloorCommand(
                            comp.building,
                            currentPosQueue,
                            selectedFloorUI);
        commandManager.Execute(command);
    }

    public void PlaceFurniture(BuildingFloor floor)
    {
        PlaceFurnCommand command = new PlaceFurnCommand(
            comp.building,
            floor,
            selectedFurnUI);
        commandManager.Execute(command);
    }

    private void PlacePainting(BuildingWall wall)
    {
        PlacePaintingCommand command = new PlacePaintingCommand(
            comp.building,
            wall,
            selectedDecorUI);
        commandManager.Execute(command);

        selectedDecorUI = null;
    }

    private void MovePainting(BuildingWall wall)
    {
        MoveDecorCommand command = new MoveDecorCommand(
            comp.building,
            selectedDecor,
            wall,
            (BuildingWall)selectedDecor.place);
        commandManager.Execute(command);
    }

    private void MoveFurniture(BuildingFloor floor)
    {
        if (selectedFurn.Data.position == floor.tilePosition)
            return;

        MoveFurnCommand command = new MoveFurnCommand(
            comp.building,
            selectedFurn,
            floor,
            (BuildingFloor)selectedFurn.place);
        commandManager.Execute(command);
    }

    private void RemovePainting(BuildingWall wall)
    {
        RemoveDecorCommand command = new RemoveDecorCommand(comp.building, wall, comp.parentPaintingList, HandleOnPaintingRemoved);
        commandManager.Execute(command);
    }

    public void RemoveFurniture(BuildingFloor floor)
    {
        RemoveFurnCommand command = new RemoveFurnCommand(comp.building, floor, comp.parentFurnitureList, HandleOnFurnitureRemoved);
        commandManager.Execute(command);
    }

    public void ImportAll()
    {
#if METAMASK
        ShowLoading("Importing NFTs...");
        NFTItemManager.Instance.Init(null, isSuccess =>
        {
            StartCoroutine(ImportProcess());
        });

        IEnumerator ImportProcess()
        {
            ShowLoading("Finishing import...");

            yield return new WaitForEndOfFrame();

            LoadData();
        }
#else
        ui.panelNFTLoader.SetActive(true);
#endif
    }

    public void LoadSource()
    {
        if (string.IsNullOrEmpty(ui.inputURL.text))
        {
            return;
        }

        ui.panelNFTLoader.SetActive(false);

        NFTViewer.LoadParas(DataManager.PlayerUsername, ui.inputURL.text, (TextureData result) =>
        {
            if (importedParasTextureData == null)
            {
                importedParasTextureData = new List<TextureData>();
            }

            importedParasTextureData.Add(result);
            SetupPainting();
        });
    }

    public void ShowLoading(string text)
    {
        ui.panelLoading.SetActive(true);
        ui.panelLoading.GetComponentInChildren<Text>().text = text;
    }

    public void HideLoading()
    {
        ui.panelLoading.SetActive(false);
    }

#endregion

#region Listener

    private void HandleOnCommandExecuted(ICommand command)
    {
        ui.buttonUndo.interactable = true;
        ui.buttonRedo.interactable = false;
        ui.buttonSave.interactable = true;
        ui.buttonDismiss.interactable = true;

        // specific needs
        if (command.GetType() == typeof(PlacePaintingCommand))
        {
            selectedDecor = ((PlacePaintingCommand)command).Representation;
        }
        else if(command.GetType() == typeof(PlaceFurnCommand))
        {
            selectedFurn = ((PlaceFurnCommand)command).Representation;
        }
        else if(command.GetType() == typeof(DrawWallCommand) || command.GetType() == typeof(ChangeFloorCommand) || command.GetType() == typeof(EraseWallCommand))
        {
            currentPosQueue.Clear();
        }
    }

    private void HandleOnUndoCommand()
    {
        ui.buttonRedo.interactable = true;
        ui.buttonUndo.interactable = commandManager.UndoAvailable;
        ui.buttonSave.interactable = true;
        ui.buttonDismiss.interactable = true;
    }

    private void HandleOnRedoCommand()
    {
        ui.buttonUndo.interactable = true;
        ui.buttonRedo.interactable = commandManager.RedoAvailable;
        ui.buttonSave.interactable = true;
        ui.buttonDismiss.interactable = true;
    }

    private void HandleOnWallClicked(BuildingWall wall)
    {
        Debug.Log("clicked wall : " + wall.tilePosition.ToString(), wall.gameObject);
        if (activeMode == PSMode.Build)
        {
            switch (activeBuildMode)
            {
                case PSBuildMode.Wall:
                    DrawWall(wall.tilePosition);
                    break;
                case PSBuildMode.WallItem:
                    if (selectedDecorUI != null && selectedDecor == null)
                    {
                        PlacePainting(wall);
                    }
                    else if (selectedDecor != null && selectedDecor.place.tilePosition != wall.tilePosition)
                    {
                        MovePainting(wall);
                    }
                    break;
                case PSBuildMode.Floor:

                    break;
                case PSBuildMode.FloorItem:

                    break;
            }
        }
        else if (activeMode == PSMode.Delete)
        {
            if (comp.building.IsDecorExistOnWall(wall))
            {
                //RemovePainting(wall);
            }
            else
            {
                DeleteWall(wall.tilePosition);
            }
        }
    }

    private void HandleOnFloorClicked(BuildingFloor floor)
    {
        Debug.Log("clicked floor : " + floor.tilePosition.ToString(), floor.gameObject);
        if (activeMode == PSMode.Build)
        {
            switch (activeBuildMode)
            {
                case PSBuildMode.Wall:
                    if (ui.toggleGroupWall.AnyTogglesOn())
                    {
                        DrawWall(floor.tilePosition);
                    }
                    break;
                case PSBuildMode.WallItem:
                    
                    break;
                case PSBuildMode.Floor:
                    if (selectedFloorUI != null)
                    {
                        ChangeFloor(floor.tilePosition);
                    }
                    break;
                case PSBuildMode.FloorItem:
                    if (selectedFurnUI != null && selectedFurn == null)
                    {
                        PlaceFurniture(floor);
                    }
                    else if (selectedFurn != null)
                    {
                        MoveFurniture(floor);
                    }
                    break;
            }
        }
        else if(activeMode == PSMode.Delete)
        {
            if (comp.building.IsDecorExistOnFloor(floor))
            {
                RemoveFurniture(floor);
            }
            else
            {
                selectedFloorUI = comp.parentFloorList.GetChild(0).GetComponent<UIDecorItem>();
                ChangeFloor(floor.tilePosition);
            }
        }
    }

    private void HandleOnWallSelected(UIDecorItem ui)
    {
        selectedWallUI = ui;

        // deselect
        if (selectedDecor != null)
        {
            selectedDecor = null;
        }
    }

    private void HandleOnPaintingSelected(UIDecorItem ui)
    {
        selectedDecorUI = ui;
        float width = ui.TextureSpriteData.texture.width;
        float height = ui.TextureSpriteData.texture.height;

        if (ui.TextureSpriteData.texture.height > DataManager.paintingDefaultHeight)
        {
            width = Mathf.FloorToInt(DataManager.paintingDefaultHeight / ui.TextureSpriteData.texture.height * ui.TextureSpriteData.texture.width);
            height = DataManager.paintingDefaultHeight;
        }

        comp.building.SetHighlighWallByPaintingSize(Mathf.CeilToInt(width / DataManager.paintingDefaultWidth));

        // deselect
        if (selectedDecor != null)
        {
            selectedDecor = null;
        }
    }

    private void HandleOnFloorSelected(UIDecorItem floor)
    {
        selectedFloorUI = floor;
        selectedFurnUI = null;
    }

    private void HandleOnFurnitureSelected(UIDecorItem furn)
    {
        selectedFurnUI = furn;
        selectedFloorUI = null;
        comp.building.SetHighlightFloor(true, false);

        // deselect
        if (selectedFurn != null)
        {
            selectedFurn = null;
        }
    }

    private void HandleOnPaintingRemoved(UIDecorItem ui, BuildingObject obj)
    {
        ui.Init(obj.TextureData, BuildingTileType.Painting, HandleOnPaintingSelected);
    }

    private void HandleOnFurnitureRemoved(UIDecorItem ui, BuildingObject obj)
    {
        ui.Init(obj.TextureData, BuildingTileType.Furniture, HandleOnFurnitureSelected);
    }

#endregion

    public enum PSMode { None, Build, Delete }
    public enum PSBuildMode { Wall, Floor, WallItem, FloorItem }

    [Serializable]
    class PersonalSpaceUI
    {
        public Camera mainCamera;
        public Joystick joystick;
        public Button buttonUndo;
        public Button buttonRedo;
        public Button buttonSave;
        public Button buttonDismiss;
        public Button buttonZoomIn;
        public Button buttonZoomOut;
        public Toggle toggleDelete;
        public Toggle toggleBuild;
        public GameObject panelBuild;
        public Toggle[] toggleBuildAll;
        public GameObject[] panelBuildAll;
        public ToggleGroup toggleGroupWall;
        public ToggleGroup toggleGroupMode;
        public GameObject panelLoading;
        public GameObject panelNFTLoader;
        public InputField inputURL;
    }

    [Serializable]
    class PersonalSpaceComp
    {
        public Grid grid;
        public Building building;
        public GameObject panelWallList;
        public Transform parentWallList;
        public GameObject sampleWallList;
        public GameObject panelPaintingList;
        public Transform parentPaintingList;
        public GameObject samplePaintingList;
        public GameObject panelFloorList;
        public Transform parentFloorList;
        public GameObject sampleFloorList;
        public GameObject panelFurnitureList;
        public Transform parentFurnitureList;
        public GameObject sampleFurnitureList;
    }
}
