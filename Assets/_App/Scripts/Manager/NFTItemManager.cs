using SimpleJSON;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NFTItemManager : Manager
{
    public static NFTItemManager Instance => Get<NFTItemManager>();

    static PlayerData playerData;

#if METAMASK
    public static PlayerUniqueOneData PlayerData
    {
        get
        {
            return (PlayerUniqueOneData)playerData;
        }
    }
#else
    public static PlayerParasData PlayerData
    {
        get
        {
            return (PlayerParasData)playerData;
        }
    }
#endif

    public List<string> ValidatedBuildingList { get; private set; }
    public Dictionary<string, int> NFTUsedData { get; private set; }

    public Dictionary<string, TextureData> NFTWalls { get; private set; }
    public Dictionary<string, TextureData> NFTFloors { get; private set; }
    public Dictionary<string, TextureData> NFTFurnitures { get; private set; }
    public Dictionary<string, TextureData> NFTPainting { get; private set; }
    public Dictionary<string, TextureData> NFTAvatarHeads { get; private set; }
    public Dictionary<string, TextureData> NFTAvatarHairs { get; private set; }
    public Dictionary<string, TextureData> NFTAvatarFaces { get; private set; }
    public Dictionary<string, TextureData> NFTAvatarTorsos { get; private set; }
    public Dictionary<string, TextureData> NFTAvatarArms { get; private set; }
    public Dictionary<string, TextureData> NFTAvatarHips { get; private set; }
    public Dictionary<string, TextureData> NFTAvatarThighs { get; private set; }
    public Dictionary<string, TextureData> NFTAvatarLegs { get; private set; }
    public Dictionary<string, TextureData> NFTAvatarShoes { get; private set; }

    private bool isLoadingData = false;

    private void Start()
    {
        if (ValidatedBuildingList == null)
        {
            ValidatedBuildingList = new List<string>();
        }
        if (NFTUsedData == null)
        {
            NFTUsedData = new Dictionary<string, int>();
        }
        if (NFTWalls == null)
        {
            NFTWalls = new Dictionary<string, TextureData>();
        }
        if (NFTFloors == null)
        {
            NFTFloors = new Dictionary<string, TextureData>();
        }
        if (NFTFurnitures == null)
        {
            NFTFurnitures = new Dictionary<string, TextureData>();
        }
        if (NFTPainting == null)
        {
            NFTPainting = new Dictionary<string, TextureData>();
        }
        if (NFTAvatarHeads == null)
        {
            NFTAvatarHeads = new Dictionary<string, TextureData>();
        }
        if (NFTAvatarHairs == null)
        {
            NFTAvatarHairs = new Dictionary<string, TextureData>();
        }
        if (NFTAvatarFaces == null)
        {
            NFTAvatarFaces = new Dictionary<string, TextureData>();
        }
        if (NFTAvatarTorsos == null)
        {
            NFTAvatarTorsos = new Dictionary<string, TextureData>();
        }
        if (NFTAvatarArms == null)
        {
            NFTAvatarArms = new Dictionary<string, TextureData>();
        }
        if (NFTAvatarHips == null)
        {
            NFTAvatarHips = new Dictionary<string, TextureData>();
        }
        if (NFTAvatarThighs == null)
        {
            NFTAvatarThighs = new Dictionary<string, TextureData>();
        }
        if (NFTAvatarLegs == null)
        {
            NFTAvatarLegs = new Dictionary<string, TextureData>();
        }
        if (NFTAvatarShoes == null)
        {
            NFTAvatarShoes = new Dictionary<string, TextureData>();
        }
    }

    public void Init(Text statusText, System.Action<bool> onFinished)
    {
        if (!isLoadingData)
        {
            isLoadingData = true;
            StartCoroutine(LoadAllNFT(statusText, onFinished));
        }
    }

    public void GetPlayer(string username, System.Action<PlayerData> OnFinished)
    {
        StartCoroutine(GetPlayerData(username, OnFinished));
    }

    IEnumerator GetPlayerData(string username, System.Action<PlayerData> OnFinished)
    {
        bool isWaiting = true;

#if METAMASK
        PlayerUniqueOneData player = null;

        string getURL = $"https://apidev.unique.one/getUserCollectedCollectibleList?userPublicAddress={username}&size=999";
        BackendConnection.GET(getURL, (isSuccess, result) =>
        {
            if (isSuccess)
            {
                JSONNode json = JSON.Parse(result);
                player = new PlayerUniqueOneData(json["data"]["result"][0]);
            }
            else
            {
                Debug.LogError(result);
            }

            isWaiting = false;
        }, 180);

        yield return new WaitUntil(() => !isWaiting);

        OnFinished?.Invoke(player);
#else
        yield return null;
        
        string nearFullname = username;
        string nearUsername = username;

        if (username.Contains("."))
        {
            nearFullname = username;
            nearUsername = nearFullname.Split('.')[0];
        }

        PlayerParasData player = new PlayerParasData(nearUsername, nearFullname);
        OnFinished?.Invoke(player);
#endif
    }

    IEnumerator LoadAllNFT(Text statusText, System.Action<bool> onFinished)
    {
        bool isWaiting = true;

        if (statusText != null)
        {
            statusText.text = "Getting Player Data ...";
        }

        StartCoroutine(GetPlayerData(DataManager.playerUsername, (PlayerData player) => {
            isWaiting = false;
            playerData = player;
        }));
        yield return new WaitUntil(() => !isWaiting);

        if (playerData == null)
        {
            isLoadingData = false;
            onFinished?.Invoke(false);
            yield break;
        }

        ValidatedBuildingList.Clear();
        NFTUsedData.Clear();
        NFTWalls.Clear();
        NFTFloors.Clear();
        NFTFurnitures.Clear();
        NFTPainting.Clear();
        NFTAvatarHeads.Clear();
        NFTAvatarHairs.Clear();
        NFTAvatarFaces.Clear();
        NFTAvatarTorsos.Clear();
        NFTAvatarArms.Clear();
        NFTAvatarHips.Clear();
        NFTAvatarThighs.Clear();
        NFTAvatarLegs.Clear();
        NFTAvatarShoes.Clear();

#if METAMASK
        int totalProcessCount = PlayerData.NFTList.Count;
        int maxProcess = 5;
        int processCount = 0;
        int currentProg = 0;

        foreach (var item in PlayerData.NFTList)
        {
            processCount++;
            currentProg++;
            if (statusText != null)
            {
                statusText.text = string.Format("Loading NFTs ... {0}%", Mathf.FloorToInt(((float)currentProg / (float)PlayerData.NFTList.Count) * 100));
            }

            TextureData textureData = new TextureData(item.SourceURL, item.imageURL, item.TextureType, item.imageType != NFTImageType.Static);
            TextureManager.Instance.GetTexture(textureData.type, textureData, (TextureManager.TextureSpriteData result) =>
            {
                TextureData[] textures = new TextureData[1];
                textures[0] = textureData;
                isWaiting = false;

                AddToList(item.SourceURL, item.TextureType, textureData);

                totalProcessCount--;
                processCount--;
            });

            if (processCount >= maxProcess)
            {
                yield return new WaitUntil(() => processCount < maxProcess);
            }
        }
        yield return new WaitUntil(() => totalProcessCount <= 0);

        if (statusText != null)
        {
            statusText.text = "Loading building ...";
        }

        List<string> buildingList = new List<string>();
        isWaiting = true;

        FirebaseManager.Instance.GetBuildingList(DataManager.PlayerUsername, result =>
        {
            if (result == null)
            {
                result = new List<string>();
            }

            buildingList = new List<string>(result);
            isWaiting = false;
        });

        yield return new WaitUntil(() => !isWaiting);

        totalProcessCount = buildingList.Count;
        maxProcess = 3;
        processCount = 0;
        currentProg = 0;

        foreach (var buildingId in buildingList)
        {
            processCount++;
            currentProg++;
            if (statusText != null)
            {
                statusText.text = $"Loading building ... {Mathf.FloorToInt(((float)currentProg / (float)buildingList.Count) * 100)}%";
            }

            FirebaseManager.Instance.GetBuilding(buildingId, data =>
            {
                if (!string.IsNullOrEmpty(data))
                {
                    JSONNode parsedData = JSON.Parse(data).AsObject;
                    BuildingData buildingData = new BuildingData(buildingId, parsedData["owner"], parsedData["data"]);
                    ValidateBuilding(buildingData);

                    BuildingData validData = new BuildingData(buildingData.ValidateData());

                    if (validData != null)
                    {
                        if (!buildingData.IsSameData(validData))
                        {
                            buildingData = new BuildingData(validData);

                            if (buildingData.owner == DataManager.PlayerUsername)
                            {
                                FirebaseManager.Instance.SetBuilding(validData, null);
                            }
                        }
                    }
                }

                totalProcessCount--;
                processCount--;
            });

            if (processCount >= maxProcess)
            {
                yield return new WaitUntil(() => processCount < maxProcess);
            }
        }

        yield return new WaitUntil(() => totalProcessCount <= 0);

        isLoadingData = false;
        onFinished?.Invoke(true);
#else
        isLoadingData = false;
        onFinished?.Invoke(true);
#endif
    }

    void AddToList(string id, TextureType type, TextureData data)
    {
        if (type == TextureType.Wall)
        {
            if (!NFTWalls.ContainsKey(id))
            {
                NFTWalls.Add(id, data);
            }
        }
        else if (type == TextureType.Floor)
        {
            if (!NFTFloors.ContainsKey(id))
            {
                NFTFloors.Add(id, data);
            }
        }
        else if (type == TextureType.Furniture)
        {
            if (!NFTFurnitures.ContainsKey(id))
            {
                NFTFurnitures.Add(id, data);
            }
        }
        else if (type == TextureType.Painting)
        {

            if (!NFTPainting.ContainsKey(id))
            {
                NFTPainting.Add(id, data);
            }
        }
        else if (type == TextureType.AvatarHead)
        {
            if (!NFTAvatarHeads.ContainsKey(id))
            {
                NFTAvatarHeads.Add(id, data);
            }
        }
        else if (type == TextureType.AvatarHair)
        {
            if (!NFTAvatarHairs.ContainsKey(id))
            {
                NFTAvatarHairs.Add(id, data);
            }
        }
        else if (type == TextureType.AvatarFace)
        {
            if (!NFTAvatarFaces.ContainsKey(id))
            {
                NFTAvatarFaces.Add(id, data);
            }
        }
        else if (type == TextureType.AvatarTop)
        {
            if (!NFTAvatarTorsos.ContainsKey(id))
            {
                NFTAvatarTorsos.Add(id, data);
            }
        }
        else if (type == TextureType.AvatarArm)
        {
            if (!NFTAvatarArms.ContainsKey(id))
            {
                NFTAvatarArms.Add(id, data);
            }
        }
        else if (type == TextureType.AvatarHip)
        {
            if (!NFTAvatarHips.ContainsKey(id))
            {
                NFTAvatarHips.Add(id, data);
            }
        }
        else if (type == TextureType.AvatarThigh)
        {
            if (!NFTAvatarThighs.ContainsKey(id))
            {
                NFTAvatarThighs.Add(id, data);
            }
        }
        else if (type == TextureType.AvatarLeg)
        {
            if (!NFTAvatarLegs.ContainsKey(id))
            {
                NFTAvatarLegs.Add(id, data);
            }
        }
        else if (type == TextureType.AvatarShoe)
        {
            if (!NFTAvatarShoes.ContainsKey(id))
            {
                NFTAvatarShoes.Add(id, data);
            }
        }
    }

    public Dictionary<string, TextureData> GetNFTList(TextureType type)
    {
        if (type == TextureType.Wall)
        {
            return NFTWalls;
        }
        else if (type == TextureType.Floor)
        {
            return NFTFloors;
        }
        else if (type == TextureType.Furniture)
        {
            return NFTFurnitures;
        }
        else if (type == TextureType.Painting)
        {
            return NFTPainting;
        }
        else if (type == TextureType.AvatarHead)
        {
            return NFTAvatarHeads;
        }
        else if (type == TextureType.AvatarHair)
        {
            return NFTAvatarHairs;
        }
        else if (type == TextureType.AvatarFace)
        {
            return NFTAvatarFaces;
        }
        else if (type == TextureType.AvatarTop)
        {
            return NFTAvatarTorsos;
        }
        else if (type == TextureType.AvatarArm)
        {
            return NFTAvatarArms;
        }
        else if (type == TextureType.AvatarHip)
        {
            return NFTAvatarHips;
        }
        else if (type == TextureType.AvatarThigh)
        {
            return NFTAvatarThighs;
        }
        else if (type == TextureType.AvatarLeg)
        {
            return NFTAvatarLegs;
        }
        else if (type == TextureType.AvatarShoe)
        {
            return NFTAvatarShoes;
        }

        return null;
    }

    public bool IsValidated(TextureData data, TextureType type)
    {
        if (data == null)
        {
            return true;
        }

        if (data.textureSourceType == TextureSourceType.Index)
        {
            return true;
        }

        if (type == TextureType.Wall)
        {
            return NFTWalls.ContainsKey(data.sourceURL);
        }
        else if (type == TextureType.Floor)
        {
            return NFTFloors.ContainsKey(data.sourceURL);
        }
        else if (type == TextureType.Furniture)
        {
            return NFTFurnitures.ContainsKey(data.sourceURL);
        }
        else if (type == TextureType.Painting)
        {
            return NFTPainting.ContainsKey(data.sourceURL);
        }
        else if (type == TextureType.AvatarHead)
        {
            return NFTAvatarHeads.ContainsKey(data.sourceURL);
        }
        else if (type == TextureType.AvatarHair)
        {
            return NFTAvatarHairs.ContainsKey(data.sourceURL);
        }
        else if (type == TextureType.AvatarFace)
        {
            return NFTAvatarFaces.ContainsKey(data.sourceURL);
        }
        else if (type == TextureType.AvatarTop)
        {
            return NFTAvatarTorsos.ContainsKey(data.sourceURL);
        }
        else if (type == TextureType.AvatarArm)
        {
            return NFTAvatarArms.ContainsKey(data.sourceURL);
        }
        else if (type == TextureType.AvatarHip)
        {
            return NFTAvatarHips.ContainsKey(data.sourceURL);
        }
        else if (type == TextureType.AvatarThigh)
        {
            return NFTAvatarThighs.ContainsKey(data.sourceURL);
        }
        else if (type == TextureType.AvatarLeg)
        {
            return NFTAvatarLegs.ContainsKey(data.sourceURL);
        }
        else if (type == TextureType.AvatarShoe)
        {
            return NFTAvatarShoes.ContainsKey(data.sourceURL);
        }

        return false;
    }

    public PlayerNFTData GetNFTData(string sourceURL)
    {
#if METAMASK
        PlayerNFTData result = PlayerData.NFTList.Find(x => x.SourceURL == sourceURL);

        if (result != null)
        {
            return result;
        }
        else
        {
            Debug.LogError($"Cannot find NFTData with url {sourceURL}");
            return PlayerData.NFTList[0];
        }
#else
        return null;
#endif
    }

    public void AddNFTUsed(string sourceURL, int count = 1)
    {
        if (string.IsNullOrEmpty(sourceURL))
        {
            return;
        }

        if (!NFTUsedData.ContainsKey(sourceURL))
        {
            NFTUsedData.Add(sourceURL, 0);
        }

        NFTUsedData[sourceURL] += count;
    }

    public void ValidateBuilding(BuildingData buildingData)
    {
        if (ValidatedBuildingList == null)
        {
            return;
        }

        if (ValidatedBuildingList.Contains(buildingData.id))
            return;

        ValidatedBuildingList.Add(buildingData.id);

        foreach (var item in buildingData.tiles.Values)
        {
            if(item.type == BuildingTileType.Painting)
            {
                AddNFTUsed(((BuildingTilePaintingData)item).objTextureData.sourceURL);
            }
            else if(item.type == BuildingTileType.Furniture)
            {
                AddNFTUsed(((BuildingTileFurnitureData)item).objTextureData.sourceURL);
            }
        }
    }
}
