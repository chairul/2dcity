using UnityEngine;

[DefaultExecutionOrder(-10)]
public class AspectRatioObjectSwitcher : MonoBehaviour
{
    [Header("Setting")]
    [SerializeField] protected GameObject[] landscapeObjs;
    [SerializeField] protected GameObject[] portraitObjs;

    private void Awake()
    {
        RefreshOritentation();
    }

    private void OnEnable()
    {
        AspectRatioManager.OnOrientationChanged += HandleOnOrientationChanged;
    }

    private void OnDisable()
    {
        AspectRatioManager.OnOrientationChanged -= HandleOnOrientationChanged;
    }

    public void RefreshOritentation()
    {
        bool isPortrait = AspectRatioManager.Instance.IsPortrait;

        for (int i = 0; i < landscapeObjs.Length; i++)
        {
            landscapeObjs[i].SetActive(!isPortrait);
        }

        for (int i = 0; i < portraitObjs.Length; i++)
        {
            portraitObjs[i].SetActive(isPortrait);
        }
    }

    private void HandleOnOrientationChanged()
    {
        RefreshOritentation();
    }
}