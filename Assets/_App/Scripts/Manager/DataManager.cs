﻿using UnityEngine;
using System.IO;
using SimpleJSON;
using System.Collections.Generic;

public class DataManager : Manager
{
    public enum Locale { EN = 0, ID = 1 };
    private static string playerFileName = "/player_data.bin";
    private static string deviceFileName = "/device_data.bin";
    private static bool isLoaded = false;

    public static bool isEncrypted = false;
    public static Locale locale = Locale.ID;
    public PlayerDataModel playerData;
    public DeviceDataModel deviceData;
    
    private static string playerName;
    public static string PlayerName
    {
        get
        {
            if (NFTItemManager.PlayerData != null)
            {
                return NFTItemManager.PlayerData.FullName;
            }
            else
            {
                return playerName;
            }
        }
        set
        {
            playerName = value;
        }
    }

    public static string playerUsername;
    public static string PlayerUsername
    {
        get
        {
            if (playerUsername.Contains("."))
            {
                return playerUsername.Split('.')[0];
            }
            else
            {
                return playerUsername;
            }
        }
    }
    public static Vector3 playerLastPosition;
    public static Vector3Int playerLasPositionGrid;
    public static bool isAutoWalk;
    public static bool isAnon;

    public readonly static List<string> buildingSpotSpriteCode = new List<string>() { "8", "12" };
    public readonly static List<string> waterSpotSpriteCode = new List<string>() { "6", "10", "14" };
    public readonly static float minimumBuildingDistance = 20f;
    public readonly static int paintingDefaultWidth = 32;
    public readonly static int paintingDefaultHeight = 120;
    public readonly static float defaultWallHigh = 1.76f;
    public readonly static float defaultWallLow = 0.5f;
    public readonly static float defaultAudioRange = 1f;
    public static float defaultWallDiff { get { return defaultWallHigh - defaultWallLow; } }

    protected override void OnAwake()
    {
        base.OnAwake();
        LoadData();
    }

    public void LoadData()
    {
        if (isLoaded)
            return;

        if (File.Exists(Application.persistentDataPath + playerFileName + ("_" + PlayerUsername)) /*&& !dontLoad*/)
        {
            /* === Read raw string from external file === */
            StreamReader reader = new StreamReader(Application.persistentDataPath + playerFileName + ("_" + PlayerUsername));
            string saveDataString = reader.ReadToEnd();
            reader.Close();

            if (isEncrypted)
            {
                saveDataString = DataEncryptor.DecryptData(saveDataString);
            }
            /* === === */

            /* === Parse string to json object === */
            JSONNode saveData = JSONNode.Parse(saveDataString);
            /* === === */

            /* === Extract game data from json object === */
            try
            {
                playerData = new PlayerDataModel(saveData);
            }
            catch (System.Exception)
            {
                Logger.LogErrorFormat("Parsing data error : ", saveData);
                playerData = new PlayerDataModel();
            }
            /* === === */
        }
        else
        {
            FileStream file = File.Create(Application.persistentDataPath + playerFileName + ("_" + PlayerUsername));
            file.Close();

            playerData = new PlayerDataModel();
            SavePlayerData();
        }

        if (File.Exists(Application.persistentDataPath + deviceFileName) /*&& !dontLoad*/)
        {
            /* === Read raw string from external file === */
            StreamReader reader = new StreamReader(Application.persistentDataPath + deviceFileName);
            string saveDataString = reader.ReadToEnd();
            reader.Close();

            if (isEncrypted)
            {
                saveDataString = DataEncryptor.DecryptData(saveDataString);
            }
            /* === === */

            /* === Parse string to json object === */
            JSONNode saveData = JSONNode.Parse(saveDataString);
            /* === === */

            /* === Extract game data from json object === */
            deviceData = new DeviceDataModel(saveData);
            /* === === */
        }
        else
        {
            FileStream file = File.Create(Application.persistentDataPath + deviceFileName);
            file.Close();

            deviceData = new DeviceDataModel();
            SaveDeviceData();
        }

        isLoaded = true;
    }

    public void SavePlayerData()
    {
        /* === Create save data structure === */
        string saveData = playerData.ToJSON().ToString();

        if (isEncrypted)
        {
            saveData = DataEncryptor.EncryptData(saveData);
        }
        /* === === */

        /* === Write data to external file === */
        StreamWriter writer = new StreamWriter(Application.persistentDataPath + playerFileName + ("_" + PlayerUsername));
        writer.Write(saveData);
        writer.Flush();
        writer.Close();
        /* === === */
    }

    public void SaveDeviceData()
    {
        /* === Create save data structure === */
        string saveData = deviceData.ToJSON().ToString();

        if (isEncrypted)
        {
            saveData = DataEncryptor.EncryptData(saveData);
        }
        /* === === */

        /* === Write data to external file === */
        StreamWriter writer = new StreamWriter(Application.persistentDataPath + deviceFileName);
        writer.Write(saveData);
        writer.Flush();
        writer.Close();
        /* === === */
    }

    #region Static

    public static DataManager Instance
    {
        get
        {
            return Get<DataManager>();
        }
    }

    #endregion
}