using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using System;
using System.Linq;
using UnityEngine.UI;
using FrostweepGames.VoicePro.NetworkProviders.Mirror;

public class MirrorManager : NetworkManager
{
    public static MirrorManager instance;

    public bool isHostMode;
    public Player myPlayer;
    public MirrorPlayer playerData;
    Action OnStarted;

    public List<MirrorPlayer> mirrorPlayers;
    public List<string> whitelistedPlayers;

    private void Awake()
    {
        Application.runInBackground = true;

        instance = this;
        DontDestroyOnLoad(gameObject);
    }

    public void ChangeConfigClient(string address, ushort portTCP, ushort portWEB)
    {
        networkAddress = address;
        Transport[] transports = ((MultiplexTransport)transport).transports;
        for (int i = 0; i < transports.Length; i++)
        {
            if (transports[i].GetType() == typeof(Mirror.TelepathyTransport))
            {
                ((TelepathyTransport)transports[i]).port = portTCP;
            }
            else if (transports[i].GetType() == typeof(Mirror.SimpleWeb.SimpleWebTransport))
            {
                ((Mirror.SimpleWeb.SimpleWebTransport)transports[i]).port = portWEB;
            }
        }
    }

    public void ChangeConfigServer(int maxPlayer, List<string> whitelistedPlayers, int maxAnon)
    {
        maxConnections = maxPlayer;
        maxWhitelistedConnections = whitelistedPlayers.Count;
        maxAnonConnections = maxAnon;
        currentWhitelistedConnectionns = 0;
        currentAnonConnectionns = 0;

        this.whitelistedPlayers = new List<string>(whitelistedPlayers);
    }

    public void SetMode(bool isHost, string nickname)
    {
        isHostMode = isHost;

        if (isHost)
        {
            nickname = "host0";
        }

        DataManager.playerUsername = nickname;
        DataManager.PlayerName = nickname;
    }

    public void StartAsServer(Text statusText, Action OnHostStarted)
    {
        this.OnStarted = OnHostStarted;
        StartServer();
    }

    public override void OnStartServer()
    {
        base.OnStartServer();
        OnStarted?.Invoke();

        Debug.Log("Server Started");
    }

    public void InitRegisterHandler()
    {
        NetworkServer.RegisterHandler<MirrorPlayer>(AddMirrorPlayer);
        NetworkServer.RegisterHandler<TransportVoiceMessage>(NetworkEventReceivedHandler);
    }

    public void StartAsHost(Text statusText, Action OnHostStarted)
    {
        this.OnStarted = OnHostStarted;
        StartCoroutine(StartingHost(statusText));

        InitRegisterHandler();
    }

    IEnumerator StartingHost(Text statusText)
    {
        if (statusText != null)
        {
            statusText.text = "Starting Host ...";
        }

        yield return null;
        StartHost();
    }

    public override void OnStartHost()
    {
        base.OnStartHost();
        OnStarted?.Invoke();
    }

    public void StartAsClient(string nickname, Text statusText, Action OnConnected)
    {
        this.OnStarted = OnConnected;
        StartCoroutine(StartingClient(nickname, statusText));
    }

    IEnumerator StartingClient(string nickname, Text statusText)
    {
        if (statusText != null)
        {
            statusText.text = "Starting Client ...";
        }

        yield return null;
        StartClient();
    }

    public override void OnStartClient()
    {
        base.OnStartClient();
        OnStarted?.Invoke();
    }

    public override void OnClientConnect(NetworkConnection conn)
    {
        base.OnClientConnect(conn);

        playerData = new MirrorPlayer(0, 0, DataManager.PlayerUsername, DataManager.PlayerName, DataManager.isAnon, AvatarManager.myAvatar.GetSaveData().ToString(), Vector3Int.zero);
        conn.Send(playerData);
    }

    public override void OnServerDisconnect(NetworkConnection conn)
    {
        base.OnServerDisconnect(conn);

        for (int i = 0; i < mirrorPlayers.ToList().Count; i++)
        {
            if (mirrorPlayers[i].connId == conn.connectionId)
            {
                if (mirrorPlayers[i].isAnon)
                {
                    currentAnonConnectionns--;
                }
                else
                {
                    currentWhitelistedConnectionns--;
                }

                mirrorPlayers.RemoveAt(i);
            }
        }
    }

    void AddMirrorPlayer(NetworkConnection conn, MirrorPlayer data)
    {
        if (mirrorPlayers == null)
        {
            mirrorPlayers = new List<MirrorPlayer>();
        }

        data.id = conn.connectionId;
        data.connId = conn.connectionId;

        bool isNeedKick = false;

#if METAMASK
        if (data.isAnon)
        {
            if (currentAnonConnectionns < maxAnonConnections)
            {
                currentAnonConnectionns++;
            }
            else
            {
                isNeedKick = true;
            }
        }
        else
        {
            if (whitelistedPlayers.Contains(data.username) && currentWhitelistedConnectionns < maxWhitelistedConnections)
            {
                currentWhitelistedConnectionns++;
            }
            else
            {
                isNeedKick = true;
            }
        }
#endif

        if (isNeedKick)
        {
            conn.Disconnect();
        }
        else
        {
            mirrorPlayers.Add(data);
        }
    }

    public override void OnClientDisconnect(NetworkConnection conn)
    {
        base.OnClientDisconnect(conn);
        UnityEngine.SceneManagement.SceneManager.LoadScene("Opening");
    }

    public MirrorPlayer GetPlayerById(int id)
    {
        if (!mirrorPlayers.Exists((x) => x.id == id))
        {
            return new MirrorPlayer(0, 0, "", "", false, new AvatarData().GetSaveData().ToString(), Vector3Int.zero);
        }
        return mirrorPlayers.Find((x) => x.id == id);
    }

    public MirrorPlayer GetPlayerByConnId(int id)
    {
        if (!mirrorPlayers.Exists((x) => x.connId == id))
        {
            return new MirrorPlayer(0, 0, "", "", false, new AvatarData().GetSaveData().ToString(), Vector3Int.zero);
        }
        return mirrorPlayers.Find((x) => x.connId == id);
    }

    public void SetPlayerNewId(int id, int newId)
    {
        for (int i = 0; i < mirrorPlayers.Count; i++)
        {
            if (mirrorPlayers[i].connId == id)
            {
                mirrorPlayers[i] = new MirrorPlayer(newId, mirrorPlayers[i].connId, mirrorPlayers[i].username, mirrorPlayers[i].nickname, mirrorPlayers[i].isAnon, mirrorPlayers[i].avatar, mirrorPlayers[i].objectToInteract);
            }
        }
    }

    public void SetBuildingToInteractOnPlayer(int id, string building)
    {
        for (int i = 0; i < mirrorPlayers.Count; i++)
        {
            if (mirrorPlayers[i].id == id)
            {
                mirrorPlayers[i] = new MirrorPlayer(id, mirrorPlayers[i].connId, mirrorPlayers[i].username, mirrorPlayers[i].nickname, mirrorPlayers[i].isAnon, mirrorPlayers[i].avatar, mirrorPlayers[i].objectToInteract, building);
            }
        }
    }

    public void SetInteractWithObjectStatus(int id, Vector3Int tilePosition)
    {
        for (int i = 0; i < mirrorPlayers.Count; i++)
        {
            if (mirrorPlayers[i].id == id)
            {
                mirrorPlayers[i] = new MirrorPlayer(id, mirrorPlayers[i].connId, mirrorPlayers[i].username, mirrorPlayers[i].nickname, mirrorPlayers[i].isAnon, mirrorPlayers[i].avatar, tilePosition, mirrorPlayers[i].buildingToInteract);
            }
        }
    }

    public void DisconnectFromGame()
    {
        if (mode == NetworkManagerMode.Host)
        {
            StopHost();
        }
        else if(mode == NetworkManagerMode.ClientOnly)
        {
            StopClient();
        }
    }

    public void SendChat(string chat)
    {
        if (myPlayer.isLocalPlayer)
        {
            myPlayer.CommandSendChat(myPlayer.data.id, myPlayer.data.nickname, chat);
        }
    }

    public void InteractWithObject(Vector3Int objectId)
    {
        if (myPlayer.isLocalPlayer)
        {
            myPlayer.CommandInteractWithObject(myPlayer.data.id, objectId);
        }
    }

    public void StopInteractWithObject()
    {
        if (myPlayer.isLocalPlayer)
        {
            myPlayer.CommandStopInteractWithObject(myPlayer.data.id);
        }
    }

    private void NetworkEventReceivedHandler(NetworkConnection connection, TransportVoiceMessage message)
    {
        NetworkServer.SendToReady(message);
    }
}

[Serializable]
public struct MirrorPlayer : NetworkMessage
{
    public int id;
    public int connId;
    public string username;
    public string nickname;
    public bool isAnon;
    public string avatar;
    public Vector3Int objectToInteract;
    public string buildingToInteract;

    public bool IsInteractingWithObject
    {
        get
        {
            return objectToInteract != Vector3Int.zero;
        }
    }

    public MirrorPlayer(int id, int connId, string username, string nickname, bool isAnon, string avatar, Vector3Int objectToInteract, string buildingToInteract = "")
    {
        this.id = id;
        this.connId = connId;
        this.username = username;
        this.nickname = nickname;
        this.isAnon = isAnon;
        this.avatar = avatar;
        this.objectToInteract = objectToInteract;
        this.buildingToInteract = buildingToInteract;
    }

    public AvatarData GetAvatarData()
    {
        return new AvatarData(SimpleJSON.JSON.Parse(avatar));
    }
}
