using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class SceneOpeningManager : MonoBehaviour
{
    [SerializeField] bool hostMode;
    [SerializeField] GameObject modePanel;
    [SerializeField] GameObject hostPanel;
    [SerializeField] GameObject clientPanel;
    [SerializeField] Transform canvas;
    [SerializeField] Text statusText;
    [SerializeField] GameObject loadingPanel;
    [SerializeField] Text loadingText;

    [Header("Client")]
    [SerializeField] GetURLParameter getUrlParameter;
    [SerializeField] InputField nameInput;
    [SerializeField] InputField addressInput;
    [SerializeField] Button joinButton;
    [SerializeField] GameObject clientInputNamePanel;
    [SerializeField] GameObject clientInputAddressPanel;
    [SerializeField] GameObject metamaskLoginButton;
    [SerializeField] GameObject nearLoginButton;

    [Header("DEV")]
    [SerializeField] GameObject devPanel;
    [SerializeField] InputField devInput;
    [SerializeField] KeyCode devKeyCode;
    [SerializeField] int devTapCount;
    [SerializeField] float devTapInterval;
    private int devCurrentTapCount;
    private float devLastTapTime;

    private void Awake()
    {
        metamaskLoginButton.SetActive(false);
        nearLoginButton.SetActive(false);
        nameInput.characterLimit = 99;
        nameInput.readOnly = false;

#if METAMASK
        metamaskLoginButton.SetActive(true);
#else
        nearLoginButton.SetActive(true);
#endif

#if UNITY_WEBGL
        DataManager.playerLasPositionGrid = getUrlParameter.GetPositionFromParam();
#endif

#if UNITY_WEBGL && !METAMASK
        nameInput.text = getUrlParameter.GetNEARId();
        nameInput.characterLimit = 0;
        nameInput.readOnly = true;
#elif !UNITY_WEBGL && !METAMASK
        nameInput.text = "rionear.near";
        nameInput.characterLimit = 0;
        nameInput.readOnly = true;
#endif
    }

    private void OnEnable()
    {
        nameInput.onEndEdit.AddListener(OnNameEditDone);
    }

    private void OnDisable()
    {
        nameInput.onEndEdit.RemoveAllListeners();
    }

    IEnumerator LoadAvatar(System.Action OnDone)
    {
#if METAMASK
        statusText.text = "Logging in ...";
        if (!DataManager.isAnon)
        {
            bool isWaitingData = true;
            bool isLoadingSuccess = false;

            NFTItemManager.Instance.Init(statusText, (isSuccess) =>
            {
                isLoadingSuccess = isSuccess;

                if (isSuccess)
                {
                    isWaitingData = false;
                }
            });

            yield return new WaitUntil(() => !isWaitingData);

            if (!isLoadingSuccess)
            {
                statusText.text = "Failed to Connect to Server";
                yield break;
            }
        }
#endif
        statusText.text = "Getting Avatar Data ...";

        bool isWaitingAvatar = true;
        bool isAvatarExist = false;

        if (!DataManager.isAnon)
        {
            FirebaseManager.Instance.GetAvatar(DataManager.PlayerUsername, (string data) =>
            {
                isWaitingAvatar = false;
                if (data == null)
                {
                    AvatarManager.SetMyAvatar(null);

                    isAvatarExist = false;
                }
                else
                {
                    if (string.IsNullOrEmpty(data))
                    {
                        isAvatarExist = false;
                        return;
                    }

                    AvatarManager.SetMyAvatar(SimpleJSON.JSON.Parse(data), true);

                    isAvatarExist = true;
                }
            });

            yield return new WaitForSeconds(1);
            yield return new WaitUntil(() => !isWaitingAvatar);
            yield return new WaitForSeconds(1);
        }
        else
        {
            AvatarManager.SetMyAvatar(null);

            isAvatarExist = false;
        }

        yield return null;

        if (!isAvatarExist)
        {
            SceneAvatarManager.avatarId = DataManager.PlayerUsername;
            UnityEngine.SceneManagement.SceneManager.LoadScene("Avatar");
            yield break;
        }

        OnDone?.Invoke();
    }

    public void OnNameEditDone(string name)
    {
        joinButton.interactable = !string.IsNullOrEmpty(name);
    }

    void SetButtonActivation(bool isAllowed)
    {
        canvas.GetComponent<CanvasGroup>().interactable = isAllowed;
    }

    private void Start()
    {
        OnNameEditDone(nameInput.text);

        SetHostMode(false);

        DataManager.isAnon = false;

#if UNITY_EDITOR
        if (ParrelSync.ClonesManager.GetArgument().Contains("host"))
        {
            StartCoroutine(LoadConfigServer(() =>
            {
                StartCoroutine(LoadConfigClient(() =>
                {
                    ForceJoin();
                }));
            }));
        }
        else
        {
            StartCoroutine(LoadConfigClient(() =>
            {

            }));
        }
#elif UNITY_SERVER
        StartCoroutine(LoadConfigServer(() =>
        {
            ForceJoin();
        }));
#else
        StartCoroutine(LoadConfigClient(() =>
        {

        }));
#endif
    }

    private void Update()
    {
        if (Input.GetKeyDown(devKeyCode))
        {
            devCurrentTapCount++;

            if (devCurrentTapCount == devTapCount)
            {
                OpenDevMode();
                devLastTapTime = 0;
            }

            devLastTapTime = Time.time;
        }

        // Reset
        if (devCurrentTapCount > 0 && Time.time - devLastTapTime > devTapInterval)
        {
            devCurrentTapCount = 0;
        }
    }

    IEnumerator LoadConfigClient(System.Action OnDone)
    {
        loadingPanel.SetActive(true);
        loadingText.text = "Reading Config File ...";

        string path = Application.streamingAssetsPath + "/config.json";
        string data = "";

#if !UNITY_WEBGL
        if (File.Exists(path))
        {
            FileStream file = new FileStream(path, FileMode.Open, FileAccess.Read);
            StreamReader reader = new StreamReader(file);

            data = reader.ReadToEnd();
            reader.Close();
            file.Close();
            yield return null;
        }
#else
        path = "StreamingAssets/config.json";
        UnityEngine.Networking.UnityWebRequest webReq = UnityEngine.Networking.UnityWebRequest.Get(path);
        yield return webReq.SendWebRequest();
        data = webReq.downloadHandler.text;
#endif

        if (!string.IsNullOrEmpty(data))
        {
            SimpleJSON.JSONNode configData = SimpleJSON.JSON.Parse(data);
            string serverAddress = configData["server_address"];
            int serverPortTCP = configData["server_port_tcp"].AsInt;
            int serverPortWEB = configData["server_port_web"].AsInt;

            MirrorManager.instance.ChangeConfigClient(serverAddress, (ushort)serverPortTCP, (ushort)serverPortWEB);
        }

        loadingPanel.SetActive(false);

        OnDone?.Invoke();
    }

    IEnumerator LoadConfigServer(System.Action OnDone)
    {
        loadingPanel.SetActive(true);
        loadingText.text = "Reading Config File ...";

        string path = Application.streamingAssetsPath + "/server_config.json";
        string data = "";

#if !UNITY_WEBGL
        if (File.Exists(path))
        {
            FileStream file = new FileStream(path, FileMode.Open, FileAccess.Read);
            StreamReader reader = new StreamReader(file);

            data = reader.ReadToEnd();
            reader.Close();
            file.Close();
            yield return null;
        }
#else
        path = "StreamingAssets/server_config.json";
        UnityEngine.Networking.UnityWebRequest webReq = UnityEngine.Networking.UnityWebRequest.Get(path);
        yield return webReq.SendWebRequest();
        data = webReq.downloadHandler.text;
#endif

        if (!string.IsNullOrEmpty(data))
        {
            SimpleJSON.JSONNode configData = SimpleJSON.JSON.Parse(data);
            int maxPlayer = configData["max_player"].AsInt;
            List<string> whitelistedPlayers = new List<string>();
            for (int i = 0; i < configData["whitelisted_player"].Count; i++)
            {
                whitelistedPlayers.Add(configData["whitelisted_player"][i]);
            }
            int maxAnon = configData["max_anon"].AsInt;

            MirrorManager.instance.ChangeConfigServer(maxPlayer, whitelistedPlayers, maxAnon);
        }

        loadingPanel.SetActive(false);

        OnDone?.Invoke();
    }

    public void SetHostMode(bool isHost)
    {
        hostMode = isHost;
        Init();
    }

    public void OpenSelectMode()
    {
        modePanel.SetActive(true);
        hostPanel.SetActive(false);
        clientPanel.SetActive(false);
        statusText.text = "Select Mode";
    }

    public void OpenDevMode()
    {
        devPanel.SetActive(true);
        clientPanel.SetActive(false);
    }

    public void CloseDevMode()
    {
        devPanel.SetActive(false);
        clientPanel.SetActive(true);
    }

    private void Init()
    {
        modePanel.SetActive(false);
        hostPanel.SetActive(false);
        clientPanel.SetActive(false);

        if (hostMode)
        {
            hostPanel.SetActive(true);
            statusText.text = "Start the Server";
        }
        else
        {
            clientPanel.SetActive(true);
            statusText.text = "";
            clientInputNamePanel.SetActive(true);
            clientInputAddressPanel.SetActive(false);
        }
    }

    public void StartServer()
    {
        MirrorManager.instance.SetMode(true, "");
        MirrorManager.instance.StartAsServer(statusText, () =>
        {

        });
    }

    public void StartHost()
    {
        MirrorManager.instance.SetMode(true, "");

        SetButtonActivation(false);

        StartCoroutine(LoadAvatar(() =>
        {
            MirrorManager.instance.StartAsHost(statusText, () =>
            {

            });
        }));
    }

    public void Join()
    {
#if UNITY_EDITOR
        if (ParrelSync.ClonesManager.GetArgument().Contains("host"))
        {
            StartHost();
        }
        else
        {
            StartClient();
        }
#elif UNITY_SERVER
        StartServer();
#else
        StartClient();
#endif
    }

    public void ForceJoin()
    {
        double unixTimestamp = System.DateTime.UtcNow.Subtract(new System.DateTime(1970, 1, 1)).TotalSeconds;
        nameInput.text = string.Format("anon{0}", unixTimestamp.ToString("0"));
        DataManager.isAnon = true;
        Join();
    }

    public void OnMetamaskLoggedIn(string accountName)
    {
        nameInput.text = accountName;
    }

    void StartClient()
    {
        if (string.IsNullOrEmpty(nameInput.text))
        {
            return;
        }

        MirrorManager.instance.SetMode(false, nameInput.text);

        SetButtonActivation(false);

        StartCoroutine(LoadAvatar(() =>
        {
            MirrorManager.instance.StartAsClient(DataManager.PlayerUsername, statusText, () =>
            {

            });
        }));
    }

    public void DevLogin()
    {
        if (string.IsNullOrEmpty(devInput.text))
        {
            return;
        }

        nameInput.text = devInput.text;
        Join();
    }

    public void OnToggleAutoWalkChanged(bool isOn)
    {
        DataManager.isAutoWalk = isOn;
    }
}
