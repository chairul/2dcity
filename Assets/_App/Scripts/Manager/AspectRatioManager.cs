using System.Collections;
using UnityEngine;

public class AspectRatioManager : Manager
{
    public static AspectRatioManager Instance => Get<AspectRatioManager>();

    private bool isPortrait = false;
    public bool IsPortrait => isPortrait;

    public static System.Action OnOrientationChanged;

    protected override void OnAwake()
    {
#if UNITY_WEBGL
        OrientationSwitcher.ListenForBrowserResize(gameObject.name, "HandleOnBrowserResize");

        try
        {
            if (bool.Parse(OrientationSwitcher.GetBrowserOrientation()))
            {
                ToggleOrientation();
            }
        }
        catch
        {
            Debug.LogError($"Error parsing {OrientationSwitcher.GetBrowserOrientation()}");
        }
#endif
    }

    private void Update()
    {
#if UNITY_EDITOR
        if (Input.GetKeyUp(KeyCode.X))
        {
            ToggleOrientation();
        }
#endif
    }

    public void SetOrientation(bool isPortrait)
    {
        if (this.isPortrait != isPortrait)
        {
            this.isPortrait = isPortrait;
            OnOrientationChanged?.Invoke();
        }
        else
        {
            this.isPortrait = isPortrait;
        }
    }

    public void ToggleOrientation()
    {
        isPortrait = !isPortrait;
        OnOrientationChanged?.Invoke();

#if UNITY_WEBGL
        OrientationSwitcher.BrowserToggleOrientation(isPortrait.ToString());
#endif
    }

    private void HandleOnBrowserResize(string strPortrait)
    {
        try
        {
            isPortrait = bool.Parse(strPortrait);
            OnOrientationChanged?.Invoke();
        }
        catch
        {
            Debug.LogError($"Failed to parse {strPortrait}");
        }
    }
}