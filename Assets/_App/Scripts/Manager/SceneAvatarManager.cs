using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SceneAvatarManager : MonoBehaviour
{
    public static string avatarId;

    [SerializeField] Button headButton;
    [SerializeField] Button facedButton;
    [SerializeField] Button topButton;
    [SerializeField] Button bottomButton;
    [SerializeField] Transform contentParent;
    [SerializeField] GameObject subCatTitleSample;
    [SerializeField] GameObject avatarItemParentSample;
    [SerializeField] GameObject avatarItemSample;
    [SerializeField] GameObject avatarSample;
    [SerializeField] Transform avatarSpot;
    [SerializeField] Button doneButton;
    [SerializeField] Text nameText;
    [SerializeField] Button importButton;
    [SerializeField] GameObject panelLoading;
    [SerializeField] Text textLoading;

    AvatarItemType currentType;
    Avatar avatar;

    public void SelectCategory(int category)
    {
        currentType = (AvatarItemType)category;
        LoadCategory();

        headButton.interactable = currentType != AvatarItemType.Head;
        facedButton.interactable = currentType != AvatarItemType.Face;
        topButton.interactable = currentType != AvatarItemType.Top;
        bottomButton.interactable = currentType != AvatarItemType.Bottom;
    }

    void LoadCategory()
    {
        contentParent.RemoveAllChildren();
        subCatTitleSample.SetActive(false);
        avatarItemParentSample.SetActive(false);
        avatarItemSample.SetActive(false);

        List<AvatarItemData> items = new List<AvatarItemData>(AvatarManager.GetAvatarItemByType(currentType));

        for (int i = 0; i < AvatarManager.avatarCategories[currentType].category.Count; i++)
        {
            Text categoryTitle = Instantiate(subCatTitleSample, contentParent).GetComponent<Text>();
            categoryTitle.text = AvatarManager.avatarCategories[currentType].category[i];
            categoryTitle.gameObject.SetActive(true);

            Transform itemParent = Instantiate(avatarItemParentSample, contentParent).transform;
            itemParent.gameObject.SetActive(true);
            for (int j = 0; j < items.Count; j++)
            {
                if (items[j].subCategory == i)
                {
                    UIAvatarItem avatarItem = Instantiate(avatarItemSample, itemParent).GetComponent<UIAvatarItem>();
                    TextureData data = new TextureData(0, items[j].id, TextureData.ToTextureType(items[j].id, items[j].type));
                    avatarItem.Init(data, OnAvatarItemSelected);

                    avatarItem.gameObject.SetActive(true);
                }
            }

            if (NFTItemManager.PlayerData != null)
            {
                TextureType type = TextureType.Wall;

                if (currentType == AvatarItemType.Head)
                {
                    if (i == 0)
                    {
                        type = TextureType.AvatarHead;
                    }
                    else if (i == 1)
                    {
                        type = TextureType.AvatarHair;
                    }
                }
                else if (currentType == AvatarItemType.Face)
                {
                    if (i == 0)
                    {
                        type = TextureType.AvatarFace;
                    }
                }
                else if (currentType == AvatarItemType.Top)
                {
                    if (i == 0)
                    {
                        type = TextureType.AvatarTop;
                    }
                    else if (i == 1)
                    {
                        type = TextureType.AvatarArm;
                    }
                }
                else if (currentType == AvatarItemType.Bottom)
                {
                    if (i == 0)
                    {
                        type = TextureType.AvatarHip;
                    }
                    else if (i == 1)
                    {
                        type = TextureType.AvatarThigh;
                    }
                    else if (i == 2)
                    {
                        type = TextureType.AvatarLeg;
                    }
                    else if (i == 3)
                    {
                        type = TextureType.AvatarShoe;
                    }
                }

                if (type != TextureType.Wall)
                {
                    Dictionary<string, TextureData> nftItems = new Dictionary<string, TextureData>(NFTItemManager.Instance.GetNFTList(type));
                    foreach (TextureData data in nftItems.Values)
                    {
                        UIAvatarItem avatarItem = Instantiate(avatarItemSample, itemParent).GetComponent<UIAvatarItem>();
                        avatarItem.Init(data, OnAvatarItemSelected);

                        avatarItem.gameObject.SetActive(true);
                    }
                }
            }
        }

        StartCoroutine(Refresh());
    }

    void OnAvatarItemSelected(TextureData data)
    {
        avatar.Apply(data);
    }

    IEnumerator Refresh()
    {
        yield return null;
        contentParent.GetComponent<ContentSizeFitter>().enabled = false;
        yield return null;
        contentParent.GetComponent<ContentSizeFitter>().enabled = true;
    }

    public void Save()
    {
        doneButton.interactable = false;

        if (!DataManager.isAnon)
        {
            FirebaseManager.Instance.SetAvatar(avatarId, AvatarManager.myAvatar.GetSaveData().ToString(), (bool isSuccess) =>
            {
                if (isSuccess)
                {
                    LoadWorld();
                }
            });
        }
        else
        {
            LoadWorld();
        }
    }

    public void Import()
    {
        panelLoading.SetActive(true);
        NFTItemManager.Instance.Init(textLoading, isSuccess =>
        {
            panelLoading.SetActive(false);
            LoadCategory();
        });
    }

    void LoadWorld()
    {
#if UNITY_EDITOR
        if (ParrelSync.ClonesManager.GetArgument().Contains("host"))
        {
            MirrorManager.instance.StartAsHost(null, null);
        }
        else
        {
            MirrorManager.instance.StartAsClient(DataManager.PlayerUsername, null, null);
        }
#else
        MirrorManager.instance.StartAsClient(DataManager.playerUsername, null, null);
#endif
    }


    // Start is called before the first frame update
    void Start()
    {
        avatar = Instantiate(avatarSample, avatarSpot).GetComponent<Avatar>();
        avatar.Load(AvatarManager.myAvatar);
        avatar.transform.localScale = Vector3.one * 1200f;
        SelectCategory(0);

        nameText.text = string.Format("{0}", DataManager.PlayerName); // avatarId;
        doneButton.interactable = true;
        importButton.interactable = !DataManager.isAnon;
    }

    // Update is called once per frame
    void Update()
    {
        if (avatar != null)
        {
            Vector3 position = avatarSpot.transform.position;
            position.z = 0;
            avatar.transform.position = position;
        }
    }
}
