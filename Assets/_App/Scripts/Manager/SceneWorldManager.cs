using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SceneWorldManager : MonoBehaviour
{
    public static SceneWorldManager instance;
    public static bool isWorldLoaded;
    private void Awake()
    {
        instance = this;
    }

    [SerializeField] GameObject camera;
    [SerializeField] Transform spawner;
    [SerializeField] Text buildingInfo;
    [SerializeField] Button buyBuildingButton;
    [SerializeField] Button askBuildingButton;
    [SerializeField] Button editBuildingButton;
    [SerializeField] Button interactWithObjectButton;
    [SerializeField] Button stopInteractWithObjectButton;
    [SerializeField] Joystick joystick;
    [SerializeField] Slider zoomSlider;
    [SerializeField] GameObject buildingListPanel;
    [SerializeField] MinimapCamera minimapCameraHUD;
    [SerializeField] GameObject popupInfo;
    [SerializeField] GameObject loadingPanel;
    [SerializeField] Text loadingText;

    [Header("Full Map")]
    [SerializeField] GameObject fullMapPanel;
    [SerializeField] GameObject teleportPinSample;
    [SerializeField] List<Transform> teleportPoints;
    [Space()]
    [SerializeField] RectTransform fullMapPinLand;
    [SerializeField] RectTransform fullMapUpLeftLand;
    [SerializeField] RectTransform fullMapUpRightLand;
    [SerializeField] RectTransform fullMapDownLeftLand;
    [SerializeField] Transform teleportPinParentLand;
    [Space()]
    [SerializeField] RectTransform fullMapPinPort;
    [SerializeField] RectTransform fullMapUpLeftPort;
    [SerializeField] RectTransform fullMapUpRightPort;
    [SerializeField] RectTransform fullMapDownLeftPort;
    [SerializeField] Transform teleportPinParentPort;

    float cameraOffset = 0.25f;
    float maxZoom = 0.7f;
    float minZoom = 2.7f;
    float defaultZoom = 1.25f;

    Player player;
    List<Player> allPlayer;
    WorldObject furnitureObjectToInteract;
    bool isLoadingNFTViewer;
    bool isBuyingBuilding;
    Coroutine cPopupInfo;

    public Player Player { get { return player; } }

    public void InitMyPlayer(Player player)
    {
        StartCoroutine(InitMyPlayer_(player));
    }

    IEnumerator InitMyPlayer_(Player player)
    {
        yield return new WaitUntil(() => isWorldLoaded);

        this.player = player;
        player.SetJoystick(joystick);

        if (DataManager.playerLastPosition == null || DataManager.playerLastPosition == Vector3.zero)
        {
            player.transform.position = spawner.position;
        }
        else
        {
            player.transform.position = DataManager.playerLastPosition;
        }

        camera.transform.SetParent(player.transform);
        camera.transform.localPosition = new Vector3(0, cameraOffset, -10);

        minimapCameraHUD.InitPlayer(player);
        MapDataRenderer.instance.Init(player.transform);
        VoiceChatManager.instance.Init(player.data.id, player.data.username);

        if (DataManager.playerLasPositionGrid != null && DataManager.playerLasPositionGrid != Vector3Int.zero)
        {
            MapDataRenderer.instance.TeleportPlayerToPosition(DataManager.playerLasPositionGrid);
            DataManager.playerLasPositionGrid = Vector3Int.zero;
        }
    }

    public void AddPlayer(Player player)
    {
        if (allPlayer == null)
        {
            allPlayer = new List<Player>();
        }

        allPlayer.Add(player);
    }

    public Player GetPlayer(int actorId)
    {
        if (allPlayer == null)
        {
            return null;
        }

        for (int i = 0; i < allPlayer.Count; i++)
        {
            if (allPlayer[i].data.id == actorId)
            {
                return allPlayer[i];
            }
        }

        return null;
    }

    public void SetBuildingInfo(bool isShowing)
    {
        buildingInfo.transform.parent.gameObject.SetActive(false);
        buyBuildingButton.gameObject.SetActive(false);
        askBuildingButton.gameObject.SetActive(false);
        editBuildingButton.gameObject.SetActive(false);

        if (isShowing)
        {
            buildingInfo.text = "Getting Data...";
            buildingInfo.transform.parent.gameObject.SetActive(true);

            BuildingSpot spot = MapDataRenderer.instance.GetBuildingSpot(player.data.buildingToInteract);

            if (spot.IsLoadingBuilding())
            {
                return;
            }

            if (!spot.IsOwned())
            {
                buildingInfo.text = "Buy Me";
                buyBuildingButton.gameObject.SetActive(true);
                buyBuildingButton.interactable = spot.GetBuildingData() != null;
            }
            else
            {
                if (spot.IsMine())
                {
                    buildingInfo.text = "Owner: You";
                    editBuildingButton.gameObject.SetActive(true);
                    editBuildingButton.interactable = spot.IsReadyToEdit();
                }
                else
                {
                    buildingInfo.text = string.Format("Owner: {0}", spot.OwnerName);
                    askBuildingButton.gameObject.SetActive(true);
                }
            }

            if (DataManager.isAnon)
            {
                buyBuildingButton.gameObject.SetActive(false);
                askBuildingButton.gameObject.SetActive(false);
                editBuildingButton.gameObject.SetActive(false);
            }
        }
    }

    public void BuyBuilding()
    {
        BuildingSpot spot = MapDataRenderer.instance.GetBuildingSpot(player.data.buildingToInteract);

        if (spot == null)
            return;

        if (!spot.IsOwned())
        {
#if METAMASK
            isBuyingBuilding = true;
            Application.OpenURL($"https://appdevv2.unique.one/token/{spot.spotData.collectionUUID}/{spot.spotData.tokenAddress}");
#endif
            spot.BuyBuilding(DataManager.PlayerUsername, player.data.id);
        }
        else
        {
            if (spot.IsMine())
            {
                EditBuilding(spot.GetBuildingData());
            }
        }
    }

    void EditBuilding(BuildingData data)
    {
        MapDataLoader.instance.ReturnAllObejctPool();

        DataManager.playerLastPosition = player.transform.position;

        MirrorManager.instance.DisconnectFromGame();

        //PhotonManager.instance.SetSceneSync(false);
        //PhotonManager.instance.LeaveRoom();
        //PhotonManager.instance.Disconnect();

        ScenePersonalSpaceManager.currentBuildingData = data;
        UnityEngine.SceneManagement.SceneManager.LoadScene("PersonalSpace");
    }

    public void RefreshBuilding()
    {
        BuildingSpot spot = MapDataRenderer.instance.GetBuildingSpot(player.data.buildingToInteract);

        if (spot == null)
            return;

        spot.RefreshBuilding();
    }

    public void ShowBuildingListPopup()
    {
        CanvasBuildingList canvasBuildingList = Instantiate(buildingListPanel).GetComponent<CanvasBuildingList>();
        canvasBuildingList.Init(DataManager.PlayerUsername, (string buildingId) =>
        {
            TeleportToBuilding(MapDataRenderer.instance.GetBuildingSpot(buildingId));
            canvasBuildingList.Close();
        });
    }

    void TeleportToBuilding(BuildingSpot building)
    {
        if (player.data.IsInteractingWithObject)
        {
            StopInteractWithObject();
        }
        player.transform.position = building.GetCenterPosition();
    }

    public void ManualRefreshAllBuilding()
    {
        //StartCoroutine(LoadAllNearBuilding());
    }

    public void SetInteractWithObjectButton(bool isActiving, WorldObject furnitureObject)
    {
        if (!isActiving)
        {
            interactWithObjectButton.gameObject.SetActive(false);

            return;
        }

        if (!furnitureObject.IsInteractable || furnitureObject.IsOccupied)
        {
            return;
        }

        furnitureObjectToInteract = furnitureObject;
        interactWithObjectButton.gameObject.SetActive(true);
        interactWithObjectButton.GetComponent<UIToWorldObjectFollower>().SetObjectToFollow(furnitureObject.gameObject);
    }

    public void InteractWithObject()
    {
        if (furnitureObjectToInteract == null)
        {
            return;
        }

        if (!furnitureObjectToInteract.IsInteractable || furnitureObjectToInteract.IsOccupied)
        {
            return;
        }

        interactWithObjectButton.gameObject.SetActive(false);

        player.InteractWithObject(furnitureObjectToInteract);
        MirrorManager.instance.InteractWithObject(furnitureObjectToInteract.data.tilePosition);
    }

    public void IniteractWitchObjectForOtherPlayer(int actorId, Vector3Int position)
    {
        Player player = GetPlayer(actorId);
        WorldObject furnitureObject = MapDataRenderer.instance.GetWorldObject(position);

        if (player == null)
        {
            return;
        }

        if (furnitureObject == null)
        {
            return;
        }
        else
        {
            if (!furnitureObject.IsInteractable || furnitureObject.IsOccupied)
            {
                return;
            }
        }

        player.InteractWithObject(furnitureObject);
    }

    public void StopInteractWithObject()
    {
        if (furnitureObjectToInteract == null)
        {
            return;
        }

        SetInteractWithObjectButton(true, furnitureObjectToInteract);

        player.StopInteractWithObject(furnitureObjectToInteract);
        MirrorManager.instance.StopInteractWithObject();
        
        furnitureObjectToInteract = null;
    }

    public void StopIniteractWitchObjectForOtherPlayer(int actorId, Vector3Int position)
    {
        Player player = GetPlayer(actorId);
        WorldObject furnitureObject = MapDataRenderer.instance.GetWorldObject(position);

        if (player == null)
        {
            return;
        }

        if (furnitureObject == null)
        {
            return;
        }

        player.StopInteractWithObject(furnitureObject);
    }

    public void SetStopInterractWithObjectButton(bool isActiving)
    {
        stopInteractWithObjectButton.gameObject.SetActive(isActiving);
    }

    public void OnZoomSliderValueChange()
    {
        camera.GetComponent<Camera>().orthographicSize = zoomSlider.value;
    }

    public void ZoomIn()
    {
        zoomSlider.value = Mathf.Min(zoomSlider.value - defaultZoom, minZoom);
        OnZoomSliderValueChange();
    }

    public void ZoomOut()
    {
        zoomSlider.value = Mathf.Max(zoomSlider.value + defaultZoom, maxZoom);
        OnZoomSliderValueChange();
    }

    public void EditAvatar()
    {
        MapDataLoader.instance.ReturnAllObejctPool();
        DataManager.playerLastPosition = player.transform.position;

        MirrorManager.instance.DisconnectFromGame();

        //PhotonManager.instance.SetSceneSync(false);
        //PhotonManager.instance.LeaveRoom();
        //PhotonManager.instance.Disconnect();

        SceneAvatarManager.avatarId = DataManager.PlayerUsername;
        UnityEngine.SceneManagement.SceneManager.LoadScene("Avatar");
    }

    public void ShowChatBubble(int actorId, string chat)
    {
        if (GetPlayer(actorId) == null)
        {
            return;
        }

        GetPlayer(actorId).ShowChatBubble(chat);
    }

    public void OpenFullMap()
    {
        fullMapPanel.SetActive(true);
        ProjectPinOnFullMap(AspectRatioManager.Instance.IsPortrait ? fullMapPinPort.gameObject : fullMapPinLand.gameObject, MapDataRenderer.instance.Grid.WorldToCell(player.transform.position));

        teleportPinSample.SetActive(false);

        if (AspectRatioManager.Instance.IsPortrait)
        {
            teleportPinParentPort.RemoveAllChildren();
        }
        else
        {
            teleportPinParentLand.RemoveAllChildren();
        }

        for (int i = 0; i < teleportPoints.Count; i++)
        {
            GameObject pin = Instantiate(teleportPinSample, AspectRatioManager.Instance.IsPortrait ? teleportPinParentPort : teleportPinParentLand).gameObject;
            ProjectPinOnFullMap(pin, MapDataRenderer.instance.Grid.WorldToCell(teleportPoints[i].position));
            pin.SetActive(true);
            pin.GetComponent<FullMapPinTeleporter>().Init(teleportPoints[i].position, (Vector3 position) =>
            {
                if (player.data.IsInteractingWithObject)
                {
                    StopInteractWithObject();
                }
                player.transform.position = position;
                CloseMapPanel();
            });
        }
    }

    void ProjectPinOnFullMap(GameObject pinObject, Vector3Int tilePos)
    {
        Vector3 playerProjection = new Vector3((float)(Mathf.Abs(tilePos.y)) / MapDataLoader.instance.mapProperties.size.y, (float)(Mathf.Abs(tilePos.x)) / MapDataLoader.instance.mapProperties.size.x, 0);
        Vector3 mapWidthVector = AspectRatioManager.Instance.IsPortrait ? 
            fullMapUpRightPort.transform.position - fullMapUpLeftPort.transform.position : 
            fullMapUpRightLand.transform.position - fullMapUpLeftLand.transform.position;
        Vector3 mapHeightVector = AspectRatioManager.Instance.IsPortrait ? 
            fullMapUpLeftPort.transform.position - fullMapDownLeftPort.transform.position : 
            fullMapUpLeftLand.transform.position - fullMapDownLeftLand.transform.position;
        Vector3 playerPositionVector = (AspectRatioManager.Instance.IsPortrait ? fullMapDownLeftPort.transform.position : fullMapDownLeftLand.transform.position) 
            + (mapHeightVector * (1 - playerProjection.y)) + (mapWidthVector * (playerProjection.x));
        pinObject.transform.position = playerPositionVector;
    }

    public void CloseMapPanel()
    {
        fullMapPanel.SetActive(false);
    }

    public void ShowPaintingNFT(string owner, TextureData data)
    {
        if (isLoadingNFTViewer)
        {
            return;
        }

        isLoadingNFTViewer = true;
        NFTViewer.Load(owner, data, () =>
        {
            isLoadingNFTViewer = false;
        });
    }

    public bool IsPlayerNearMe(int actorId)
    {
        if (GetPlayer(actorId) == null)
        {
            return false;
        }

        if (actorId == player.data.id)
        {
            return true;
        }

        //string otherPlayerBuilding = PhotonManager.instance.GetPlayerBuildingToInteract(actorId);
        string otherPlayerBuilding = GetPlayer(actorId).data.buildingToInteract;

        bool isMeInsideBuilding = !string.IsNullOrEmpty(player.data.buildingToInteract);
        bool isOtherIndsideBuilding = !string.IsNullOrEmpty(otherPlayerBuilding);

        if (isMeInsideBuilding && isOtherIndsideBuilding)
        {
            if (player.data.buildingToInteract == otherPlayerBuilding)
            {
                return true;
            }
        }
        else if (!isMeInsideBuilding && !isOtherIndsideBuilding)
        {
            if (Vector2.Distance(player.transform.position, GetPlayer(actorId).transform.position) <= DataManager.minimumBuildingDistance)
            {
                return true;
            }
        }

        return false;
    }

    public bool IsSomeoneInteractingWithObject(Vector3 objectPosition)
    {
        for (int i = 0; i < allPlayer.Count; i++)
        {
            if (allPlayer[i].data.objectToInteract == objectPosition)
            {
                return true;
            }
        }

        return false;
    }

    public void ShowPopupInfo(string message)
    {
        if (cPopupInfo != null)
        {
            StopCoroutine(cPopupInfo);
        }

        popupInfo.SetActive(true);
        popupInfo.GetComponentInChildren<Text>().text = message;
        cPopupInfo = StartCoroutine(ShowPopupInfo_());

        IEnumerator ShowPopupInfo_()
        {
            yield return new WaitForSecondsRealtime(2f);
            popupInfo.SetActive(false);
            cPopupInfo = null;
        }
    }

    private void OnApplicationFocus(bool focus)
    {
        if (focus && isBuyingBuilding)
        {
            NFTItemManager.Instance.Init(null, isSuccess =>
            {
                isBuyingBuilding = false;
            });
        }
    }

    // Start is called before the first frame update
    void Start()
    {
#if UNITY_SERVER
        MirrorManager.instance.InitRegisterHandler();
#endif

        SetInteractWithObjectButton(false, null);
        SetBuildingInfo(false);

        zoomSlider.minValue = maxZoom;
        zoomSlider.maxValue = minZoom;
        zoomSlider.value = defaultZoom;
        OnZoomSliderValueChange();

        if (!isWorldLoaded)
        {
            StartCoroutine(LoadWorld());
        }
        else
        {
            loadingPanel.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    IEnumerator LoadWorld()
    {
        isWorldLoaded = false;

        loadingPanel.SetActive(true);
        loadingText.text = "Loading World ...";

        var task = UnityEngine.SceneManagement.SceneManager.LoadSceneAsync("Loader", UnityEngine.SceneManagement.LoadSceneMode.Additive);
        task.allowSceneActivation = false;

        while (!task.isDone)
        {
            if (task.progress >= 0.9f)
            {
                task.allowSceneActivation = true;
            }

            yield return null;
        }

        yield return null;

        bool isExtracting = true;

        MapDataLoader.instance.OnExtractFinished += () =>
        {
            isExtracting = false;
        };

        MapDataLoader.instance.Load(loadingText);

        yield return new WaitUntil(() => !isExtracting);

        UnityEngine.SceneManagement.SceneManager.UnloadSceneAsync("Loader");

        loadingPanel.SetActive(false);

        isWorldLoaded = true;
    }
}
