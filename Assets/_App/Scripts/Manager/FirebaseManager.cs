using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_WEBGL
using FirebaseWebGL.Scripts;
using FirebaseWebGL.Scripts.FirebaseBridge;
using FirebaseWebGL.Scripts.Objects;
#else
using Firebase;
using Firebase.Database;
using Firebase.Extensions;
#endif

public class FirebaseManager : Manager
{
    public static FirebaseManager Instance => Get<FirebaseManager>();
    public List<string> newBuildings;

    #region NOT_WEBGL_ONLY
#if !UNITY_WEBGL

    public FirebaseApp app;

    private void Start()
    {
        //FirebaseDatabase.DefaultInstance.SetPersistenceEnabled(false);
    }

    public void Init()
    {
        Firebase.FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task =>
        {
            var dependencyStatus = task.Result;
            if (dependencyStatus == Firebase.DependencyStatus.Available)
            {
                // Create and hold a reference to your FirebaseApp,
                // where app is a Firebase.FirebaseApp property of your application class.
                app = Firebase.FirebaseApp.DefaultInstance;

                // Set a flag here to indicate whether Firebase is ready to use by your app.
            }
            else
            {
                UnityEngine.Debug.LogError(System.String.Format(
                  "Could not resolve all Firebase dependencies: {0}", dependencyStatus));
                // Firebase Unity SDK is not safe to use here.
            }
        });
    }

    public DatabaseReference GetReferencePath()
    {
        return FirebaseDatabase.DefaultInstance.RootReference;
    }

    void AddNewBuildingToList(object x, ChildChangedEventArgs childChangedEventArgs)
    {
        string newId = childChangedEventArgs.Snapshot.Key;
        if (!newBuildings.Contains(newId))
        {
            newBuildings.Add(newId);
        }
    }
#endif
    #endregion

    #region WEBGL_ONLY
#if UNITY_WEBGL

    public string GetReferencePath()
    {
        return "";
    }

    void AddNewBuildingToListSuccess(string result)
    {
        string newId = result;

        if (!newBuildings.Contains(newId))
        {
            newBuildings.Add(newId);
        }
    }

    void AddNewBuildingToListFail(string result)
    {
        //Debug.LogError("error :" + result);
    }

#endif
    #endregion

    public void BuyBuilding(BuildingData data, System.Action<bool> OnDone, System.Action OnTimeout = null)
    {
        FirebaseLoader loader = new GameObject("buy_building_" + data.id).AddComponent<FirebaseLoader>();
        loader.BuyBuilding(data, OnDone, OnTimeout);
    }

    public void SetBuilding(BuildingData data, System.Action<bool> OnDone, System.Action OnTimeout = null)
    {
        FirebaseLoader loader = new GameObject("set_building_" + data.id).AddComponent<FirebaseLoader>();
        loader.SetBuilding(data, OnDone, OnTimeout);
    }

    public void GetBuilding(string buildingId, System.Action<string> OnDone, System.Action OnTimeout = null)
    {
        FirebaseLoader loader = new GameObject("get_building_" + buildingId).AddComponent<FirebaseLoader>();
        loader.GetBuilding(buildingId, OnDone, OnTimeout);
    }

    public void GetBuildingList(string ownerId, System.Action<List<string>> OnDone, System.Action OnTimeout = null)
    {
        FirebaseLoader loader = new GameObject("get_building_list_" + ownerId).AddComponent<FirebaseLoader>();
        loader.GetBuildingList(ownerId, OnDone, OnTimeout);
    }

    public void GetAvatar(string userId, System.Action<string> OnDone, System.Action OnTimeout = null)
    {
        FirebaseLoader loader = new GameObject("get_avatar_" + userId).AddComponent<FirebaseLoader>();
        loader.GetAvatar(userId, OnDone, OnTimeout);
    }

    public void GetAllAvatar(System.Action<string> OnDone, System.Action OnTimeout = null)
    {
        FirebaseLoader loader = new GameObject("get_avatar_all").AddComponent<FirebaseLoader>();
        loader.GetAllAvatar(OnDone, OnTimeout);
    }

    public void SetAvatar(string userId, string data, System.Action<bool> OnDone, System.Action OnTimeout = null)
    {
        FirebaseLoader loader = new GameObject("set_avatar_"+userId).AddComponent<FirebaseLoader>();
        loader.SetAvatar(userId, data, OnDone, OnTimeout);
    }

    public void InitBuildingListener(List<BuildingSpot> spots)
    {
        newBuildings = new List<string>();
        for (int i = 0; i < spots.Count; i++)
        {
            newBuildings.Add(spots[i].spotData.id);
        }

#if !UNITY_WEBGL
        DatabaseReference reference = GetReferencePath();
        reference.Child("building").ChildAdded += AddNewBuildingToList;
        reference.Child("building").ChildChanged += AddNewBuildingToList;
#else
        string path = Instance.GetReferencePath() + "building/";
        FirebaseDatabase.ListenForChildAdded(path, gameObject.name, "AddNewBuildingToListSuccess", "AddNewBuildingToListFail");
        FirebaseDatabase.ListenForChildChanged(path, gameObject.name, "AddNewBuildingToListSuccess", "AddNewBuildingToListFail");
#endif
    }
}