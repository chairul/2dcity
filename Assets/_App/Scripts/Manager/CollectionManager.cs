using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectionManager : Manager
{
    public static CollectionManager Instance => Get<CollectionManager>();
    private static Sprite[] AvailableFloor { get { return Instance.availableFloor; } }
    private static Sprite[] AvailableWall { get { return Instance.availableWall; } }
    private static Sprite[] AvailableFurniture { get { return Instance.availableFurniture; } }
    private static Sprite[] AvailablePainting { get { return Instance.availablePainting; } }
    private static SpriteRef[] AvailableAvatar { get { return Instance.availableAvatar; } }
    private static Sprite[] AvailableWorldObject { get { return Instance.availableWorldObject; } }

    [SerializeField] Sprite[] availableFloor;
    [SerializeField] Sprite[] availableWall;
    [SerializeField] Sprite[] availableFurniture;
    [SerializeField] Sprite[] availablePainting;
    [SerializeField] SpriteRef[] availableAvatar;
    [SerializeField] Sprite[] availableWorldObject;

    public static SpriteRef GetAvatarItem(string id)
    {
        return AvailableAvatar.ToList().Find((x) => x.id == id);
    }

    public static Sprite[] GetAllFloor()
    {
        return AvailableFloor;
    }

    public static Sprite GetFloor(int index)
    {
        if (AvailableFloor[index] != null)
        {
            return AvailableFloor[index];
        }

        return AvailableFloor[0];
    }

    public static Sprite[] GetAllWall()
    {
        return AvailableWall;
    }

    public static Sprite GetWall(int index)
    {
        if (AvailableWall[index] != null)
        {
            return AvailableWall[index];
        }

        return AvailableWall[0];
    }

    public static Sprite[] GetAllFurniture()
    {
        return AvailableFurniture;
    }

    public static Sprite GetFurniture(int index)
    {
        if (AvailableFurniture[index] != null)
        {
            return AvailableFurniture[index];
        }

        return AvailableFurniture[0];
    }

    public static Sprite[] GetAllPainting()
    {
        return AvailablePainting;
    }

    public static Sprite GetPainting(int index)
    {
        if (AvailablePainting[index] != null)
        {
            return AvailablePainting[index];
        }

        return AvailablePainting[0];
    }

    public static Sprite[] GetAllWorldObject()
    {
        return AvailableWorldObject;
    }

    public static Sprite GetWorldObject(int index)
    {
        if (AvailableWorldObject[index] != null)
        {
            return AvailableWorldObject[index];
        }

        return AvailableWorldObject[0];
    }

    public static Sprite GetWorldObject(string id)
    {
        if (AvailableWorldObject.ToList().Exists((x) => x.name == id))
        {
            return AvailableWorldObject.ToList().Find((x) => x.name == id);
        }

        return null;
    }
}

[Serializable]
public struct SpriteRef
{
    public string id;
    public Sprite sprite;
}
