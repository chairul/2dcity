using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class TextureManager : Manager
{
    public static TextureManager Instance => Get<TextureManager>();
    public static readonly Vector2 defaultSpritePivot = new Vector2(0.5f, 0.5f);
    public static readonly Vector2 avatarArmSpritePivot = new Vector2(0.8299607f, 0.8449593f);
    public static readonly Vector2 avatarThighSpritePivot = new Vector2(0.818148f, 0.8718617f);
    public static readonly Vector2 avatarLegSpritePivot = new Vector2(0.1608905f, 0.8246396f);
    public static readonly Vector2 avatarLegBackSpritePivot = new Vector2(0.1970015f, 0.8718617f);
    public static readonly Vector2 avatarShoeSpritePivot = new Vector2(0.3129311f, 0.8094382f);

    public struct TextureSpriteData
    {
        public Texture2D texture;
        public Sprite sprite;

        public TextureSpriteData(Texture2D texture, Sprite sprite)
        {
            this.texture = texture;
            this.sprite = sprite;
        }

        public void DestroyTexture()
        {
            if (texture != null && texture != Texture2D.whiteTexture)
            {
                Destroy(texture);
            }
        }
    }

    Dictionary<string, TextureSpriteData> downloadedTextures;
    System.Action<TextureSpriteData> OnFinished;
    const int MAX_DOWNLOADED_COUNT = 50;

    public void GetTexture(TextureType type, TextureData textureData, System.Action<TextureSpriteData> OnFinished)
    {
        this.OnFinished = OnFinished;

        if (textureData.textureSourceType == TextureSourceType.Index)
        {
            if (type == TextureType.Floor)
            {
                Texture2D texture = CollectionManager.GetFloor(textureData.textureIndex).texture;
                OnFinished?.Invoke(CreateTextureSprite(type, texture));
            }
            else if (type == TextureType.Wall)
            {
                Texture2D texture = CollectionManager.GetWall(textureData.textureIndex).texture;
                OnFinished?.Invoke(CreateTextureSprite(type, texture));
            }
            else if (type == TextureType.Furniture)
            {
                Texture2D texture = CollectionManager.GetFurniture(textureData.textureIndex).texture;
                OnFinished?.Invoke(CreateTextureSprite(type, texture));
            }
            else if (type == TextureType.Painting)
            {
                Texture2D texture = CollectionManager.GetPainting(textureData.textureIndex).texture;
                OnFinished?.Invoke(CreateTextureSprite(type, texture));
            }
            else if (type == TextureType.AvatarHead || type == TextureType.AvatarHair || type == TextureType.AvatarFace || type == TextureType.AvatarTop || type == TextureType.AvatarArm || type == TextureType.AvatarHip || type == TextureType.AvatarThigh || type == TextureType.AvatarLeg || type == TextureType.AvatarShoe)
            {
                Texture2D texture = CollectionManager.GetAvatarItem(textureData.textureId).sprite.texture;
                OnFinished?.Invoke(CreateTextureSprite(type, texture));
            }
        }
        else
        {
            if (downloadedTextures == null)
            {
                downloadedTextures = new Dictionary<string, TextureSpriteData>();
            }

            if (downloadedTextures.ContainsKey(textureData.textureURL))
            {
                OnFinished?.Invoke(downloadedTextures[textureData.textureURL]);
            }
            else
            {
                TextureLoader loader = gameObject.AddComponent<TextureLoader>();

                AddToList(textureData.textureURL, Texture2D.whiteTexture, type);

                loader.LoadTexture(textureData.textureURL, textureData.isAnimated, (bool isSuccess, Texture2D texture) =>
                {
                    if (isSuccess)
                    {
                        if (downloadedTextures.Count >= MAX_DOWNLOADED_COUNT)
                        {
                            RemoveFromList(downloadedTextures.ElementAt(0).Key);
                        }

                        downloadedTextures[textureData.textureURL] = CreateTextureSprite(type, texture);
                        OnFinished?.Invoke(downloadedTextures[textureData.textureURL]);
                    }
                    else
                    {
                        RemoveFromList(textureData.textureURL);
                    }
                    Destroy(loader);
                });
            }
        }
    }

    void ResizeTexture(Texture2D sourceTexture)
    {
        Texture2D result = sourceTexture;

        int width = result.width;
        int height = result.height;

        if (height > DataManager.paintingDefaultHeight)
        {
            float ratio = (float)DataManager.paintingDefaultHeight / (float)height;
            width = Mathf.CeilToInt(width * ratio);
            height = DataManager.paintingDefaultHeight;
            TextureScaler.scale(result, width, height);
        }
    }

    public TextureSpriteData CreateTextureSprite(TextureType type, Texture2D texture)
    {
        Sprite sprite = null;

        if (type == TextureType.Floor)
        {
            sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), defaultSpritePivot);
        }
        else if (type == TextureType.Wall)
        {
            sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0f), 100, 1, SpriteMeshType.FullRect, new Vector4(0, 17, 0, 34));
        }
        else if (type == TextureType.Furniture)
        {
            sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), defaultSpritePivot);
        }
        else if (type == TextureType.Painting)
        {
            ResizeTexture(texture);
            sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), defaultSpritePivot);
        }
        else if (type == TextureType.AvatarHead || type == TextureType.AvatarHair || type == TextureType.AvatarFace || type == TextureType.AvatarTop || type == TextureType.AvatarHip)
        {
            sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), defaultSpritePivot);
        }
        else if (type == TextureType.AvatarArm)
        {
            sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), avatarArmSpritePivot);
        }
        else if (type == TextureType.AvatarThigh)
        {
            sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), avatarThighSpritePivot);
        }
        else if (type == TextureType.AvatarLeg)
        {
            sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), avatarLegSpritePivot);
        }
        else if (type == TextureType.AvatarShoe)
        {
            sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), avatarShoeSpritePivot);
        }
        else
        {
            sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), defaultSpritePivot);
        }

        return new TextureSpriteData(texture, sprite);
    }

    public void AddToList(string url, Texture2D texture, TextureType type)
    {
        if (downloadedTextures == null)
        {
            downloadedTextures = new Dictionary<string, TextureSpriteData>();
        }

        if (!downloadedTextures.ContainsKey(url))
        {
            downloadedTextures.Add(url, CreateTextureSprite(type, texture));
        }
    }

    public void RemoveFromList(string url)
    {
        if (downloadedTextures.ContainsKey(url))
        {
            downloadedTextures[url].DestroyTexture();
            downloadedTextures.Remove(url);
        }
    }

    public Sprite[] SplitAvatarParts(Texture2D texture, TextureType type)
    {
        Sprite[] result = new Sprite[2];

        Vector2 pivot = defaultSpritePivot;
        if (type == TextureType.AvatarArm)
        {
            pivot = avatarArmSpritePivot;
        }
        else if (type == TextureType.AvatarThigh)
        {
            pivot = avatarThighSpritePivot;
        }
        else if (type == TextureType.AvatarLeg)
        {
            pivot = avatarLegSpritePivot;
        }
        else if (type == TextureType.AvatarShoe)
        {
            pivot = avatarShoeSpritePivot;
        }

        Sprite frontSprite = Sprite.Create(texture, new Rect(0, 0, texture.width / 2, texture.height), pivot);
        result[0] = frontSprite;

        Sprite backSprite = Sprite.Create(texture, new Rect(texture.width / 2, 0, texture.width / 2, texture.height), type == TextureType.AvatarLeg ? avatarLegBackSpritePivot : pivot);
        result[1] = backSprite;

        return result;
    }
}
