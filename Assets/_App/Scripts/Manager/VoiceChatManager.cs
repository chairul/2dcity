using FrostweepGames.Plugins.Native;
using FrostweepGames.VoicePro;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class VoiceChatManager : MonoBehaviour
{
    public static VoiceChatManager instance;
    public static bool sendVoiceViaManualRPC;
    public static bool listenToYourself;
    private void Awake()
    {
        instance = this;
    }

    [SerializeField] Recorder recorder;
    [SerializeField] Listener listener;
    [SerializeField] GameObject voiceChatSettingPanel;
    [SerializeField] Dropdown micInputList;
    [SerializeField] GameObject muteIcon;
    [SerializeField] Text speakerText;
    [SerializeField] Toggle voiceModeToggle;
    [SerializeField] Toggle hearYourselfToggle;

    float speakTimer;
    int microphoneIndex;
    AudioClip clip;
    Dictionary<int, AudioSource> speakers;

    public void ToggleShoVCSettingPanel()
    {
        if (voiceChatSettingPanel.activeSelf)
        {
            HideVCSettingPanel();
        }
        else
        {
            ShowVCSettingPanel();
        }
    }

    public void ToggleSendVoiceMode(bool isSendViaRPC)
    {
        sendVoiceViaManualRPC = isSendViaRPC;
    }

    public void ToggleHearYourself(bool isOn)
    {
        listenToYourself = isOn;
        recorder.debugEcho = isOn;
    }

    public void ShowVCSettingPanel()
    {
        voiceChatSettingPanel.SetActive(true);
        RefreshMicInputList();
        HideSprekerText();
    }

    public void HideVCSettingPanel()
    {
        voiceChatSettingPanel.SetActive(false);
    }

    void RefreshMicInputList()
    {
        MuteMicrophone(true);

        micInputList.ClearOptions();
        micInputList.AddOptions(CustomMicrophone.devices.ToList());

        if (micInputList.options.Count > microphoneIndex)
        {
            micInputList.value = microphoneIndex;
        }
    }

    void OnMicInputSelected(int index)
    {
        if (microphoneIndex == index)
        {
            return;
        }

        microphoneIndex = index;

        recorder.SetMicrophone(CustomMicrophone.devices[index]);
    }

    public void ToggleMuteMicrophone()
    {
        MuteMicrophone(!muteIcon.activeSelf);
    }

    void MuteMicrophone(bool isMuting)
    {
        if (!isMuting)
        {
            if (!CustomMicrophone.HasMicrophonePermission())
            {
                CustomMicrophone.RequestMicrophonePermission();
            }

            recorder.StartRecord();
            muteIcon.SetActive(false);
        }
        else
        {
            recorder.StopRecord();
            muteIcon.SetActive(true);
        }
    }

    public void Listen(int actorId, string name, byte[] data)
    {
        if (!SceneWorldManager.instance.IsPlayerNearMe(actorId))
        {
            return;
        }

        speakerText.text = string.Format("RPC: {0} Speaking", name);
        //listener.ListenViaRPC(actorId, name, data);

        ProcessAudioData(actorId, Compressor.Decompress(data));

        speakTimer = 1f;
    }

    public void Listen(int actorId, string name)
    {
        speakerText.text = string.Format("{0} Speaking", name);

        speakTimer = 1f;
    }

    void ProcessAudioData(int id, byte[] data)
    {
        float[] rawData = AudioConverter.ByteToFloat(data);

        if (clip != null || clip)
        {
            Destroy(clip);
        }

        clip = AudioClip.Create("clip" + id, Constants.SampleRate * Constants.RecordingTime, 1, Constants.SampleRate, false);
        clip.SetData(rawData, 0);

        AudioSource audioSource = null;
        if (speakers.ContainsKey(id))
        {
            audioSource = speakers[id];
        }
        else
        {
            audioSource = gameObject.AddComponent<AudioSource>();
            speakers.Add(id, audioSource);
        }
        audioSource.clip = clip;
        audioSource.Play();
    }

    void ShowSpeakerText()
    {
        if (!voiceChatSettingPanel.activeInHierarchy)
        {
            speakerText.gameObject.SetActive(true);
        }
    }

    void HideSprekerText()
    {
        speakerText.gameObject.SetActive(false);
    }

    public void Init(int actorId, string name)
    {
        NetworkRouter.Instance.Register(actorId, name, Enumerators.NetworkType.Mirror);
    }

    // Start is called before the first frame update
    void Start()
    {
        HideVCSettingPanel();
        MuteMicrophone(true);
        HideSprekerText();

        voiceModeToggle.isOn = false;
        voiceModeToggle.onValueChanged.AddListener(ToggleSendVoiceMode);
        ToggleSendVoiceMode(false);

        hearYourselfToggle.isOn = false;
        hearYourselfToggle.onValueChanged.AddListener(ToggleHearYourself);
        ToggleHearYourself(false);

        micInputList.onValueChanged.AddListener(OnMicInputSelected);

        speakers = new Dictionary<int, AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.LogError(NetworkRouter.Instance.GetCurrentRoomName());
        speakTimer -= Time.deltaTime;
        if (speakTimer > 0)
        {
            ShowSpeakerText();
        }
        else
        {
            HideSprekerText();
        }
    }
}
