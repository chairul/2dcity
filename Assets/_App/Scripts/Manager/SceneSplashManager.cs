using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneSplashManager : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
#if !UNITY_WEBGL
        FirebaseManager.Instance.Init();
#endif
        AvatarManager.LoadData();
        WorldObjectManager.LoadData();

        UnityEngine.SceneManagement.SceneManager.LoadScene("Opening");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
