using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;

public enum AvatarItemType { Head, Face, Top, Bottom }
public static class AvatarManager
{
    public static Dictionary<AvatarItemType, AvatarCatergory> avatarCategories { get; private set; }
    public static Dictionary<string, AvatarItemData> avatarItems { get; private set; }
    public static AvatarData myAvatar { get; private set; }
    public static Dictionary<string, AvatarData> allAvatarData { get; private set; }

    public static void LoadData()
    {
        avatarCategories = new Dictionary<AvatarItemType, AvatarCatergory>();
        JSONNode data = JSON.Parse(Resources.Load<TextAsset>("Data/avatar_cat").text);
        for (int i = 0; i < data.Count; i++)
        {
            avatarCategories.Add((AvatarItemType)data[i]["type"].AsInt, new AvatarCatergory(data[i]));
        }

        avatarItems = new Dictionary<string, AvatarItemData>();
        data = JSON.Parse(Resources.Load<TextAsset>("Data/avatar").text);
        for (int i = 0; i < data.Count; i++)
        {
            avatarItems.Add(data[i]["id"], new AvatarItemData(data[i]));
        }
        myAvatar = new AvatarData();
    }

    public static List<AvatarItemData> GetAvatarItemByType(AvatarItemType type)
    {
        List<AvatarItemData> result = new List<AvatarItemData>();

        foreach (AvatarItemData data in avatarItems.Values)
        {
            if (data.type == type)
            {
                result.Add(data);
            }
        }

        return result;
    }

    public static void SetAllAvatar(string data)
    {
        allAvatarData = new Dictionary<string, AvatarData>();

        var avatarList = SimpleJSON.JSON.Parse(data).AsObject;
        foreach (KeyValuePair<string, JSONNode> avatar in avatarList)
        {
            allAvatarData.Add(avatar.Key, new AvatarData(avatar.Value));
        }
    }

    public static void SetMyAvatar(JSONNode data, bool needValidation = false)
    {
        if (data == null)
        {
            myAvatar = new AvatarData();
            return;
        }

        myAvatar = new AvatarData(data, needValidation);
    }
}

public class AvatarCatergory
{
    public AvatarItemType type;
    public List<string> category;

    public AvatarCatergory(JSONNode data)
    {
        type = (AvatarItemType)data["type"].AsInt;
        category = new List<string>();
        for (int i = 0; i < data["sub"].Count; i++)
        {
            category.Add(data["sub"][i]);
        }
    }
}

public class AvatarItemData
{
    public string id;
    public string name;
    public int price;
    public AvatarItemType type;
    public int subCategory;

    public AvatarItemData(JSONNode data)
    {
        id = data["id"];
        name = data["id"];
        price = data["price"].AsInt;
        type = (AvatarItemType)data["type"].AsInt;
        subCategory = data["sub"].AsInt;
    }
}

public class AvatarData
{
    public TextureData head;
    public TextureData hair;
    public TextureData face;
    public TextureData torso;
    public TextureData arm;
    public TextureData hip;
    public TextureData thigh;
    public TextureData leg;
    public TextureData shoe;

    public AvatarData(JSONNode data, bool needValidation = false)
    {
        if (string.IsNullOrEmpty(data["head"].ToString()))
        {
            GetDefault(TextureType.AvatarHead);
        }
        else
        {
            head = new TextureData(data["head"], TextureType.AvatarHead);
        }

        if (string.IsNullOrEmpty(data["hair"].ToString()))
        {
            GetDefault(TextureType.AvatarHair);
        }
        else
        {
            hair = new TextureData(data["hair"], TextureType.AvatarHair);
        }

        if (string.IsNullOrEmpty(data["face"].ToString()))
        {
            GetDefault(TextureType.AvatarFace);
        }
        else
        {
            face = new TextureData(data["face"], TextureType.AvatarFace);
        }

        if (string.IsNullOrEmpty(data["torso"].ToString()))
        {
            GetDefault(TextureType.AvatarTop);
        }
        else
        {
            torso = new TextureData(data["torso"], TextureType.AvatarTop);
        }

        if (string.IsNullOrEmpty(data["arm"].ToString()))
        {
            GetDefault(TextureType.AvatarArm);
        }
        else
        {
            arm = new TextureData(data["arm"], TextureType.AvatarArm);
        }

        if (string.IsNullOrEmpty(data["hip"].ToString()))
        {
            GetDefault(TextureType.AvatarHip);
        }
        else
        {
            hip = new TextureData(data["hip"], TextureType.AvatarHip);
        }

        if (string.IsNullOrEmpty(data["thigh"].ToString()))
        {
            GetDefault(TextureType.AvatarThigh);
        }
        else
        {
            thigh = new TextureData(data["thigh"], TextureType.AvatarThigh);
        }

        if (string.IsNullOrEmpty(data["leg"].ToString()))
        {
            GetDefault(TextureType.AvatarLeg);
        }
        else
        {
            leg = new TextureData(data["leg"], TextureType.AvatarLeg);
        }

        if (string.IsNullOrEmpty(data["shoe"].ToString()))
        {
            GetDefault(TextureType.AvatarShoe);
        }
        else
        {
            shoe = new TextureData(data["shoe"], TextureType.AvatarShoe);
        }

        if (needValidation)
        {
            CheckNFTValidation();
        }
    }

    public AvatarData()
    {
        head = GetDefault(TextureType.AvatarHead);
        hair = GetDefault(TextureType.AvatarHair);
        face = GetDefault(TextureType.AvatarFace);
        torso = GetDefault(TextureType.AvatarTop);
        arm = GetDefault(TextureType.AvatarArm);
        hip = GetDefault(TextureType.AvatarHip);
        thigh = GetDefault(TextureType.AvatarThigh);
        leg = GetDefault(TextureType.AvatarLeg);
        shoe = GetDefault(TextureType.AvatarShoe);
    }

    void CheckNFTValidation()
    {
        bool isNeedValidation = false;

        if (!NFTItemManager.Instance.IsValidated(head, TextureType.AvatarHead))
        {
            isNeedValidation = true;
            head = GetDefault(TextureType.AvatarHead);
        }

        if (!NFTItemManager.Instance.IsValidated(hair, TextureType.AvatarHair))
        {
            isNeedValidation = true;
            hair = GetDefault(TextureType.AvatarHair);
        }

        if (!NFTItemManager.Instance.IsValidated(face, TextureType.AvatarFace))
        {
            isNeedValidation = true;
            face = GetDefault(TextureType.AvatarFace);
        }

        if (!NFTItemManager.Instance.IsValidated(torso, TextureType.AvatarTop))
        {
            isNeedValidation = true;
            torso = GetDefault(TextureType.AvatarTop);
        }

        if (!NFTItemManager.Instance.IsValidated(arm, TextureType.AvatarArm))
        {
            isNeedValidation = true;
            arm = GetDefault(TextureType.AvatarArm);
        }

        if (!NFTItemManager.Instance.IsValidated(hip, TextureType.AvatarHip))
        {
            isNeedValidation = true;
            hip = GetDefault(TextureType.AvatarHip);
        }

        if (!NFTItemManager.Instance.IsValidated(thigh, TextureType.AvatarThigh))
        {
            isNeedValidation = true;
            thigh = GetDefault(TextureType.AvatarThigh);
        }

        if (!NFTItemManager.Instance.IsValidated(leg, TextureType.AvatarLeg))
        {
            isNeedValidation = true;
            leg = GetDefault(TextureType.AvatarLeg);
        }

        if (!NFTItemManager.Instance.IsValidated(shoe, TextureType.AvatarShoe))
        {
            isNeedValidation = true;
            shoe = GetDefault(TextureType.AvatarShoe);
        }

        if (isNeedValidation)
        {
            AvatarData avatarData = new AvatarData();
            avatarData.head = new TextureData(head);
            avatarData.hair = new TextureData(hair);
            avatarData.face = new TextureData(face);
            avatarData.torso = new TextureData(torso);
            avatarData.arm = new TextureData(arm);
            avatarData.hip = new TextureData(hip);
            avatarData.thigh = new TextureData(thigh);
            avatarData.leg = new TextureData(leg);
            avatarData.shoe = new TextureData(shoe);
            FirebaseManager.Instance.SetAvatar(DataManager.PlayerUsername, avatarData.GetSaveData().ToString(), null);
        }
    }

    public TextureData GetDefault(TextureType type)
    {
        if (type == TextureType.AvatarHead)
        {
            return new TextureData(0, "a001", TextureType.AvatarHead);
        }
        else if (type == TextureType.AvatarHair)
        {
            return new TextureData(0, "h001", TextureType.AvatarHair);
        }
        else if (type == TextureType.AvatarFace)
        {
            return new TextureData(0, "f006", TextureType.AvatarFace);
        }
        else if (type == TextureType.AvatarTop)
        {
            return new TextureData(0, "s001", TextureType.AvatarTop);
        }
        else if (type == TextureType.AvatarArm)
        {
            return new TextureData(0, "r001", TextureType.AvatarArm);
        }
        else if (type == TextureType.AvatarHip)
        {
            return new TextureData(0, "p001", TextureType.AvatarHip);
        }
        else if (type == TextureType.AvatarThigh)
        {
            return new TextureData(0, "t001", TextureType.AvatarThigh);
        }
        else if (type == TextureType.AvatarLeg)
        {
            return new TextureData(0, "l001", TextureType.AvatarLeg);
        }
        else if (type == TextureType.AvatarShoe)
        {
            return new TextureData(0, "z001", TextureType.AvatarShoe);
        }
        else
        {
            return new TextureData(0, "", type);
        }
    }

    public JSONClass GetSaveData()
    {
        JSONClass data = new JSONClass();
        data["head"] = head.ToJSON();
        data["hair"] = hair.ToJSON();
        data["face"] = face.ToJSON();
        data["torso"] = torso.ToJSON();
        data["arm"] = arm.ToJSON();
        data["hip"] = hip.ToJSON();
        data["thigh"] = thigh.ToJSON();
        data["leg"] = leg.ToJSON();
        data["shoe"] = shoe.ToJSON();

        return data;
    }
}