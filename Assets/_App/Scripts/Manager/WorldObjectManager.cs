using SimpleJSON;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum WorldObjectInteractionType { None, Chair, Bed }
public static class WorldObjectManager
{
    public static Dictionary<string, WorldObjectData> worldObjects;
    public static void LoadData()
    {
        worldObjects = new Dictionary<string, WorldObjectData>();
        JSONNode data = JSON.Parse(Resources.Load<TextAsset>("Data/world_objects").text);
        for (int i = 0; i < data.Count; i++)
        {
            worldObjects.Add(data[i]["id"], new WorldObjectData(data[i]));
        }
    }
}
