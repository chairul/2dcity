using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextureLoader : MonoBehaviour
{
    public void LoadTexture(string url, bool isAnimated,  System.Action<bool, Texture2D> OnFinished)
    {
        if (isAnimated)
        {
            BackendConnection.GetBytes(url, (isSuccess, data, message) =>
            {
                if (isSuccess)
                {
                    StartCoroutine(UniGif.GetTextureListCoroutine(data, (result, loopCount, width, height) =>
                    {
                        OnFinished?.Invoke(isSuccess, result[0].m_texture2d);
                    }));
                }
                else
                {
                    OnFinished?.Invoke(false, Texture2D.whiteTexture);
                }
            });
        }
        else
        {
            BackendConnection.GetTexture(url, (bool isSuccess, Texture2D texture, string message) =>
            {
                OnFinished?.Invoke(isSuccess, texture);
            });
        }
    }
}
