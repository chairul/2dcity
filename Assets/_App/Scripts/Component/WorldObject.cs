using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldObject : MonoBehaviour
{
    [SerializeField] GameObject highLight;
    [SerializeField] SpriteRenderer spriteIcon;

    public SpriteRenderer Sprite
    {
        get
        {
            return GetComponent<SpriteRenderer>();
        }
    }

    public bool IsInteractable
    {
        get
        {
            return data.interactionType != WorldObjectInteractionType.None;
        }
    }

    public bool IsOccupied
    {
        get
        {
            return SceneWorldManager.instance == null ? false : SceneWorldManager.instance.IsSomeoneInteractingWithObject(data.tilePosition);
        }
    }

    public WorldObjectData data;
    System.Action<Vector3Int> OnDeleted;

    public void Init(Grid grid, WorldObjectData data, System.Action<Vector3Int> OnDeleted)
    {
        this.data = data;
        this.OnDeleted = OnDeleted;
        
        transform.position = new Vector3(grid.CellToWorld(data.tilePosition).x, grid.CellToWorld(data.tilePosition).y + (Sprite.size.y / 2), 0);
        spriteIcon.sprite = CollectionManager.GetWorldObject(data.id);
        SetHighlight(false);
    }

    public void Delete()
    {
        OnDeleted?.Invoke(data.tilePosition);
        Destroy(gameObject);
    }

    public void SetHighlight(bool isActiving)
    {
        highLight.SetActive(isActiving);
    }
}
