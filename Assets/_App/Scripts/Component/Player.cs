﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using TMPro;
using Mirror;

public class Player : NetworkBehaviour
{
    [SyncVar] public MirrorPlayer data;

    private void Start()
    {
        if (isLocalPlayer)
        {
            InitPlayer(MirrorManager.instance.playerData);

            MirrorManager.instance.myPlayer = this;
        }
        else
        {
            if (MirrorManager.instance.isHostMode)
            {
                InitPlayer(MirrorManager.instance.GetPlayerByConnId(connectionToClient.connectionId));
            }
            else
            {
                StartCoroutine(StartingInitPlayer());
            }
        }
    }
    
    [Command] void SendPlayerId(int id)
    {
        MirrorManager.instance.SetPlayerNewId(connectionToClient.connectionId, id);
    }

    IEnumerator StartingInitPlayer()
    {
        yield return new WaitUntil(() => !string.IsNullOrEmpty(data.username) && !string.IsNullOrEmpty(data.avatar));
        InitPlayer(data);
    }

    public void InitPlayer(MirrorPlayer data)
    {
        StartCoroutine(InitPlayerProccess(data));
    }

    IEnumerator InitPlayerProccess(MirrorPlayer data)
    {
        this.data = data;
        this.data.id = (int)netId;

        SendPlayerId(this.data.id);

        nameText.text = data.nickname;
        avatar.Load(data.GetAvatarData());
        minimapLegendMe.SetActive(isLocalPlayer);
        minimapLegendOther.SetActive(!isLocalPlayer);

        HideChatBubble();

        yield return new WaitUntil(() => SceneWorldManager.instance != null);
        SceneWorldManager.instance.AddPlayer(this);

        if (isLocalPlayer)
        {
            SceneWorldManager.instance.InitMyPlayer(this);
        }

        yield return new WaitUntil(() => MapDataRenderer.instance != null);
        yield return new WaitUntil(() => MapDataRenderer.isMapReady);
        if (data.IsInteractingWithObject)
        {
            SceneWorldManager.instance.IniteractWitchObjectForOtherPlayer(data.id, data.objectToInteract);
        }
        else
        {
            SceneWorldManager.instance.StopIniteractWitchObjectForOtherPlayer(data.id, data.objectToInteract);
        }
    }

    [Command] void SendBuildingToInteract(string buildingId)
    {
        MirrorManager.instance.SetBuildingToInteractOnPlayer(data.id, buildingId);
        RecieveBuildingToInteract(data.id, buildingId);
    }

    [ClientRpc] void RecieveBuildingToInteract(int id, string buildingId)
    {
        if (data.id == id)
        {
            data.buildingToInteract = buildingId;
        }
    }

    [ClientRpc] void RecieveInteractToObjectStatus(int id, Vector3Int position)
    {
        if (data.id == id)
        {
            data.objectToInteract = position;
        }
    }

    [Command] public void CommandSendChat(int id, string name, string chat)
    {
        RPCSendChat(id, name, chat);
    }

    [ClientRpc] void RPCSendChat(int id, string name, string chat)
    {
        if (SceneWorldManager.instance.IsPlayerNearMe(id))
        {
            ChatManagerUI.instance.CreateChatText(name, id == data.id, chat);
            SceneWorldManager.instance.ShowChatBubble(id, chat);
        }
    }

    [Command] public void CommandInteractWithObject(int actorId, Vector3Int position)
    {
        if(SceneWorldManager.instance.GetPlayer(actorId) == null)
        {
            return;
        }

        RPCInteractWithObject(actorId, position);

        MirrorManager.instance.SetInteractWithObjectStatus(data.id, position);
        RecieveInteractToObjectStatus(data.id, position);
    }

    [ClientRpc] void RPCInteractWithObject(int actorId, Vector3Int objectId)
    {
        if (actorId == data.id)
        {
            SceneWorldManager.instance.IniteractWitchObjectForOtherPlayer(actorId, objectId);
        }
    }

    [Command] public void CommandStopInteractWithObject(int actorId)
    {
        if(SceneWorldManager.instance.GetPlayer(actorId) == null)
        {
            return;
        }

        RPCStopInteractWithObject(actorId, Vector3Int.zero);

        MirrorManager.instance.SetInteractWithObjectStatus(data.id, Vector3Int.zero);
        RecieveInteractToObjectStatus(data.id, Vector3Int.zero);
    }

    [ClientRpc] void RPCStopInteractWithObject(int actorId, Vector3Int objectId)
    {
        if (actorId == data.id)
        {
            SceneWorldManager.instance.StopIniteractWitchObjectForOtherPlayer(actorId, objectId);
        }
    }

    [SerializeField] Avatar avatar;
    [SerializeField] float speed;
    [SerializeField] Animator anim;
    [SerializeField] TMP_Text nameText;
    [SerializeField] GameObject chatBubble;
    [SerializeField] TMP_Text chatText;
    [SerializeField] float timeTillHello;
    [SerializeField] GameObject minimapLegendMe;
    [SerializeField] GameObject minimapLegendOther;

    float timer = 0;
    Joystick joystick;
    Vector2 movement;
    Rigidbody2D rb;
    bool isNotFirstWalk;

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    public void SetJoystick(Joystick joystick)
    {
        this.joystick = joystick;
    }

    public void Move(Vector2 direction)
    {
        if (ChatManagerUI.instance.isChatPanelActive() || !MapDataRenderer.isMapReady)
        {
            return;
        }

        if (direction == Vector2.zero)
        {
            return;
        }

        Vector2 target = new Vector2(rb.position.x + (direction.x * speed), rb.position.y + (direction.y * speed));
        rb.MovePosition(target);

        anim.SetFloat("XSpeed", direction.x);
        anim.SetFloat("YSpeed", direction.y);
        anim.SetFloat("Speed", direction.sqrMagnitude);
    }

    public void ToggleWalking(bool isWalking, float xMovement, float yMovement)
    {
        Vector2 direction = new Vector2(xMovement, yMovement);
        anim.SetFloat("XSpeed", direction.x);
        anim.SetFloat("YSpeed", direction.y);
        anim.SetFloat("Speed", direction.sqrMagnitude);
    }

    void Update()
    {
        if (!isLocalPlayer)
        {
            return;
        }

        SceneWorldManager.instance.SetStopInterractWithObjectButton(data.IsInteractingWithObject);

        if (data.IsInteractingWithObject)
        {
            return;
        }

        if (joystick == null)
        {
            return;
        }

        if (!DataManager.isAutoWalk)
        {
            movement = joystick.Direction != Vector2.zero ?
                joystick.Direction :
                new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));

            if (movement.x == 0 && movement.y == 0)
            {
                timer += Time.deltaTime;
                if (timer >= timeTillHello)
                {
                    anim.SetTrigger("Hello");
                    timer = 0;
                }
            }
            else
            {
                timer = 0;
            }
        }
        else
        {
            if (timer <= 0)
            {
                if (Random.Range(0, 100) < 20)
                {
                    ChatManagerUI.instance.SendChat("halo woi");
                }
                else
                {
                    if (!isNotFirstWalk)
                    {
                        movement = new Vector2(-1f, 1f);
                    }
                    else
                    {
                        movement = new Vector2(Random.Range(-1f, 1f), Random.Range(-1f, 1f));
                    }
                    isNotFirstWalk = true;
                }

                timer = 1;
            }
            else
            {
                timer -= Time.deltaTime;
            }
        }
    }

    private void FixedUpdate()
    {
        if (!isLocalPlayer)
        {
            return;
        }

        if (!string.IsNullOrEmpty(data.buildingToInteract))
        {
            SceneWorldManager.instance.SetBuildingInfo(true);
        }
        else
        {
            SceneWorldManager.instance.SetBuildingInfo(false);
        }

        if (movement.x != 0 || movement.y != 0)
        {
            Move(movement);
        }
        else
        {
            rb.velocity = Vector2.zero;
            ToggleWalking(false, 0, 0);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!isLocalPlayer)
        {
            return;
        }

        if (collision.GetComponent<BuildingSpot>() != null)
        {
            data.buildingToInteract = collision.GetComponent<BuildingSpot>().spotData.id;
            SendBuildingToInteract(data.buildingToInteract);
        }

        if (collision.GetComponent<WorldObject>() != null)
        {
            SceneWorldManager.instance.SetInteractWithObjectButton(true, collision.GetComponent<WorldObject>());
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (!isLocalPlayer)
        {
            return;
        }

        if (collision.GetComponent<BuildingSpot>() != null)
        {
            data.buildingToInteract = "";
            SendBuildingToInteract(data.buildingToInteract);
        }

        if (collision.GetComponent<WorldObject>() != null)
        {
            SceneWorldManager.instance.SetInteractWithObjectButton(false, collision.GetComponent<WorldObject>());
        }
    }

    public void InteractWithObject(WorldObject furnitureObject)
    {
        data.objectToInteract = furnitureObject.data.tilePosition;
        GetComponent<BoxCollider2D>().isTrigger = true;

        transform.position = new Vector3(furnitureObject.transform.position.x + furnitureObject.data.playerPosX, furnitureObject.transform.position.y + furnitureObject.data.playerPosY, 0);

        if (furnitureObject.data.interactionType == WorldObjectInteractionType.Chair)
        {
            anim.ResetTrigger("SitDone");
            anim.SetTrigger("Sit");
        }
        else if (furnitureObject.data.interactionType == WorldObjectInteractionType.Bed)
        {
            anim.ResetTrigger("SleepDone");
            anim.SetTrigger("Sleep");
        }
    }

    public void StopInteractWithObject(WorldObject furnitureObject)
    {
        data.objectToInteract = Vector3Int.zero;
        GetComponent<BoxCollider2D>().isTrigger = false;

        if (furnitureObject.data.interactionType == WorldObjectInteractionType.Chair)
        {
            anim.SetTrigger("SitDone");
        }
        else if (furnitureObject.data.interactionType == WorldObjectInteractionType.Bed)
        {

            anim.SetTrigger("SleepDone");
        }
    }

    public void ShowChatBubble(string chat)
    {
        chatText.text = chat;
        chatBubble.gameObject.SetActive(true);
        Invoke("HideChatBubble", 2f);
    }

    void HideChatBubble()
    {
        chatBubble.gameObject.SetActive(false);
    }
}
