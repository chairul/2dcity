using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class BuildingWall : BuildingStatic
{
    public new static System.Action<BuildingWall> OnClick;

    [SerializeField] GameObject highlightB;

    private bool isWallHigh = true;
    private BuildingObjectFace face;
    public BuildingObjectFace Face { get { return face; } }

    public override void Click(BaseEventData eventData)
    {
        //OnClick?.Invoke(this);
    }

    public void SetHeight(bool isHigh)
    {
        if (isWallHigh == isHigh)
            return;

        Base.GetComponent<SpriteRenderer>().size = new Vector2(
            Base.GetComponent<SpriteRenderer>().size.x,
            isHigh ? DataManager.defaultWallHigh : DataManager.defaultWallLow);

        isWallHigh = isHigh;
    }

    public void SetHighlight(BuildingObjectFace face)
    {
        highlightA.SetActive(face == BuildingObjectFace.Right || face == BuildingObjectFace.Both);
        highlightB.SetActive(face == BuildingObjectFace.Left || face == BuildingObjectFace.Both);

        SetFace(face);
    }

    public void SetAlpha(float alpha)
    {
        Base.GetComponent<SpriteRenderer>().color = Base.GetComponent<SpriteRenderer>().color.SetAlpha(alpha);
    }

    public void SetFace(BuildingObjectFace face)
    {
        this.face = face;
    }

    public bool IsBackWall(int width, int height)
    {
        return tilePosition.x == width - 1 || tilePosition.y == height - 1;
    }

    public void SetTilePosition(Vector3Int tilePosition)
    {
        this.tilePosition = tilePosition;
    }
}
