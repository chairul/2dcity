using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FullMapPinTeleporter : MonoBehaviour
{
    Vector3 position;
    System.Action<Vector3> OnTeleport;
    public void Init(Vector3 position, System.Action<Vector3> OnTeleport)
    {
        this.position = position;
        this.OnTeleport = OnTeleport;
    }

    public void Teleport()
    {
        OnTeleport?.Invoke(position);
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
