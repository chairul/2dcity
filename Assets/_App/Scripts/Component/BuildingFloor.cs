using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class BuildingFloor : BuildingStatic
{
    public new static System.Action<BuildingFloor> OnClick;

    public override void Click(BaseEventData eventData)
    {
        //OnClick?.Invoke(this);
    }

    public void SetHighlight(bool isActiving)
    {
        highlightA.SetActive(isActiving);
    }
}
