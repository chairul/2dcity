using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_WEBGL
using FirebaseWebGL.Scripts;
using FirebaseWebGL.Scripts.FirebaseBridge;
using FirebaseWebGL.Scripts.Objects;
#else
using Firebase;
using Firebase.Database;
using Firebase.Extensions;
#endif
public enum FirebaseWaitingStatus { Waiting, Success, Failed }
public class FirebaseLoader : MonoBehaviour
{
    FirebaseWaitingStatus waitingStatus;
    string waitingResult;

    public void SetIsWaitingDone(string result)
    {
        waitingResult = result;
        waitingStatus = FirebaseWaitingStatus.Success;
    }

    public void SetIsWaitingFail(string result)
    {
        waitingResult = result;
        waitingStatus = FirebaseWaitingStatus.Failed;
    }

    public void BuyBuilding(BuildingData data, System.Action<bool> OnDone, System.Action OnTimeout = null)
    {
        StartCoroutine(BuyBuilding_(data, (bool result) =>
        {
            OnDone?.Invoke(result);

            StopAllCoroutines();
            Destroy(gameObject, 1);
        }));

        StartCoroutine(Timeout(60, () =>
        {
            Debug.LogError("Operation timed out", gameObject);

            OnTimeout?.Invoke();
            StopAllCoroutines();
            Destroy(gameObject, 1);
        }));
    }

    public void SetBuilding(BuildingData data, System.Action<bool> OnDone, System.Action OnTimeout = null)
    {
        StartCoroutine(SetBuilding_(data, (bool result) =>
        {
            OnDone?.Invoke(result);

            StopAllCoroutines();
            Destroy(gameObject, 1);
        }));

        StartCoroutine(Timeout(60, () =>
        {
            Debug.LogError("Operation timed out", gameObject);

            OnTimeout?.Invoke();
            StopAllCoroutines();
            Destroy(gameObject, 1);
        }));
    }

    public void GetBuilding(string buildingId, System.Action<string> OnDone, System.Action OnTimeout = null)
    {
        StartCoroutine(GetBuilding_(buildingId, (string result) =>
        {
            OnDone?.Invoke(result);

            StopAllCoroutines();
            Destroy(gameObject, 1);
        }));

        StartCoroutine(Timeout(60, () =>
        {
            Debug.LogError("Operation timed out", gameObject);

            OnTimeout?.Invoke();
            StopAllCoroutines();
            Destroy(gameObject, 1);
        }));
    }

    public void GetBuildingList(string ownerId, System.Action<List<string>> OnDone, System.Action OnTimeout = null)
    {
        StartCoroutine(GetBuildingList_(ownerId, (List<string> result) =>
        {
            OnDone?.Invoke(result);

            StopAllCoroutines();
            Destroy(gameObject, 1);
        }));

        StartCoroutine(Timeout(60, () =>
        {
            Debug.LogError("Operation timed out", gameObject);

            OnTimeout?.Invoke();
            StopAllCoroutines();
            Destroy(gameObject, 1);
        }));
    }

    public void GetAvatar(string userId, System.Action<string> OnDone, System.Action OnTimeout = null)
    {
        StartCoroutine(GetAvatar_(userId, (string result) =>
        {
            OnDone?.Invoke(result);

            StopAllCoroutines();
            Destroy(gameObject, 1);
        }));

        StartCoroutine(Timeout(60, () =>
        {
            Debug.LogError("Operation timed out", gameObject);

            OnTimeout?.Invoke();
            StopAllCoroutines();
            Destroy(gameObject, 1);
        }));
    }

    public void GetAllAvatar(System.Action<string> OnDone, System.Action OnTimeout = null)
    {
        StartCoroutine(GetAllAvatar_((string result) =>
        {
            OnDone?.Invoke(result);

            StopAllCoroutines();
            Destroy(gameObject, 1);
        }));

        StartCoroutine(Timeout(60, () =>
        {
            Debug.LogError("Operation timed out", gameObject);

            OnTimeout?.Invoke();
            StopAllCoroutines();
            Destroy(gameObject, 1);
        }));
    }

    public void SetAvatar(string userId, string data, System.Action<bool> OnDone, System.Action OnTimeout = null)
    {
        StartCoroutine(SetAvatar_(userId, data, (bool result) =>
        {
            OnDone?.Invoke(result);

            StopAllCoroutines();
            Destroy(gameObject, 1);
        }));

        StartCoroutine(Timeout(60, () =>
        {
            Debug.LogError("Operation timed out", gameObject);

            OnTimeout?.Invoke();
            StopAllCoroutines();
            Destroy(gameObject, 1);
        }));
    }

    IEnumerator BuyBuilding_(BuildingData data, System.Action<bool> OnDone)
    {
        waitingStatus = FirebaseWaitingStatus.Waiting;
        yield return null;

#if !UNITY_WEBGL
        DatabaseReference reference = FirebaseManager.Instance.GetReferencePath();

        //Debug.LogError("1");
        //reference.Child("building_list").RunTransaction(mutableData =>
        //{
        //    Debug.LogError(mutableData.Value);
        //    Dictionary<string, object> buildingIds = mutableData.Value as Dictionary<string, object>;

        //    if (buildingIds != null)
        //    {
        //        Debug.LogError("1a");
        //        if (buildingIds.ContainsKey(data.id))
        //        {
        //            Debug.LogError("1aa");
        //            waitingStatus = FirebaseWaitingStatus.Failed;
        //            return TransactionResult.Abort();
        //        }
        //        else
        //        {
        //            Debug.LogError("1ab");
        //            buildingIds.Add(data.id, true);
        //            mutableData.Value = buildingIds;
        //        }
        //    }
        //    else
        //    {
        //        Debug.LogError("1b");
        //        buildingIds = new Dictionary<string, object>();
        //        buildingIds.Add(data.id, true);
        //        mutableData.Value = buildingIds;
        //    }

        //    waitingStatus = FirebaseWaitingStatus.Success;
        //    return TransactionResult.Success(mutableData);
        //});
        //yield return new WaitUntil(() => waitingStatus != FirebaseWaitingStatus.Waiting);
        //if (waitingStatus == FirebaseWaitingStatus.Failed)
        //{
        //    Debug.LogError("1c");
        //    OnDone?.Invoke(false);
        //    yield break;
        //}

        //Debug.LogError("0");
        reference.Child("building_list").GetValueAsync().ContinueWithOnMainThread(task =>
        {
            if (task.Result.HasChild(data.id))
            {
                //Debug.LogError("0a");
                SetIsWaitingFail("false");
            }
            else
            {
                SetIsWaitingDone("true");
            }
        });
        yield return new WaitUntil(() => waitingStatus != FirebaseWaitingStatus.Waiting);
        if (waitingStatus == FirebaseWaitingStatus.Failed)
        {
            OnDone?.Invoke(false);
            yield break;
        }

        waitingStatus = FirebaseWaitingStatus.Waiting;
        yield return null;
        //Debug.LogError("1");
        reference.Child("building_list").Child(data.id).SetValueAsync(true).ContinueWith(task =>
        {
            //Debug.LogError("1a");
            SetIsWaitingDone("true");
        });
        yield return new WaitUntil(() => waitingStatus != FirebaseWaitingStatus.Waiting);
        waitingStatus = FirebaseWaitingStatus.Waiting;
        yield return null;

        waitingStatus = FirebaseWaitingStatus.Waiting;
        yield return null;
        //Debug.LogError("2");
        reference.Child("building").Child(data.id).Child("owner").SetValueAsync(data.owner).ContinueWith(task =>
        {
            SetIsWaitingDone("true");
        });
        yield return new WaitUntil(() => waitingStatus != FirebaseWaitingStatus.Waiting);
        waitingStatus = FirebaseWaitingStatus.Waiting;
        yield return null;

        //Debug.LogError("3");
        reference.Child("building").Child(data.id).Child("data").SetRawJsonValueAsync(data.ToJSON().ToString()).ContinueWith(task =>
        {
            SetIsWaitingDone("true");
        });
        yield return new WaitUntil(() => waitingStatus != FirebaseWaitingStatus.Waiting);
        waitingStatus = FirebaseWaitingStatus.Waiting;
        yield return null;

        //Debug.LogError("4");
        reference.Child("profile").Child(data.owner).Child("owned_building").Child(data.id).SetValueAsync(true).ContinueWith(task =>
        {
            SetIsWaitingDone("true");
        });
        yield return new WaitUntil(() => waitingStatus != FirebaseWaitingStatus.Waiting);
        OnDone?.Invoke(true);
#else
        string path = FirebaseManager.Instance.GetReferencePath() + "building_list/";
        FirebaseDatabase.GetJSON(path, gameObject.name, "SetIsWaitingDone", "SetIsWaitingFail");
        yield return new WaitUntil(() => waitingStatus != FirebaseWaitingStatus.Waiting);

        if (waitingStatus != FirebaseWaitingStatus.Failed && !string.IsNullOrEmpty(waitingResult) && waitingResult != "null")
        {
            SimpleJSON.JSONNode result = SimpleJSON.JSON.Parse(waitingResult);
            if (result[data.id] != null || !string.IsNullOrEmpty(result[data.id]))
            {
                OnDone?.Invoke(false);
                yield break;
            }
        }

        waitingStatus = FirebaseWaitingStatus.Waiting;
        waitingResult = "";
        yield return null;

        path = FirebaseManager.Instance.GetReferencePath() + "building_list/" + data.id;
        string value = "true";
        FirebaseDatabase.PushJSONObject(path, value, gameObject.name, "SetIsWaitingDone", "SetIsWaitingFail");
        yield return new WaitUntil(() => waitingStatus != FirebaseWaitingStatus.Waiting);
        if (waitingStatus == FirebaseWaitingStatus.Failed)
        {
            OnDone?.Invoke(false);
            yield break;
        }
        waitingStatus = FirebaseWaitingStatus.Waiting;
        yield return null;

        path = FirebaseManager.Instance.GetReferencePath() + "building/" + data.id;
        value = data.ToJSONAll().ToString();
        FirebaseDatabase.PushJSONObject(path, value, gameObject.name, "SetIsWaitingDone", "SetIsWaitingFail");
        yield return new WaitUntil(() => waitingStatus != FirebaseWaitingStatus.Waiting);
        if (waitingStatus == FirebaseWaitingStatus.Failed)
        {
            OnDone?.Invoke(false);
            yield break;
        }
        waitingStatus = FirebaseWaitingStatus.Waiting;
        yield return null;

        path = FirebaseManager.Instance.GetReferencePath() + "profile/" + data.owner + "/owned_building/" + data.id;
        value = "true";
        FirebaseDatabase.PushJSONObject(path, value, gameObject.name, "SetIsWaitingDone", "SetIsWaitingFail");
        yield return new WaitUntil(() => waitingStatus != FirebaseWaitingStatus.Waiting);
        if (waitingStatus == FirebaseWaitingStatus.Failed)
        {
            OnDone?.Invoke(false);
            yield break;
        }

        OnDone?.Invoke(true);
#endif
    }

    IEnumerator SetBuilding_(BuildingData data, System.Action<bool> OnDone)
    {
        waitingStatus = FirebaseWaitingStatus.Waiting;
        yield return null;

#if !UNITY_WEBGL
        DatabaseReference reference = FirebaseManager.Instance.GetReferencePath();

        reference.Child("building").Child(data.id).Child("data").SetRawJsonValueAsync(data.ToJSON().ToString()).ContinueWith(task =>
        {
            SetIsWaitingDone("true");
        });
        yield return new WaitUntil(() => waitingStatus != FirebaseWaitingStatus.Waiting);
        OnDone?.Invoke(true);
#else
        string path = FirebaseManager.Instance.GetReferencePath() + "building/" + data.id + "/data";
        string value = data.ToJSON().ToString();
        FirebaseDatabase.PushJSONObject(path, value, gameObject.name, "SetIsWaitingDone", "SetIsWaitingFail");
        yield return new WaitUntil(() => waitingStatus != FirebaseWaitingStatus.Waiting);
        if (waitingStatus == FirebaseWaitingStatus.Failed)
        {
            OnDone?.Invoke(false);
            yield break;
        }
        OnDone?.Invoke(true);
#endif
    }

    IEnumerator GetBuilding_(string buildingId, System.Action<string> OnDone)
    {
        waitingStatus = FirebaseWaitingStatus.Waiting;
        waitingResult = "";
        yield return null;

#if !UNITY_WEBGL
        DatabaseReference reference = FirebaseManager.Instance.GetReferencePath();
        string data = "";

        reference.Child("building_list").GetValueAsync().ContinueWithOnMainThread(task => 
        {
            if (task.IsFaulted)
            {
                SetIsWaitingFail("false");
            }

            if (task.Result.HasChild(buildingId))
            {
                SetIsWaitingDone("true");
            }
            else
            {
                SetIsWaitingFail("false");
            }
        });
        yield return new WaitUntil(() => waitingStatus != FirebaseWaitingStatus.Waiting);

        if (waitingStatus == FirebaseWaitingStatus.Failed)
        {
            OnDone?.Invoke("");
            yield break;
        }

        waitingStatus = FirebaseWaitingStatus.Waiting;
        yield return null;

        reference.Child("building").Child(buildingId).GetValueAsync().ContinueWithOnMainThread(task =>
        {
            if (task.Result != null)
            {
                SetIsWaitingDone("true");
                data = task.Result.GetRawJsonValue().ToString();
            }
            else
            {
                SetIsWaitingFail("false");
            }
        });

        yield return new WaitUntil(() => waitingStatus != FirebaseWaitingStatus.Waiting);

        if (waitingStatus == FirebaseWaitingStatus.Success)
        {
            OnDone?.Invoke(data);
        }
        else
        {
            OnDone?.Invoke("");
        }
#else
        string path = FirebaseManager.Instance.GetReferencePath() + "building_list/";
        FirebaseDatabase.GetJSON(path, gameObject.name, "SetIsWaitingDone", "SetIsWaitingFail");
        yield return new WaitUntil(() => waitingStatus != FirebaseWaitingStatus.Waiting);

        if (waitingStatus == FirebaseWaitingStatus.Failed || string.IsNullOrEmpty(waitingResult) || waitingResult == "null")
        {
            OnDone?.Invoke(null);
            yield break;
        }
        else
        {
            SimpleJSON.JSONNode data = SimpleJSON.JSON.Parse(waitingResult);
            if (data[buildingId] == null || string.IsNullOrEmpty(data[buildingId]))
            {
                OnDone?.Invoke(null);
                yield break;
            }
        }

        waitingStatus = FirebaseWaitingStatus.Waiting;
        waitingResult = "";
        yield return null;

        path = FirebaseManager.Instance.GetReferencePath() + "building/" + buildingId;
        FirebaseDatabase.GetJSON(path, gameObject.name, "SetIsWaitingDone", "SetIsWaitingFail");
        yield return new WaitUntil(() => waitingStatus != FirebaseWaitingStatus.Waiting);

        if (waitingStatus == FirebaseWaitingStatus.Failed || string.IsNullOrEmpty(waitingResult) || waitingResult == "null")
        {
            OnDone?.Invoke(null);
            yield break;
        }
        else
        {
            OnDone?.Invoke(waitingResult);
        }
#endif
    }

    IEnumerator GetBuildingList_(string ownerId, System.Action<List<string>> OnDone)
    {
        waitingStatus = FirebaseWaitingStatus.Waiting;
        yield return null;

        List<string> result = new List<string>();

#if !UNITY_WEBGL
        DatabaseReference reference = FirebaseManager.Instance.GetReferencePath();

        reference.Child("profile").Child(ownerId).Child("owned_building").GetValueAsync().ContinueWithOnMainThread(task =>
        {
            if (task.Result == null)
            {
                SetIsWaitingFail("false");
            }
            else
            {
                SetIsWaitingDone("true");
                var data = SimpleJSON.JSON.Parse(task.Result.GetRawJsonValue()).AsObject;
                foreach (KeyValuePair<string, SimpleJSON.JSONNode> buildingList in data)
                {
                    result.Add(buildingList.Key);
                }
            }
        });

        yield return new WaitUntil(() => waitingStatus != FirebaseWaitingStatus.Waiting);

        if (waitingStatus == FirebaseWaitingStatus.Success)
        {
            OnDone?.Invoke(result);
        }
        else
        {
            OnDone?.Invoke(null);
        }
#else
        string path = FirebaseManager.Instance.GetReferencePath() + "profile/" + ownerId + "/owned_building";
        FirebaseDatabase.GetJSON(path, gameObject.name, "SetIsWaitingDone", "SetIsWaitingFail");
        yield return new WaitUntil(() => waitingStatus != FirebaseWaitingStatus.Waiting);

        if (waitingStatus == FirebaseWaitingStatus.Failed || string.IsNullOrEmpty(waitingResult) || waitingResult == "null")
        {
            OnDone?.Invoke(null);
            yield break;
        }
        else
        {
            try
            {
                var data = SimpleJSON.JSON.Parse(waitingResult).AsObject;
                foreach (KeyValuePair<string, SimpleJSON.JSONNode> buildingList in data)
                {
                    result.Add(buildingList.Key);
                }
                OnDone?.Invoke(result);
            }
            catch
            {
                OnDone?.Invoke(null);
            }
        }
#endif
    }

    IEnumerator GetAvatar_(string userId, System.Action<string> OnDone)
    {
        waitingStatus = FirebaseWaitingStatus.Waiting;
        yield return null;

        string result = string.Empty;

#if !UNITY_WEBGL
        DatabaseReference reference = FirebaseManager.Instance.GetReferencePath();

        reference.Child("profile").Child(userId).Child("avatar").GetValueAsync().ContinueWithOnMainThread(task =>
        {
            if (task.IsFaulted)
            {
                Debug.Log(task.Exception.Message);
            }
            if (task.Result == null)
            {
                SetIsWaitingFail("false");
            }
            else
            {
                SetIsWaitingDone("true");
                result = task.Result.GetRawJsonValue();
            }
        });

        yield return new WaitUntil(() => waitingStatus != FirebaseWaitingStatus.Waiting);

        if (waitingStatus == FirebaseWaitingStatus.Success)
        {
            OnDone?.Invoke(result);
        }
        else
        {
            OnDone?.Invoke(null);
        }
#else
        string path = FirebaseManager.Instance.GetReferencePath() + "profile/" + userId + "/avatar";
        FirebaseDatabase.GetJSON(path, gameObject.name, "SetIsWaitingDone", "SetIsWaitingFail");
        yield return new WaitUntil(() => waitingStatus != FirebaseWaitingStatus.Waiting);

        if (waitingStatus == FirebaseWaitingStatus.Failed || string.IsNullOrEmpty(waitingResult) || waitingResult == "null")
        {
            OnDone?.Invoke(null);
            yield break;
        }
        else
        {
            try
            {
                OnDone?.Invoke(waitingResult);
            }
            catch
            {
                OnDone?.Invoke(null);
            }
        }
#endif
    }

    IEnumerator GetAllAvatar_(System.Action<string> OnDone)
    {
        waitingStatus = FirebaseWaitingStatus.Waiting;
        yield return null;

        string result = string.Empty;

#if !UNITY_WEBGL
        DatabaseReference reference = FirebaseManager.Instance.GetReferencePath();

        reference.Child("profile").GetValueAsync().ContinueWithOnMainThread(task =>
        {
            if (task.Result == null)
            {
                SetIsWaitingFail("false");
            }
            else
            {
                SetIsWaitingDone("true");
                result = task.Result.GetRawJsonValue();
            }
        });

        yield return new WaitUntil(() => waitingStatus != FirebaseWaitingStatus.Waiting);

        if (waitingStatus == FirebaseWaitingStatus.Success)
        {
            OnDone?.Invoke(result);
        }
        else
        {
            OnDone?.Invoke(null);
        }
#else
        string path = FirebaseManager.Instance.GetReferencePath() + "profile/";
        FirebaseDatabase.GetJSON(path, gameObject.name, "SetIsWaitingDone", "SetIsWaitingFail");
        yield return new WaitUntil(() => waitingStatus != FirebaseWaitingStatus.Waiting);

        if (waitingStatus == FirebaseWaitingStatus.Failed || string.IsNullOrEmpty(waitingResult) || waitingResult == "null")
        {
            OnDone?.Invoke(null);
            yield break;
        }
        else
        {
            try
            {
                OnDone?.Invoke(waitingResult);
            }
            catch
            {
                OnDone?.Invoke(null);
            }
        }
#endif
    }

    IEnumerator SetAvatar_(string userId, string data, System.Action<bool> OnDone)
    {
        waitingStatus = FirebaseWaitingStatus.Waiting;
        yield return null;

#if !UNITY_WEBGL
        DatabaseReference reference = FirebaseManager.Instance.GetReferencePath();

        reference.Child("profile").Child(userId).Child("avatar").SetRawJsonValueAsync(data).ContinueWith(task =>
        {
            SetIsWaitingDone("true");
        });
        yield return new WaitUntil(() => waitingStatus != FirebaseWaitingStatus.Waiting);
        OnDone?.Invoke(true);
#else
        string path = FirebaseManager.Instance.GetReferencePath() + "profile/" + userId + "/avatar";
        string value = data;
        FirebaseDatabase.PushJSONObject(path, value, gameObject.name, "SetIsWaitingDone", "SetIsWaitingFail");
        yield return new WaitUntil(() => waitingStatus != FirebaseWaitingStatus.Waiting);
        if (waitingStatus == FirebaseWaitingStatus.Failed)
        {
            OnDone?.Invoke(false);
            yield break;
        }
        OnDone?.Invoke(true);
#endif
    }

    IEnumerator Timeout(float time = 60, System.Action OnTimeout = null)
    {
        yield return new WaitForSecondsRealtime(time);
        OnTimeout?.Invoke();
    }
}
