using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class BuildingStatic : MonoBehaviour
{
    public static System.Action<BuildingStatic> OnClick;
    [SerializeField] protected GameObject highlightA;

    protected TextureData textureData;

    public TextureData TextureData => textureData;
    public Transform Base => transform.GetChild(0);
    public Vector3Int tilePosition { get; protected set; }

    public bool IsHighlightActive => highlightA.activeSelf;
    public bool IsOccupied => GetComponentInChildren<BuildingObjectSplitted>(false) != null;
    

    public virtual void Click(BaseEventData eventData)
    {
        OnClick?.Invoke(this);
    }

    public void SetCollider(bool isActiving)
    {
        GetComponent<PolygonCollider2D>().enabled = isActiving;
    }

    public void CheckVisibility()
    {
        gameObject.SetActive(MapDataRenderer.instance.IsTileRendered(transform.position));
    }

    public virtual void Init(Vector3Int tilePosition, TextureData textureData)
    {
        this.tilePosition = tilePosition;
        this.textureData = textureData;
        SetSprite(textureData);
    }

    public void SetSprite(TextureData textureData)
    {
        this.textureData = textureData;

        BuildingTileType type = GetComponent<BuildingWall>() != null ? BuildingTileType.Wall : BuildingTileType.Floor;

        TextureManager.Instance.GetTexture(TextureData.ToTextureType(type), textureData, (TextureManager.TextureSpriteData result) =>
        {
            Base.GetComponent<SpriteRenderer>().sprite = result.sprite;
        });
    }

    public void ClearDecorations()
    {
        BuildingObject[] buildingObjects = transform.GetComponentsInChildren<BuildingObject>(true);
        if (buildingObjects != null && buildingObjects.Length > 0)
        {
            for (int i = 0; i < buildingObjects.Length; i++)
            {
                if (buildingObjects[i].Data.type == BuildingTileType.Painting)
                {
                    buildingObjects[i].DestroySplittedPainting();
                }
                Destroy(buildingObjects[i].gameObject);
            }
        }
    }
}
