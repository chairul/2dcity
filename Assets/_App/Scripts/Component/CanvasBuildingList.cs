using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasBuildingList : MonoBehaviour
{
    [SerializeField] Transform buildingParent;
    [SerializeField] GameObject buildingSample;
    [SerializeField] GameObject closeButton;

    string ownerId;
    System.Action<string> OnBuildingSelected;

    public void Init(string ownerId, System.Action<string> OnBuildingSelected)
    {
        this.ownerId = ownerId;
        this.OnBuildingSelected = OnBuildingSelected;

        buildingSample.SetActive(false);
        closeButton.SetActive(false);

        FirebaseManager.Instance.GetBuildingList(ownerId, (List<string> buildingIds) =>
        {
            closeButton.SetActive(true);

            if (buildingIds == null)
            {
                return;
            }

            for (int i = 0; i < buildingIds.Count; i++)
            {
                UIBuildingListItem item = Instantiate(buildingSample, buildingParent).GetComponent<UIBuildingListItem>();
                item.Init(buildingIds[i], (string id) =>
                {
                    OnBuildingSelected?.Invoke(id);
                });

                item.gameObject.SetActive(true);
            }
        });
    }

    public void Close()
    {
        Destroy(gameObject);
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
