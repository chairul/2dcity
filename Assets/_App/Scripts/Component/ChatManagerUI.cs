using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChatManagerUI : MonoBehaviour
{
    public static ChatManagerUI instance;
    private void Awake()
    {
        instance = this;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return))
        {
            if (isChatPanelActive())
            {
                SendChat();
            }
            else
            {
                ShowChatPanel();
            }
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (isChatPanelActive())
            {
                HideChatPanel();
            }
        }
    }

    //[SerializeField] GameObject openChatButton;
    [SerializeField] GameObject chatPanel;
    [SerializeField] Transform chatParent;
    [SerializeField] InputField chatInput;
    [SerializeField] GameObject notif;
    [SerializeField] GameObject chatSample;

    const int maxChatTextCount = 25;

    private void Start()
    {
        HideChatPanel();
    }

    public bool isChatPanelActive()
    {
        return chatPanel.activeSelf;
    }
    
    public void ToggleChatPanel()
    {
        if (isChatPanelActive())
        {
            HideChatPanel();
        }
        else
        {
            ShowChatPanel();
        }
    }

    public void ShowChatPanel()
    {
        //openChatButton.SetActive(false);
        chatPanel.SetActive(true);
        SetNotif(false);

        chatInput.ActivateInputField();

        StartCoroutine(RefreshChatParent());
    }

    public void HideChatPanel()
    {
        //openChatButton.SetActive(true);
        chatPanel.SetActive(false);
        SetNotif(false);
    }

    public void SendChat(string chat)
    {
        chatInput.text = chat;
        SendChat();
    }

    public void SendChat()
    {
        chatInput.ActivateInputField();

        if (string.IsNullOrEmpty(chatInput.text))
        {
            return;
        }

        if (chatInput.text.StartsWith("/t"))
        {
            string chatText = chatInput.text;
            chatText = chatText.Replace(" ", string.Empty);

            if (!chatText.Contains(","))
            {
                return;
            }

            string positionText = chatText.Substring(2, chatText.Length - 2);
            if (positionText.Length < 3 || positionText.StartsWith(",") || positionText.EndsWith(","))
            {
                return;
            }

            int x = 0;
            int y = 0;
            bool successX = int.TryParse(positionText.Substring(0, positionText.IndexOf(",")), out x);
            bool successY = int.TryParse(positionText.Substring(positionText.IndexOf(",") + 1), out y);

            if (successX && successY)
            {
                Vector3Int targetPosition = new Vector3Int(x, y, 0);
                MapDataRenderer.instance.TeleportPlayerToPosition(targetPosition);

                chatInput.text = "";
                HideChatPanel();
            }

            return;
        }

        MirrorManager.instance.SendChat(chatInput.text);

        chatInput.text = "";
    }

    public void CreateChatText(string playerName, bool isMyChat, string chat)
    {
        if (chatParent.childCount > maxChatTextCount)
        {
            Destroy(chatParent.GetChild(0).gameObject);
        }

        GameObject text = Instantiate(chatSample, chatParent);
        text.GetComponent<Text>().fontStyle = isMyChat ? FontStyle.Bold : FontStyle.Normal;
        text.GetComponent<Text>().text = string.Format("[{0}] {1}", playerName, chat);

        SetNotif(true);

        StartCoroutine(RefreshChatParent());
    }

    IEnumerator RefreshChatParent()
    {
        yield return null;
        chatParent.GetComponent<ContentSizeFitter>().enabled = false;
        yield return null;
        chatParent.GetComponent<ContentSizeFitter>().enabled = true;
    }

    void SetNotif(bool isActiving)
    {
        if (isChatPanelActive())
        {
            isActiving = false;
        }

        notif.SetActive(isActiving);
    }
}
