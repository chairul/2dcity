using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class UIDecorItem : MonoBehaviour
{
    [Header("Component")]
    [SerializeField] Image image;
    [SerializeField] GameObject panelQty;
    [SerializeField] Text textQty;

    private PlayerNFTData nftData;
    private TextureData data;
    private BuildingTileType placement;
    private System.Action<UIDecorItem> OnSelected;

    public TextureData Data { get { return data; } }

    public TextureManager.TextureSpriteData TextureSpriteData { get; private set; }

    public void Init(int index, BuildingTileType placement, System.Action<UIDecorItem> OnSelected)
    {
        if (panelQty != null)
        {
            panelQty.SetActive(false);
        }

        TextureData textureData = new TextureData(index, "", TextureData.ToTextureType(placement));
        Init(textureData, placement, OnSelected);
    }

    public void Init(PlayerNFTData data, BuildingTileType placement, System.Action<UIDecorItem> OnSelected)
    {
        this.nftData = data;

        if (panelQty != null)
        {
            panelQty.SetActive(true);
            textQty.text = $"{data.quantityLeft}";
        }

        TextureData textureData = new TextureData(data.SourceURL, data.imageURL, TextureData.ToTextureType(placement), data.imageType == NFTImageType.Animated);
        Init(textureData, placement, OnSelected);
    }

    public void Init(TextureData data, BuildingTileType placement, System.Action<UIDecorItem> OnSelected)
    {
        this.data = data;
        this.placement = placement;
        this.OnSelected = OnSelected;

        TextureManager.Instance.GetTexture(TextureData.ToTextureType(placement), data, (TextureManager.TextureSpriteData result) =>
        {
            TextureSpriteData = result;

            image.sprite = result.sprite;
        });
    }

    public void ChangeQuantity(int qty)
    {
        if (nftData != null)
        {
            NFTItemManager.Instance.AddNFTUsed(nftData.SourceURL, qty * -1);
            gameObject.SetActive(nftData.quantityLeft > 0);

            if (panelQty != null)
            {
                panelQty.SetActive(nftData.quantityLeft > 0);
                textQty.text = $"{nftData.quantityLeft}";
            }
        }
        else
        {
            gameObject.SetActive(qty > 0);
        }
    }

    public void Select(bool isOn)
    {
        if (isOn)
        {
            OnSelected?.Invoke(this);
        }
    }
}

public enum DecorPlacement
{
    Wall, Floor
}