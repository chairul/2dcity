using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;

public class Avatar : MonoBehaviour
{
    [SerializeField] SpriteRenderer head;
    [SerializeField] SpriteRenderer hair;
    [SerializeField] SpriteRenderer hairBack;
    [SerializeField] SpriteRenderer face;
    [SerializeField] SpriteRenderer torso;
    [SerializeField] SpriteRenderer torsoBack;
    [SerializeField] SpriteRenderer[] arm;
    [SerializeField] SpriteRenderer[] armBack;
    [SerializeField] SpriteRenderer hip;
    [SerializeField] SpriteRenderer hipBack;
    [SerializeField] SpriteRenderer[] thigh;
    [SerializeField] SpriteRenderer[] thighBack;
    [SerializeField] SpriteRenderer[] leg;
    [SerializeField] SpriteRenderer[] legBack;
    [SerializeField] SpriteRenderer[] shoe;
    [SerializeField] SpriteRenderer[] shoeBack;

    public void Load(AvatarData data)
    {
        ApplyHead(data.head, false);
        ApplyHair(data.hair, false);
        ApplyFace(data.face, false);
        ApplyTorso(data.torso, false);
        ApplyArm(data.arm, false);
        ApplyHip(data.hip, false);
        ApplyThigh(data.thigh, false);
        ApplyLeg(data.leg, false);
        ApplyShoe(data.shoe, false);
    }

    public void Apply(TextureData data)
    {
        if (data.type == TextureType.AvatarHead)
        {
            ApplyHead(data);
        }
        else if (data.type == TextureType.AvatarHair)
        {
            ApplyHair(data);
        }
        else if (data.type == TextureType.AvatarFace)
        {
            ApplyFace(data);
        }
        else if (data.type == TextureType.AvatarTop)
        {
            ApplyTorso(data);
        }
        else if (data.type == TextureType.AvatarArm)
        {
            ApplyArm(data);
        }
        else if (data.type == TextureType.AvatarHip)
        {
            ApplyHip(data);
        }
        else if (data.type == TextureType.AvatarThigh)
        {
            ApplyThigh(data);
        }
        else if (data.type == TextureType.AvatarLeg)
        {
            ApplyLeg(data);
        }
        else if (data.type == TextureType.AvatarShoe)
        {
            ApplyShoe(data);
        }
    }

    public void ApplyHead(TextureData data, bool applyInEdit = true)
    {
        if (applyInEdit)
        {
            AvatarManager.myAvatar.head = data;
        }

        TextureManager.Instance.GetTexture(TextureType.AvatarHead, data, (TextureManager.TextureSpriteData result) =>
        {
            head.sprite = result.sprite;
            head.enabled = head.sprite != null;
        });
    }

    public void ApplyHair(TextureData data, bool applyInEdit = true)
    {
        if (applyInEdit)
        {
            AvatarManager.myAvatar.hair = data;
        }

        TextureManager.Instance.GetTexture(TextureType.AvatarHair, data, (TextureManager.TextureSpriteData result) =>
        {
            if (data.textureSourceType == TextureSourceType.Index)
            {
                hair.sprite = result.sprite;
                hair.enabled = hair.sprite != null;
                ApplyBackSide(hairBack, data, TextureType.AvatarHair);
            }
            else
            {
                ApplyCuttedSprite(result.texture, hair, hairBack, TextureType.AvatarHair);
            }
        });
    }

    public void ApplyFace(TextureData data, bool applyInEdit = true)
    {
        if (applyInEdit)
        {
            AvatarManager.myAvatar.face = data;
        }

        TextureManager.Instance.GetTexture(TextureType.AvatarFace, data, (TextureManager.TextureSpriteData result) =>
        {
            face.sprite = result.sprite;
            face.enabled = face.sprite != null;
        });
    }

    public void ApplyTorso(TextureData data, bool applyInEdit = true)
    {
        if (applyInEdit)
        {
            AvatarManager.myAvatar.torso = data;
        }

        TextureManager.Instance.GetTexture(TextureType.AvatarTop, data, (TextureManager.TextureSpriteData result) =>
        {
            if (data.textureSourceType == TextureSourceType.Index)
            {
                torso.sprite = result.sprite;
                torso.enabled = torso.sprite != null;
                ApplyBackSide(torsoBack, data, TextureType.AvatarTop);
            }
            else
            {
                ApplyCuttedSprite(result.texture, torso, torsoBack, TextureType.AvatarTop);
            }
        });
    }

    public void ApplyArm(TextureData data, bool applyInEdit = true)
    {
        if (applyInEdit)
        {
            AvatarManager.myAvatar.arm = data;
        }

        TextureManager.Instance.GetTexture(TextureType.AvatarArm, data, (TextureManager.TextureSpriteData result) =>
        {
            if (data.textureSourceType == TextureSourceType.Index)
            {
                for (int i = 0; i < arm.Length; i++)
                {
                    arm[i].sprite = result.sprite;
                    arm[i].enabled = arm[i].sprite != null;
                }

                for (int i = 0; i < armBack.Length; i++)
                {
                    ApplyBackSide(armBack[i], data, TextureType.AvatarArm);
                }
            }
            else
            {
                ApplyCuttedSprite(result.texture, arm, armBack, TextureType.AvatarArm);
            }
        });
    }

    public void ApplyHip(TextureData data, bool applyInEdit = true)
    {
        if (applyInEdit)
        {
            AvatarManager.myAvatar.hip = data;
        }

        TextureManager.Instance.GetTexture(TextureType.AvatarHip, data, (TextureManager.TextureSpriteData result) =>
        {
            if (data.textureSourceType == TextureSourceType.Index)
            {
                hip.sprite = result.sprite;
                hip.enabled = hip.sprite != null;
                ApplyBackSide(hipBack, data, TextureType.AvatarHip);
            }
            else
            {
                ApplyCuttedSprite(result.texture, hip, hipBack, TextureType.AvatarHip);
            }
        });
    }

    public void ApplyThigh(TextureData data, bool applyInEdit = true)
    {
        if (applyInEdit)
        {
            AvatarManager.myAvatar.thigh = data;
        }

        TextureManager.Instance.GetTexture(TextureType.AvatarThigh, data, (TextureManager.TextureSpriteData result) =>
        {
            if (data.textureSourceType == TextureSourceType.Index)
            {
                for (int i = 0; i < thigh.Length; i++)
                {
                    thigh[i].sprite = result.sprite;
                    thigh[i].enabled = thigh[i].sprite != null;
                }

                for (int i = 0; i < thighBack.Length; i++)
                {
                    ApplyBackSide(thighBack[i], data, TextureType.AvatarThigh);
                }
            }
            else
            {
                ApplyCuttedSprite(result.texture, thigh, thighBack, TextureType.AvatarThigh);
            }
        });
    }

    public void ApplyLeg(TextureData data, bool applyInEdit = true)
    {
        if (applyInEdit)
        {
            AvatarManager.myAvatar.leg = data;
        }

        TextureManager.Instance.GetTexture(TextureType.AvatarLeg, data, (TextureManager.TextureSpriteData result) =>
        {
            if (data.textureSourceType == TextureSourceType.Index)
            {
                for (int i = 0; i < leg.Length; i++)
                {
                    leg[i].sprite = result.sprite;
                    leg[i].enabled = leg[i].sprite != null;
                }

                for (int i = 0; i < legBack.Length; i++)
                {
                    ApplyBackSide(legBack[i], data, TextureType.AvatarLeg);
                }
            }
            else
            {
                ApplyCuttedSprite(result.texture, leg, legBack, TextureType.AvatarLeg);
            }
        });
    }

    public void ApplyShoe(TextureData data, bool applyInEdit = true)
    {
        if (applyInEdit)
        {
            AvatarManager.myAvatar.shoe = data;
        }

        TextureManager.Instance.GetTexture(TextureType.AvatarShoe, data, (TextureManager.TextureSpriteData result) =>
        {
            if (data.textureSourceType == TextureSourceType.Index)
            {
                for (int i = 0; i < shoe.Length; i++)
                {
                    shoe[i].sprite = result.sprite;
                    shoe[i].enabled = shoe[i].sprite != null;
                }

                for (int i = 0; i < shoeBack.Length; i++)
                {
                    ApplyBackSide(shoeBack[i], data, TextureType.AvatarShoe);
                }
            }
            else
            {
                ApplyCuttedSprite(result.texture, shoe, shoeBack, TextureType.AvatarShoe);
            }
        });
    }

    void ApplyBackSide(SpriteRenderer renderer, TextureData data, TextureType type)
    {
        renderer.enabled = false;
        if (!data.textureId.Contains("000"))
        {
            TextureData backData = new TextureData(0, data.textureId + "a", type);
            TextureManager.Instance.GetTexture(type, backData, (TextureManager.TextureSpriteData result) =>
            {
                renderer.sprite = result.sprite;
                renderer.enabled = renderer.sprite != null;

                if (type == TextureType.AvatarLeg)
                {
                    renderer.sprite = Sprite.Create(result.texture, new Rect(0, 0, result.texture.width, result.texture.height), TextureManager.avatarLegBackSpritePivot);
                }
            });
        }
    }
    void ApplyCuttedSprite(Texture2D texture, SpriteRenderer frontSprite, SpriteRenderer backSprite, TextureType type)
    {
        SpriteRenderer[] frontSprites = new SpriteRenderer[1];
        frontSprites[0] = frontSprite;
        SpriteRenderer[] backSprites = new SpriteRenderer[1];
        backSprites[0] = backSprite;

        ApplyCuttedSprite(texture, frontSprites, backSprites, type);
    }

    void ApplyCuttedSprite(Texture2D texture, SpriteRenderer[] frontSprites, SpriteRenderer[] backSprites, TextureType type)
    {
        Sprite[] splittedSprite = TextureManager.Instance.SplitAvatarParts(texture, type);

        for (int i = 0; i < frontSprites.Length; i++)
        {
            frontSprites[i].sprite = splittedSprite[0];
        }

        for (int i = 0; i < backSprites.Length; i++)
        {
            backSprites[i].sprite = splittedSprite[1];
        }
    }
}
