using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingObjectSplitted : MonoBehaviour
{
    [SerializeField] SpriteRenderer srLeft;
    [SerializeField] SpriteRenderer srRight;

    private float textureWidth;
    private BuildingObject parentObject;

    public BuildingObjectFace Face { get { return srLeft.gameObject.activeSelf ? BuildingObjectFace.Left : BuildingObjectFace.Right; } }
    public BuildingObject ParentObject { get { return parentObject; } }

    // for painting
    public void Init(BuildingObject parentObject, Texture2D texture, int index, int size, BuildingObjectFace face)
    {
        this.parentObject = parentObject;
        textureWidth = DataManager.paintingDefaultWidth;

        if (index == size - 1)
        {
            textureWidth = texture.width - (index * DataManager.paintingDefaultWidth);
        }

        srLeft.sprite = Sprite.Create(texture,
            new Rect(index * DataManager.paintingDefaultWidth, 0, textureWidth, texture.height),
            new Vector2(0f, 0.5f));
        srRight.sprite = Sprite.Create(texture,
            new Rect(index * DataManager.paintingDefaultWidth, 0, textureWidth, texture.height),
            new Vector2(0f, 0.5f));

        srLeft.gameObject.SetActive(face == BuildingObjectFace.Left);
        srRight.gameObject.SetActive(face == BuildingObjectFace.Right);
    }

    // for furniture
    public void Init(BuildingObject parentObject, Texture2D texture)
    {
        this.parentObject = parentObject;

        srLeft.sprite = Sprite.Create(texture,
            new Rect(0, 0, texture.width, texture.height),
            new Vector2(0.5f, 0f));

        srLeft.gameObject.SetActive(true);
    }

    //public void UpdateCollider()
    //{
    //    if (srLeft.GetComponent<BoxCollider2D>() == null)
    //    {
    //        return;
    //    }

    //    srLeft.GetComponent<BoxCollider2D>().size = srRight.GetComponent<BoxCollider2D>().size = new Vector2(textureWidth / 100f, textureWidth / 100f);
    //    srLeft.GetComponent<BoxCollider2D>().offset = new Vector2((srLeft.GetComponent<BoxCollider2D>().size.x / 2f), 0);
    //    srRight.GetComponent<BoxCollider2D>().offset = new Vector2(-srRight.GetComponent<BoxCollider2D>().size.x / 2f, 0);
    //}

    public void ToggleFace()
    {
        srLeft.gameObject.SetActive(!srLeft.gameObject.activeSelf);
        srRight.gameObject.SetActive(!srRight.gameObject.activeSelf);
    }
}
