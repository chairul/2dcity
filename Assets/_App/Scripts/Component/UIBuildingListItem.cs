using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIBuildingListItem : MonoBehaviour
{
    [SerializeField] Text buildingName;

    string buildingId;
    System.Action<string> OnClick;

    public void Init(string buildingId, System.Action<string> OnClick)
    {
        this.buildingId = buildingId;
        this.OnClick = OnClick;

        buildingName.text = buildingId;
    }

    public void Select()
    {
        OnClick?.Invoke(buildingId);
    }


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
