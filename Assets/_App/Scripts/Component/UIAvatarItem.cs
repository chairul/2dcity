using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIAvatarItem : MonoBehaviour
{
    [SerializeField] Image icon;

    TextureData data;
    System.Action<TextureData> OnSelected;

    public void Init(TextureData data, System.Action<TextureData> OnSelected)
    {
        this.data = data;
        this.OnSelected = OnSelected;

        TextureManager.Instance.GetTexture(data.type, data, (TextureManager.TextureSpriteData result) =>
        {
            icon.sprite = result.sprite;
            if (data.textureSourceType == TextureSourceType.URL)
            {
                if (data.type == TextureType.AvatarHair || data.type == TextureType.AvatarArm || data.type == TextureType.AvatarTop || data.type == TextureType.AvatarHip || data.type == TextureType.AvatarThigh || data.type == TextureType.AvatarLeg || data.type == TextureType.AvatarShoe)
                {
                    Sprite[] splittedSprites = TextureManager.Instance.SplitAvatarParts(result.texture, data.type);
                    icon.sprite = splittedSprites[0];
                }
            }
            icon.enabled = icon.sprite != null;
        });
    }

    public void Select()
    {
        OnSelected?.Invoke(data);
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
