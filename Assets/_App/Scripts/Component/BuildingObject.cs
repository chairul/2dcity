using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.EventSystems;

public class BuildingObject : MonoBehaviour
{
    #region Fields & Properties

    [Header("Component")]
    [SerializeField] GameObject objSample;

    private BuildingTileData data;
    public BuildingTileData Data { get { return data; } }

    public BuildingStatic place { get; set; }
    public int PaintingSize { get; private set; }
    public List<BuildingObjectSplitted> paintingObject { get; private set; }
    public TextureData TextureData => data.type == BuildingTileType.Painting ? ((BuildingTilePaintingData)data).objTextureData : ((BuildingTileFurnitureData)data).objTextureData;
    public BuildingObjectFace Face => data.type == BuildingTileType.Painting ? ((BuildingTilePaintingData)data).face : ((BuildingTileFurnitureData)data).face;
    public TextureManager.TextureSpriteData TextureSpriteData { get; private set; }
    public string ObjectOwner { get; private set; }

    private System.Action<BuildingObject> OnTextureSet;
    private AudioSource audioSource;
    #endregion

    #region Gameplay

    public void Init(BuildingTileData data, BuildingStatic place, string objectOwner)
    {
        this.data = data;
        this.place = place;
        this.ObjectOwner = objectOwner;

        TextureManager.Instance.GetTexture(TextureData.ToTextureType(data.type), TextureData, (TextureManager.TextureSpriteData result) =>
        {
            if (this == null)
            {
                return;
            }

            TextureSpriteData = result;
            SetTexture(result.texture);

        });

        InitWorldProperties();
    }

    public void SetCallback(System.Action<BuildingObject> OnTextureSet)
    {
        this.OnTextureSet = OnTextureSet;
    }

    public void SetTexture(Texture2D texture)
    {
        if (paintingObject != null && paintingObject.Count > 0)
        {
            for (int i = 0; i < paintingObject.Count; i++)
            {
                Destroy(paintingObject[i].gameObject);
            }
        }

        paintingObject = new List<BuildingObjectSplitted>();

        if (data.type == BuildingTileType.Painting)
        {
            PaintingSize = Mathf.CeilToInt((float)texture.width / DataManager.paintingDefaultWidth);

            for (int i = 0; i < PaintingSize; i++)
            {
                GameObject obj = Instantiate(objSample, transform);
                if (obj == null || obj.GetComponent<BuildingObjectSplitted>() == null)
                {
                    return;
                }

                BuildingObjectSplitted buildingObject = obj.GetComponent<BuildingObjectSplitted>();
                buildingObject.Init(this, texture, i, PaintingSize, Face);

                paintingObject.Add(buildingObject);
            }

            MoveToWall((BuildingWall)place);
        }
        else if (data.type == BuildingTileType.Furniture)
        {
            GameObject obj = Instantiate(objSample, transform);
            if (obj == null || obj.GetComponent<BuildingObjectSplitted>() == null)
            {
                return;
            }

            BuildingObjectSplitted buildingObject = obj.GetComponent<BuildingObjectSplitted>();

            buildingObject.Init(this, texture);

            MoveToPlace(place);
        }

        UpdateCollider();
        OnTextureSet?.Invoke(this);
    }

    public void UpdateCollider()
    {
        GetComponent<BoxCollider2D>().size = new Vector2(TextureSpriteData.texture.width / 100f, 1.5f);
        GetComponent<BoxCollider2D>().offset = new Vector2((TextureSpriteData.texture.width / 2 / 100f) - (Face == BuildingObjectFace.Left ? 0.1f : 0.2f), 0.64f);
    }

    public void MoveToWall(BuildingWall place)
    {
        if (Face == place.Face)
        {
            ToggleFace();
        }

        MoveToPlace(place);
    }

    public void MoveToPlace(BuildingStatic place)
    {
        this.place = place;
        this.data.position = place.tilePosition;
        transform.SetParent(place.transform);
        transform.localPosition = Vector3.zero;

        OnTextureSet?.Invoke(this);
    }

    public void ToggleFace()
    {
        for (int i = 0; i < paintingObject.Count; i++)
        {
            paintingObject[i].ToggleFace();
        }

        if (data.type == BuildingTileType.Painting)
        {
            ((BuildingTilePaintingData)data).face = paintingObject[0].Face;
        }

        UpdateCollider();
    }

    public void DestroySplittedPainting()
    {
        if (paintingObject == null)
        {
            return;
        }

        for (int i = 0; i < paintingObject.Count; i++)
        {
            Destroy(paintingObject[i].gameObject);
        }
    }

    private void InitWorldProperties()
    {
        GetComponent<BoxCollider2D>().enabled = false;

        if (UnityEngine.SceneManagement.SceneManager.GetActiveScene().name != "World")
        {
            return;
        }

        if (TextureData.textureSourceType == TextureSourceType.URL && data.type == BuildingTileType.Painting)
        {
            GetComponent<BoxCollider2D>().enabled = true;
            StartCoroutine(GetNFTData());
        }
        else if (TextureData.textureSourceType == TextureSourceType.Index && data.type == BuildingTileType.Furniture)
        {
            string spriteName = CollectionManager.GetFurniture(TextureData.textureIndex).name;
            if (WorldObjectManager.worldObjects.ContainsKey(spriteName))
            {
                WorldObject worldObject = gameObject.AddComponent<WorldObject>();
                worldObject.data = new WorldObjectData();
                worldObject.data.CopyNewData(WorldObjectManager.worldObjects[spriteName], data.position);
                GetComponent<BoxCollider2D>().enabled = true;

                MapDataRenderer.instance.AddToWorldObject(worldObject);
            }
        }
    }

    private IEnumerator GetNFTData()
    {
        bool isWaiting = false;

#if METAMASK
        PlayerData player = NFTItemManager.PlayerData;

        if (ObjectOwner != DataManager.PlayerUsername || player == null)
        {
            isWaiting = true;
            Debug.Log("getting player data");
            NFTItemManager.Instance.GetPlayer(ObjectOwner, (PlayerData result) =>
            {
                isWaiting = false;
                player = result;
            });
        }

        yield return new WaitUntil(() => !isWaiting);

        int index = player.NFTList.FindIndex(x => x.SourceURL == TextureData.sourceURL);
        if (index > -1)
        {
            PlayerNFTData nftData = player.NFTList[index];

            // Download media if exist
            if (nftData.mediaType == NFTMediaType.audio)
            {
                Debug.Log("downloading media");
                BackendConnection.GetAudio(nftData.mediaURL, nftData.audioType,
                    (isSuccess, data, message) =>
                    {
                        Debug.Log(isSuccess + "," + message);
                        if (isSuccess)
                        {
                            if (audioSource == null)
                            {
                                if (this == null || gameObject == null)
                                {
                                    return;
                                }

                                audioSource = gameObject.AddComponent<AudioSource>();
                                audioSource.loop = true;
                                audioSource.playOnAwake = false;
                            }

                            audioSource.clip = data;
                        }
                        else
                        {
                            Debug.LogError($"Error getting media : {message}");
                        }
                    });
            }
        }
#else
        yield return null;
#endif
    }

    private void CheckAudio()
    {
        if (audioSource == null)
            return;

        if (Vector2.Distance(transform.position, SceneWorldManager.instance.Player.transform.position) < DataManager.defaultAudioRange)
        {
            if (!audioSource.isPlaying)
            {
                audioSource.Play();
            }
        }
        else
        {
            if (audioSource.isPlaying)
            {
                audioSource.Stop();
            }
        }
    }

    private void Update()
    {
        CheckAudio();
    }

    private void OnDisable()
    {
        if (GetComponent<WorldObject>() != null)
        {
            MapDataRenderer.instance.RemoveFromWorldObject(GetComponent<WorldObject>().data.tilePosition);
        }
    }

    private void OnMouseDown()
    {
        if (data.type != BuildingTileType.Painting)
        {
            return;
        }

        SceneWorldManager.instance.ShowPaintingNFT(ObjectOwner, TextureData);
    }
#endregion
}