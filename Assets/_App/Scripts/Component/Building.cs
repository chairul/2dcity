using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using System.Linq;

public class Building : MonoBehaviour
{
    #region Fields & Properties

    [Header("Component")]
    [SerializeField] Transform parentWall;
    [SerializeField] Transform parentFloor;
    [SerializeField] GameObject sampleObjectDecor;
    [SerializeField] GameObject sampleObjectFurn;

    [Header("Data")]
    [SerializeField] GameObject sampleFloor;
    [SerializeField] GameObject sampleWall;

    private BuildingData data;
    private BuildingData newData;
    private Grid grid;
    private Vector3Int spotPosition;
    private int[,] sortingValue;
    private Dictionary<Vector3Int, BuildingWall> wallObjects;
    private Dictionary<Vector3Int, BuildingFloor> floorObjects;
    private Dictionary<Vector3Int, BuildingObject> decorObjects;
    private Coroutine drawBuildingCoroutine;

    public BuildingData Data { get { return data; } }

    public bool IsReady { get; private set; }

    private System.Action<BuildingWall> OnWallClicked;
    private System.Action<BuildingFloor> OnFloorClicked;

    #endregion

    #region Unity Callback

    private void Awake()
    {

    }

    private void OnEnable()
    {
        BuildingWall.OnClick += HandleOnWallClicked;
        BuildingFloor.OnClick += HandleOnFloorClicked;
        MapDataRenderer.OnRefresh += DrawBuilding;
    }

    private void OnDisable()
    {
        BuildingWall.OnClick -= HandleOnWallClicked;
        BuildingFloor.OnClick -= HandleOnFloorClicked;
        MapDataRenderer.OnRefresh -= DrawBuilding;
    }
    #endregion

    #region Gameplay

    public void SetCallback(
        System.Action<BuildingWall> OnWallClicked,
        System.Action<BuildingFloor> OnFloorClicked)
    {
        this.OnWallClicked = OnWallClicked;
        this.OnFloorClicked = OnFloorClicked;
    }

    void Init(BuildingData data, Grid grid, Vector3Int spotPosition)
    {
        for (int i = 0; i < parentFloor.childCount; i++)
        {
            Destroy(parentFloor.GetChild(i).gameObject);
        }
        for (int i = 1; i < parentWall.childCount; i++)
        {
            Destroy(parentWall.GetChild(i).gameObject);
        }
        for (int i = 0; i < parentWall.GetChild(0).childCount; i++)
        {
            Destroy(parentWall.GetChild(0).GetChild(i).gameObject);
        }

        this.data = data;
        this.newData = data;
        this.grid = grid;
        this.spotPosition = spotPosition;
        wallObjects = new Dictionary<Vector3Int, BuildingWall>();
        floorObjects = new Dictionary<Vector3Int, BuildingFloor>();
        decorObjects = new Dictionary<Vector3Int, BuildingObject>();

        InitiateLayers(data);
    }

    void InitiateLayers(BuildingData data)
    {
        int diffX = 0;
        int diffY = 0;
        int lastX = 0;
        int lastY = 0;

        sortingValue = new int[data.size.x, data.size.y];

        for (int y = 0; y < data.size.y; y++)
        {
            diffX = diffY + 2;

            for (int x = 0; x < data.size.x; x++)
            {
                if (x == 0)
                {
                    sortingValue[x, y] = lastY + diffY;
                    lastX = lastY = sortingValue[x, y];
                }
                else
                {
                    sortingValue[x, y] = lastX + diffX;
                    ++diffX;
                    lastX = sortingValue[x, y];
                }
            }
            ++diffY;
        }
    }

    public void New(string id, int width, int height, Grid grid, Vector3Int spotPosition)
    {
        BuildingData data = new BuildingData(id, width, height);
        Load(data, grid, spotPosition);
    }

    public void LoadPS(BuildingData data, Grid grid, Vector3Int spotPosition, System.Action OnFinished = null)
    {
        Init(data, grid, spotPosition);
        StartCoroutine(LoadBuilding());

        IEnumerator LoadBuilding()
        {
            IsReady = false;

            yield return null;

            // Set wall and floor
            for (int y = 0; y < data.size.y; y++)
            {
                for (int x = 0; x < data.size.x; x++)
                {
                    Vector3Int tilePos = new Vector3Int(x, y, 0);
                    SpawnBuildingTile(tilePos);
                }
            }

            yield return null;

            foreach (BuildingWall wall in wallObjects.Values)
            {
                if (wall == null)
                {
                    continue;
                }
                wall.GetComponent<SortingGroup>().sortingOrder = 0;
            }

            foreach (BuildingObject buildingObject in decorObjects.Values)
            {
                if (buildingObject.Data.type == BuildingTileType.Painting)
                {
                    SplitPainting(buildingObject);
                }
            }

            IsReady = true;
            OnFinished?.Invoke();
        }
    }

    public void Load(BuildingData data, Grid grid, Vector3Int spotPosition)
    {
        if (!IsReady)
        {
            Init(data, grid, spotPosition);
        }

        this.newData = data;

        IsReady = true;

        //if (this.data != newData)
        //{
            DrawBuilding();
        //}
    }

    public void DrawBuilding()
    {
        if (!IsReady)
        {
            return;
        }

        for (int x = 0; x < data.size.x; x++)
        {
            for (int y = 0; y < data.size.y; y++)
            {
                Vector3Int position = new Vector3Int(x, y, 0);
                Vector3Int positionOnMap = position + spotPosition;

                if (!MapDataRenderer.instance.IsTileRendered(positionOnMap))
                {
                    DestroyBuildingTile(position);
                }
                else
                {
                    SpawnBuildingTile(position);
                }
            }
        }

        var sortedWall = wallObjects.OrderBy((a) => a.Key.y).OrderBy((a) => a.Key.x);
        foreach (var item in sortedWall)
        {
            if (item.Value == null)
            {
                continue;
            }

            item.Value.GetComponent<SortingGroup>().sortingOrder = 0;
        }

        foreach (BuildingObject buildingObject in decorObjects.Values)
        {
            if (buildingObject.Data.type == BuildingTileType.Painting)
            {
                SplitPainting(buildingObject);
            }
        }

        this.data = new BuildingData(newData);
    }

    void SpawnBuildingTile(Vector3Int position)
    {
        if (!data.tiles.ContainsKey(position))
        {
            return;
        }

        BuildingTileData tileData = data.tiles[position];
        BuildingTileData newTileData = newData.tiles[position];

        bool isDifferent = tileData != newTileData;

        if (isDifferent)
        {
            DestroyBuildingTile(position);
        }

        if (newTileData.type == BuildingTileType.Floor)
        {
            CreateFloor(newTileData);
        }
        else if (newTileData.type == BuildingTileType.Wall)
        {
            CreateWall(newTileData);
        }
        else if (newTileData.type == BuildingTileType.Furniture)
        {
            CreateFurniture(newTileData, CreateFloor(newTileData));
        }
        else if (newTileData.type == BuildingTileType.Painting)
        {
            CreatePainting(newTileData, CreateWall(newTileData));
        }
    }

    void DestroyBuildingTile(Vector3Int position)
    {
        if (!data.tiles.ContainsKey(position))
        {
            return;
        }

        BuildingTileData tile = data.tiles[position];

        if (tile.type == BuildingTileType.Floor)
        {
            DeleteFloor(position);
        }
        else if (tile.type == BuildingTileType.Wall)
        {
            DeleteWall(position);
        }
        else if (tile.type == BuildingTileType.Furniture)
        {
            DeleteFurniture(position);
        }
        else if (tile.type == BuildingTileType.Painting)
        {
            DeletePainting(position);
        }
    }

    BuildingFloor CreateFloor(BuildingTileData data)
    {
        BuildingFloor floor = null;
        if (IsInsidePersonalSpace())
        {
            floor = Instantiate(sampleFloor, parentFloor).GetComponent<BuildingFloor>();
        }
        else
        {
            if (floorObjects.ContainsKey(data.position) && floorObjects[data.position] != null)
            {
                floor = floorObjects[data.position];
                floor.gameObject.SetActive(true);
                return floor;
            }
            else
            {
                floor = MapDataLoader.instance.GetFreeFloorFromPool();
            }
        }

        if (floor == null)
        {
            return floor;
        }

        floor.transform.SetParent(parentFloor);
        floor.gameObject.name = data.position.ToString();
        floor.transform.localPosition = CellToLocal(new Vector3Int(data.position.x, data.position.y, 0));
        floor.Init(data.position, data.textureData);
        floor.SetHighlight(false);
        floor.SetCollider(false);
        floor.gameObject.SetActive(true);

        if (!floorObjects.ContainsKey(data.position))
        {
            floorObjects.Add(data.position, floor);
        }
        else
        {
            floorObjects[data.position] = floor;
        }

        return floor;
    }

    void DeleteFloor(Vector3Int position)
    {
        if (floorObjects.ContainsKey(position) && floorObjects[position] != null)
        {
            MapDataLoader.instance.ReturnFloorToPool(floorObjects[position]);
            floorObjects[position] = null;
        }
    }

    BuildingWall CreateWall(BuildingTileData data, bool isNewWall = false)
    {
        BuildingWall wall = null;
        if (IsInsidePersonalSpace())
        {
            wall = Instantiate(sampleWall, parentWall).GetComponent<BuildingWall>();
        }
        else
        {
            if (wallObjects.ContainsKey(data.position) && wallObjects[data.position] != null)
            {
                wall = wallObjects[data.position];
                wall.gameObject.SetActive(true);
                return wall;
            }
            else
            {
                wall = MapDataLoader.instance.GetFreeWallFromPool();
            }
        }

        if (wall == null)
        {
            return wall;
        }

        wall.transform.SetParent(parentWall);
        wall.gameObject.name = data.position.ToString();
        wall.transform.localPosition = CellToLocal(new Vector3Int(data.position.x, data.position.y, 0));
        wall.Init(data.position, data.textureData);
        wall.SetHighlight(BuildingObjectFace.None);
        wall.SetCollider(!IsInsidePersonalSpace());
        wall.GetComponent<SortingGroup>().sortingOrder = isNewWall ? 0 : -sortingValue[data.position.x, data.position.y];
        wall.gameObject.SetActive(true);

        if (!wallObjects.ContainsKey(data.position))
        {
            wallObjects.Add(data.position, wall);
        }
        else
        {
            wallObjects[data.position] = wall;
        }

        return wall;
    }

    void DeleteWall(Vector3Int position)
    {
        if (wallObjects.ContainsKey(position) && wallObjects[position] != null)
        {
            MapDataLoader.instance.ReturnWallToPool(wallObjects[position]);
            wallObjects[position] = null;
        }
    }

    void CreateFurniture(BuildingTileData data, BuildingFloor floor)
    {
        if (decorObjects.ContainsKey(data.position))
        {
            return;
        }

        BuildingObject clone = AddDecor(transform, DecorPlacement.Floor);
        clone.Init(data, floor, Data.owner);
        AddDecorToList(clone);
    }

    void DeleteFurniture(Vector3Int position)
    {
        if (decorObjects.ContainsKey(position))
        {
            Destroy(decorObjects[position].gameObject);
            decorObjects.Remove(position);
        }
        DeleteFloor(position);
    }


    void CreatePainting(BuildingTileData data, BuildingWall wall)
    {
        if (decorObjects.ContainsKey(data.position))
        {
            return;
        }

        BuildingObject clone = AddDecor(transform, DecorPlacement.Wall);
        clone.Init(data, wall, Data.owner);
        clone.transform.SetParent(wall.transform);
        clone.transform.localPosition = Vector3.zero;
        AddDecorToList(clone);
    }

    void DeletePainting(Vector3Int position)
    {
        if (decorObjects.ContainsKey(position))
        {
            decorObjects[position].DestroySplittedPainting();
            decorObjects.Remove(position);
        }
        DeleteWall(position);
    }

    public BuildingData GetCurrentData()
    {
        BuildingData buildingData = new BuildingData(data.id, data.size.x, data.size.y);

        buildingData.tiles.Clear();

        foreach(var floor in floorObjects)
        {
            BuildingTileData fData = new BuildingTileData();
            fData.position = floor.Key;
            fData.type = BuildingTileType.Floor;
            fData.textureData = floor.Value.TextureData;
            buildingData.tiles[fData.position] = fData;
        }

        foreach (var wall in wallObjects)
        {
            BuildingTileData wData = new BuildingTileData();
            wData.position = wall.Key;
            wData.type = BuildingTileType.Wall;
            wData.textureData = wall.Value.TextureData;
            buildingData.tiles[wData.position] = wData;
        }

        foreach (var obj in decorObjects)
        {
            if (obj.Value.Data.type == BuildingTileType.Furniture)
            {
                buildingData.tiles[obj.Value.Data.position] = (BuildingTileFurnitureData)obj.Value.Data;
            }
            else if (obj.Value.Data.type == BuildingTileType.Painting)
            {
                buildingData.tiles[obj.Value.Data.position] = (BuildingTilePaintingData)obj.Value.Data;
            }
        }

        return buildingData;
    }

    public bool IsInvalidTile(Vector3Int tilePos)
    {
        return tilePos.x >= data.size.x || tilePos.y >= data.size.y || tilePos.x < 0 || tilePos.y < 0;
    }

    public bool IsWallExist(Vector3Int tilePos)
    {
        return wallObjects.ContainsKey(tilePos);
    }

    public bool IsFloorExist(Vector3Int tilePos)
    {
        return floorObjects.ContainsKey(tilePos);
    }

    public BuildingWall GetWallByPosition(Vector3Int position)
    {
        if (wallObjects.ContainsKey(position))
        {
            return wallObjects[position];
        }

        return null;
    }

    public BuildingFloor GetFloorByPosition(Vector3Int position)
    {
        if (floorObjects.ContainsKey(position))
        {
            return floorObjects[position];
        }

        return null;
    }

    public bool IsValidNeighborTile(Vector3Int tilePos, int x, int y)
    {
        Vector3Int neighbor = new Vector3Int(tilePos.x + x, tilePos.y + y, 0);
        return !IsInvalidTile(neighbor);
    }

    public BuildingWall SpawnWall(Vector3Int tilePos, BuildingWall spawnedWall = null, TextureData textureData = null)
    {
        if (IsInvalidTile(tilePos))
            return null;

        if (spawnedWall == null)
        {
            BuildingTileWallData wallData = new BuildingTileWallData();
            wallData.position = tilePos;
            wallData.type = BuildingTileType.Wall;
            wallData.textureData = textureData is null ? new TextureData() : new TextureData(textureData);

            return CreateWall(wallData, true);
        }
        else
        {
            wallObjects.Add(tilePos, spawnedWall);
            spawnedWall.gameObject.SetActive(true);

            return spawnedWall;
        }
    }

    public BuildingFloor SpawnFloor(Vector3Int tilePos, BuildingFloor spawnedFloor = null)
    {
        if (IsInvalidTile(tilePos))
            return null;

        if (spawnedFloor == null)
        {
            BuildingTileFloorData floorData = new BuildingTileFloorData();
            floorData.position = tilePos;
            floorData.type = BuildingTileType.Floor;
            floorData.textureData = new TextureData(0, "", TextureType.Floor);

            return CreateFloor(floorData);
        }
        else
        {
            if (!floorObjects.ContainsKey(tilePos))
            {
                floorObjects.Add(tilePos, spawnedFloor);
            }
            spawnedFloor.gameObject.SetActive(true);

            return spawnedFloor;
        }
    }

    public void DestroyWall(Vector3Int tilePos)
    {
        if (IsWallExist(tilePos))
        {
            wallObjects[tilePos].gameObject.SetActive(false);
            wallObjects.Remove(tilePos);
        }
    }

    public void DestroyFloor(Vector3Int tilePos)
    {
        if (IsFloorExist(tilePos))
        {
            floorObjects[tilePos].gameObject.SetActive(false);
            floorObjects.Remove(tilePos);
        }
    }

    public WallNeighbor GetWallNeighbor(Vector3Int tilePos)
    {
        return new WallNeighbor()
        {
            hasTop = IsValidNeighborTile(tilePos, 0, 1) && IsWallExist(new Vector3Int(tilePos.x, tilePos.y + 1, 0)),
            TopTilePos = new Vector3Int(tilePos.x, tilePos.y + 1, 0),
            hasRight = IsValidNeighborTile(tilePos, 1, 0) && IsWallExist(new Vector3Int(tilePos.x + 1, tilePos.y, 0)),
            RightTilePos = new Vector3Int(tilePos.x + 1, tilePos.y, 0),
            hasBottom = IsValidNeighborTile(tilePos, 0, -1) && IsWallExist(new Vector3Int(tilePos.x, tilePos.y - 1, 0)),
            BottomTilePos = new Vector3Int(tilePos.x, tilePos.y - 1, 0),
            hasLeft = IsValidNeighborTile(tilePos, -1, 0) && IsWallExist(new Vector3Int(tilePos.x - 1, tilePos.y, 0)),
            LeftTilePos = new Vector3Int(tilePos.x - 1, tilePos.y, 0),
        };
    }

    public void ChangeFloor(Vector3Int tilePos, TextureData textureData)
    {
        floorObjects[tilePos].SetSprite(textureData);
    }

    public BuildingObject AddDecor(Transform place, DecorPlacement placement = DecorPlacement.Wall, GameObject sample = null)
    {
        GameObject toClone = null;

        if(sample == null)
        {
            if (placement == DecorPlacement.Wall)
                toClone = sampleObjectDecor;
            else
                toClone = sampleObjectFurn;
        }
        else
        {
            toClone = sample;
        }

        BuildingObject obj = Instantiate(toClone, place).GetComponent<BuildingObject>();
        obj.transform.position = new Vector3(place.position.x, place.position.y, 0);
        obj.gameObject.SetActive(true);

        if (placement == DecorPlacement.Wall)
            obj.SetCallback(SplitPainting);

        return obj;
    }

    public void AddDecorToList(BuildingObject buildingObject)
    {
        if (decorObjects == null)
        {
            decorObjects = new Dictionary<Vector3Int, BuildingObject>();
        }

        decorObjects.Add(buildingObject.Data.position, buildingObject);
    }

    public void RemoveDecorFromList(BuildingObject buildingObject)
    {
        if (decorObjects.ContainsKey(buildingObject.place.tilePosition))
        {
            decorObjects.Remove(buildingObject.place.tilePosition);
        }

        if (buildingObject.TextureData.textureSourceType == TextureSourceType.URL && NFTItemManager.Instance.NFTUsedData != null)
        {
            NFTItemManager.Instance.AddNFTUsed(buildingObject.TextureData.sourceURL, -1);
        }
    }

    public void MoveDecorInList(BuildingObject buildingObject, Vector3Int prevTilePosition)
    {
        if (decorObjects.ContainsKey(prevTilePosition))
        {
            decorObjects.Remove(prevTilePosition);
            AddDecorToList(buildingObject);
        }
    }

    public bool IsDecorExistOnWall(BuildingWall wall)
    {
        return decorObjects != null && wall.IsOccupied;
    }

    public bool IsDecorExistOnFloor(BuildingFloor floor)
    {
        return decorObjects != null && decorObjects.ContainsKey(floor.tilePosition);
    }

    public BuildingObject GetDecorOnWall(BuildingWall wall)
    {
        return wallObjects[wall.tilePosition].GetComponentInChildren<BuildingObjectSplitted>().ParentObject;
    }

    public BuildingObject GetDecorOnFloor(BuildingFloor floor)
    {
        return decorObjects[floor.tilePosition].GetComponent<BuildingObject>();
    }

    public Vector3 CellToLocal(Vector3Int tile)
    {
        return grid.CellToLocal(tile);
    }

    public bool IsInsidePersonalSpace()
    {
        return UnityEngine.SceneManagement.SceneManager.GetActiveScene().name == "PersonalSpace";
    }

    private void SplitPainting(BuildingObject obj)
    {
        StartCoroutine(SplitProcess());

        IEnumerator SplitProcess()
        {
            Vector3Int tilePos = new Vector3Int(obj.place.tilePosition.x, obj.place.tilePosition.y, 0);
            List<Transform> parent = new List<Transform>();

            Texture texture = null;
            TextureManager.Instance.GetTexture(TextureType.Painting, obj.TextureData, (TextureManager.TextureSpriteData result) =>
            {
                texture = result.texture;
            });
            yield return new WaitUntil(() => texture != null);

            if (obj.Face == BuildingObjectFace.Left)
            {
                Vector3Int finalPos = new Vector3Int(obj.place.tilePosition.x + (obj.PaintingSize - 1), obj.place.tilePosition.y, 0);
                for (int i = tilePos.x; i <= finalPos.x; i++)
                {
                    if (IsWallExist(new Vector3Int(i, tilePos.y, 0)))
                    {
                        if (wallObjects[new Vector3Int(i, tilePos.y, 0)] == null)
                        {
                            continue;
                        }
                        parent.Add(wallObjects[new Vector3Int(i, tilePos.y, 0)].transform);
                    }
                }
            }
            else if (obj.Face == BuildingObjectFace.Right)
            {
                Vector3Int finalPos = new Vector3Int(obj.place.tilePosition.x, obj.place.tilePosition.y - (obj.PaintingSize - 1), 0);
                for (int i = tilePos.y; i >= finalPos.y; i--)
                {
                    if (IsWallExist(new Vector3Int(tilePos.x, i, 0)))
                    {
                        if (wallObjects[new Vector3Int(tilePos.x, i, 0)] == null)
                        {
                            continue;
                        }
                        parent.Add(wallObjects[new Vector3Int(tilePos.x, i, 0)].transform);
                    }
                }
            }

            if (parent.Count < obj.PaintingSize || parent.Count < 1)
            {
                obj.gameObject.SetActive(false);
                yield break;
            }
            else
            {
                obj.gameObject.SetActive(true);
                obj.transform.SetParent(parent[0]);
                obj.transform.localPosition = Vector3.zero;

                for (int i = 1; i < parent.Count; i++)
                {
                    obj.paintingObject[i].transform.SetParent(parent[i]);
                    obj.paintingObject[i].transform.localPosition = Vector3.zero;
                }
            }
        }
    }

    public bool IsAValidWallToPlacePainting(Vector3Int tilePos)
    {
        if (!IsWallExist(tilePos))
        {
            return false;
        }

        //if (wallObjects[tilePos].GetComponent<BuildingWall>().IsCorner)
        //{
        //    return false;
        //}

        //if (!wallObjects[tilePos].GetComponent<BuildingWall>().HasHighlight)
        //{
        //    return false;
        //}

        if (IsInsidePersonalSpace())
        {
            if (wallObjects[tilePos].GetComponent<BuildingWall>().IsOccupied)
            {
                return false;
            }
        }

        //if (wallObjects[tilePos].GetComponent<BuildingWall>().Face != face)
        //{
        //    return false;
        //}

        return true;
    }

    private void HandleOnWallClicked(BuildingStatic wall)
    {
        //Debug.Log("click : " + wall.gameObject.name, wall.gameObject);
        OnWallClicked?.Invoke((BuildingWall)wall);
    }

    private void HandleOnFloorClicked(BuildingStatic floor)
    {
        //Debug.Log("click : " + floor.gameObject.name, floor.gameObject);
        OnFloorClicked?.Invoke((BuildingFloor)floor);
    }

    public void SetHighlightWall(bool isActiving, bool includeOccupied = false)
    {
        foreach (BuildingWall wall in wallObjects.Values)
        {
            SetHightlightForWall(wall, isActiving, includeOccupied);
        }
    }

    public void SetHightlightForWall(BuildingWall wall, bool isActiving, bool includeOccupied = false)
    {
        BuildingObjectFace face = BuildingObjectFace.None;

        if (isActiving)
        {
            WallNeighbor neighbor = GetWallNeighbor(wall.tilePosition);

            if (!neighbor.hasLeft && !neighbor.hasBottom)
                face = BuildingObjectFace.Both;
            else if (!neighbor.hasLeft)
                face = BuildingObjectFace.Left;
            else if (!neighbor.hasBottom)
                face = BuildingObjectFace.Right;

        }

        wall.GetComponent<BuildingWall>().SetHighlight(face);
    }

    public void SetHighlighWallByPaintingSize(int paintingSize)
    {
        foreach (var wall in wallObjects)
        {
            SetHightlightForWall(wall.Value.GetComponent<BuildingWall>(), true);

            Vector3Int pos = wall.Key;
            if (!IsAValidWallToPlacePainting(pos))
            {
                wall.Value.GetComponent<BuildingWall>().SetHighlight(BuildingObjectFace.None);
                continue;
            }

            if (wall.Value.GetComponent<BuildingWall>().Face == BuildingObjectFace.Right)
            {
                Vector3Int finalPost = new Vector3Int(pos.x + (paintingSize), pos.y, 0);
                for (int i = pos.x; i < finalPost.x; i++)
                {
                    if (!IsAValidWallToPlacePainting(new Vector3Int(i, pos.y, 0)))
                    {
                        wall.Value.GetComponent<BuildingWall>().SetHighlight(BuildingObjectFace.None);
                        break;
                    }
                }
            }
            else if (wall.Value.GetComponent<BuildingWall>().Face == BuildingObjectFace.Left)
            {
                Vector3Int finalPost = new Vector3Int(pos.x, pos.y - (paintingSize), 0);
                for (int i = pos.y; i >= finalPost.y; i--)
                {
                    if (!IsAValidWallToPlacePainting(new Vector3Int(pos.x, i, 0)))
                    {
                        wall.Value.GetComponent<BuildingWall>().SetHighlight(BuildingObjectFace.None);
                        break;
                    }
                }
            }
            else if (wall.Value.GetComponent<BuildingWall>().Face == BuildingObjectFace.Both)
            {
                Vector3Int finalPost = new Vector3Int(pos.x + (paintingSize), pos.y, 0);
                for (int i = pos.x; i < finalPost.x; i++)
                {
                    if (!IsAValidWallToPlacePainting(new Vector3Int(i, pos.y, 0)))
                    {
                        wall.Value.GetComponent<BuildingWall>().SetHighlight(BuildingObjectFace.None);
                        break;
                    }
                }

                finalPost = new Vector3Int(pos.x, pos.y - (paintingSize), 0);
                for (int i = pos.y; i >= finalPost.y; i--)
                {
                    if (!IsAValidWallToPlacePainting(new Vector3Int(pos.x, i, 0)))
                    {
                        wall.Value.GetComponent<BuildingWall>().SetHighlight(BuildingObjectFace.None);
                        break;
                    }
                }
            }
        }
    }

    public void SetHighlightFloor(bool isActiving, bool includeOccupied = false)
    {
        for (int i = 0; i < parentFloor.childCount; i++)
        {
            parentFloor.GetChild(i).GetComponent<BuildingFloor>().SetHighlight(isActiving && (includeOccupied ? true : !parentFloor.GetChild(i).GetComponent<BuildingFloor>().IsOccupied));
        }
    }

    public void SetWallHeightMode(bool isHigh, bool excludeBackWall = false)
    {
        foreach (var wall in wallObjects.Values)
        {
            wall.GetComponent<BuildingWall>().SetHeight(
                isHigh &&
                excludeBackWall ? (wall.GetComponent<BuildingWall>().IsBackWall(data.size.x, data.size.y)) : true);
        }
    }

    public void ToggleBuildingVisibility(bool isVisible)
    {
        parentFloor.gameObject.SetActive(isVisible);
        parentWall.gameObject.SetActive(isVisible);
    }

    #endregion

    public struct WallNeighbor
    {
        public bool hasTop;
        public Vector3Int TopTilePos;
        public bool hasRight;
        public Vector3Int RightTilePos;
        public bool hasBottom;
        public Vector3Int BottomTilePos;
        public bool hasLeft;
        public Vector3Int LeftTilePos;

        public bool AllExist()
        {
            return hasTop && hasRight && hasBottom && hasLeft;
        }
    }
}
