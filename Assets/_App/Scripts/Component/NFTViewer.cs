using SimpleJSON;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using System.Collections;
using System.Collections.Generic;
using System;

public class NFTViewer : MonoBehaviour
{
    #region Fields & Properties
    [Header("Result UI")]
    [SerializeField] GameObject panelLoading;
    [SerializeField] GameObject imageNFT;
    [SerializeField] GameObject gifNFT;
    [SerializeField] GameObject audioNFT;
    [SerializeField] GameObject videoNFT;
    [SerializeField] Image imgProvider;
    [SerializeField] Text textDescription;
    [SerializeField] Text textOwner;
    [SerializeField] Text textCreator;
    [SerializeField] GameObject viewAtUniqueOne;
    [SerializeField] GameObject importButton;

    [Header("Value")]
    [SerializeField] Sprite logoParas;
    [SerializeField] Sprite logoUniqueOne;

    private string owner;
    private TextureData textureData;
    private Texture2D textureResult;
    private bool isAnimated;
    private bool hasAudio;
    private bool hasVideo;
    private Action OnClose;
    private Action<TextureData> OnImport;
    private List<BackendConnection> currentBackendList = new List<BackendConnection>();

    private AudioSource audioNFTSource => audioNFT.GetComponent<AudioSource>();
    private UniGifImage gifNFTSource => gifNFT.GetComponent<UniGifImage>();
    private VideoPlayer videoNFTSource => videoNFT.GetComponent<VideoPlayer>();

    #endregion

    #region Unity Callback

    private void Start()
    {
        importButton.SetActive(false);
    }

    private void Update()
    {
        if (hasAudio)
        {
            audioNFT.GetComponentInChildren<Button>().interactable = !audioNFTSource.isPlaying;
        }

        if (hasVideo)
        {
            videoNFT.GetComponentInChildren<Button>().interactable = !videoNFTSource.isPlaying;
        }
    }

    #endregion

    #region Load Phase

    public void Init(string owner, TextureData textureData, Action OnClose)
    {
        this.owner = owner;
        this.textureData = textureData;
        this.OnClose = OnClose;

        StartCoroutine(LoadTexture(owner, textureData));
    }

    public void InitParas(string owner, string url, Action<TextureData> OnImport)
    {
        this.owner = owner;
        this.textureData = new TextureData(url, "", TextureType.Painting, false);
        this.OnImport = OnImport;

        StartCoroutine(LoadTexture(owner, textureData));
    }

    IEnumerator LoadTexture(string owner, TextureData textureData)
    {
        panelLoading.SetActive(true);

        if (textureData.sourceURL.Contains("unique.one"))
        {
            imgProvider.sprite = logoUniqueOne;
            viewAtUniqueOne.SetActive(true);

            bool isWaiting = true;
            PlayerData player = null;
            NFTItemManager.Instance.GetPlayer(owner, (PlayerData result) =>
            {
                isWaiting = false;
                player = result;
            });
            yield return new WaitUntil(() => !isWaiting);

            if (player == null)
            {
                panelLoading.GetComponent<Text>().text = $"Error getting image : Invalid URL ({textureData.sourceURL})";
            }
            else
            {
                int index = player.NFTList.FindIndex(x => x.SourceURL == textureData.sourceURL);
                if (index > -1)
                {
                    PlayerNFTData nftData = player.NFTList[index];

                    string viewURL = nftData.SourceURL;
                    string imageURL = nftData.imageURL;
                    textDescription.text = nftData.description;
                    textOwner.text = nftData.owner;
                    textCreator.text = nftData.creator;

                    // Download thumbnail
                    if (nftData.imageType == NFTImageType.Static)
                    {
                        currentBackendList.Add(BackendConnection.GetTexture(imageURL, HandleOnTextureRetrieved));
                    }
                    else
                    {
                        currentBackendList.Add(BackendConnection.GetBytes(imageURL, HandleOnBytesRetrieved));
                    }

                    // Download media if exist
                    if (nftData.mediaType == NFTMediaType.audio)
                    {
                        currentBackendList.Add(BackendConnection.GetAudio(nftData.mediaURL, nftData.audioType, HandleOnAudioRetrieved));
                    }
                    else if (nftData.mediaType == NFTMediaType.video)
                    {
                        videoNFT.SetActive(true);
                        videoNFTSource.url = nftData.mediaURL;
                        videoNFTSource.prepareCompleted += HandleOnVideoRetrieved;
                        videoNFTSource.Prepare();
                    }
                }
            }
        }
        else if (textureData.sourceURL.Contains("paras.id"))
        {
            imgProvider.sprite = logoParas;
            viewAtUniqueOne.SetActive(false);

            string[] splitToken = textureData.sourceURL.Split('/');
            string url = string.Format("https://api-v2-mainnet.paras.id/{0}/{1}/", splitToken[3], splitToken[4]);

            currentBackendList.Add(BackendConnection.GET(url, HandleOnTextRetrieved));
        }
    }

    private void HandleOnTextRetrieved(bool isSuccess, string data)
    {
        if (isSuccess)
        {
            JSONNode parsedData = JSON.Parse(data);

            textCreator.text = parsedData["creator_id"];
            textOwner.text = parsedData["owner_id"];
            textDescription.text = !string.IsNullOrEmpty(parsedData["metadata"]["description"]) ? parsedData["metadata"]["description"].Value : "No description";
            textureData.textureURL = parsedData["metadata"]["media"];

            currentBackendList.Add(BackendConnection.GetTexture(textureData.textureURL, HandleOnTextureRetrieved));
        }
        else
        {
            panelLoading.SetActive(true);
            panelLoading.GetComponent<Text>().text = $"Error getting image :\n{data}";
        }
    }

    private void HandleOnTextureRetrieved(bool isSuccess, Texture2D texture, string message)
    {
        isAnimated = false;
        panelLoading.SetActive(!isSuccess);

        if (isSuccess)
        {
            imageNFT.SetActive(true);
            imageNFT.GetComponent<Image>().sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f));
            imageNFT.GetComponent<Image>().color = Color.white;

            if (OnImport != null)
            {
                textureResult = texture;
                importButton.SetActive(true);
            }
        }
        else
        {
            panelLoading.GetComponent<Text>().text = $"Error getting image :\n{message}";
        }
    }

    private void HandleOnBytesRetrieved(bool isSuccess, byte[] data, string message)
    {
        if (isSuccess)
        {
            isAnimated = true;
            panelLoading.GetComponent<Text>().text = "Parsing gif...";
            StartCoroutine(GIFLoader());
        }
        else
        {
            panelLoading.GetComponent<Text>().text = $"Error getting image :\n{message}";
        }

        IEnumerator GIFLoader()
        {
            yield return StartCoroutine(UniGif.GetTextureListCoroutine(data, (result, loopCount, width, height) =>
            {
                gifNFT.SetActive(true);
                gifNFT.GetComponent<UniGifImage>().SetGifFromResult(result, loopCount, width, height);

                panelLoading.SetActive(false);
            }));
        }
    }

    private void HandleOnAudioRetrieved(bool isSuccess, AudioClip clip, string message)
    {
        if (isSuccess)
        {
            panelLoading.GetComponent<Text>().text = "Parsing audio...";
            StartCoroutine(AudioLoader());
        }
        else
        {
            panelLoading.GetComponent<Text>().text = $"Error getting audio :\n{message}";
        }

        IEnumerator AudioLoader()
        {
            yield return new WaitForEndOfFrame();

            hasAudio = true;
            audioNFT.SetActive(true);
            audioNFTSource.clip = clip;
            audioNFTSource.Play();
        }
    }

    private void HandleOnVideoRetrieved(VideoPlayer source)
    {
        panelLoading.SetActive(false);
        hasVideo = true;
        source.Play();
    }

    #endregion

    #region Action

    public void PlayAudio()
    {
        if (hasAudio && !audioNFTSource.isPlaying)
        {
            audioNFTSource.Play();
        }
    }

    public void PlayVideo()
    {
        if (hasVideo && !videoNFTSource.isPlaying)
        {
            videoNFTSource.Play();
        }
    }

    public void ViewOnWeb()
    {
        Application.OpenURL(textureData.textureURL);
    }

    public void Import()
    {
        if (OnImport == null)
        {
            return;
        }

        if (textureResult == null)
        {
            return;
        }

        TextureManager.Instance.AddToList(textureData.textureURL, textureResult, TextureType.Painting);
        OnImport?.Invoke(textureData);

        Close();
    }

    public void Close()
    {
        for (int i = 0; i < currentBackendList.Count; i++)
        {
            if (currentBackendList[i] != null)
            {
                currentBackendList[i].Abort();
            }
        }

        Destroy(gameObject);
        OnClose?.Invoke();
    }

    #endregion

    #region Static

    public static void Load(string owner, TextureData data, Action OnClose = null)
    {
        NFTViewer viewer = Instantiate(Resources.Load<NFTViewer>("Prefabs/NFTViewer"));
        viewer.Init(owner, data, OnClose);
    }

    public static void LoadParas(string owner, string url, Action<TextureData> OnImport)
    {
        NFTViewer viewer = Instantiate(Resources.Load<NFTViewer>("Prefabs/NFTViewer"));
        viewer.InitParas(owner, url, OnImport);
    }

    #endregion
}
