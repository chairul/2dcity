using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using System.IO;
using System;

public class PostProcessBuildWebGL
{
    [PostProcessBuild(1)]
    public static void OnPostprocessBuild(BuildTarget target, string pathToBuiltProject)
    {
        if (target == BuildTarget.WebGL)
        {
            InsertFirebaseImport(pathToBuiltProject);
        }
    }

    private static void InsertFirebaseImport(string buildPath)
    {
        string path = Path.Combine(buildPath, "index.html");
        string[] lineToAdd = new string[]
        {
            "<!-- The core Firebase JS SDK is always required and must be listed first -->",
            "<script src=\"https://www.gstatic.com/firebasejs/8.6.7/firebase-app.js\"></script>",
            "<!-- TODO: Add SDKs for Firebase products that you want to use https://firebase.google.com/docs/web/setup#available-libraries -->",
            "<script src=\"https://www.gstatic.com/firebasejs/8.6.7/firebase-database.js\"></script>",
            "<script src=\"StreamingAssets/firebase_config.js\"></script>",
        };
        string[] lines = File.ReadAllLines(path);
        int closeBodyTagPosition = Array.FindIndex(lines, x => x.Contains("</body>"));

        using (StreamWriter writer = new StreamWriter(path))
        {
            for (int i = 0; i < closeBodyTagPosition; i++)
                writer.WriteLine(lines[i]);
            for (int i = 0; i < lineToAdd.Length; i++)
                writer.WriteLine(lineToAdd[i]);
            for (int i = closeBodyTagPosition; i < lines.Length; i++)
                writer.WriteLine(lines[i]);
        }

        Debug.LogFormat("Done adding script to {0}", path);
    }
}
